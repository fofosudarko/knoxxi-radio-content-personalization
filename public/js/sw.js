self.addEventListener('install', () => {
  console.log('Service worker install event called');
});
self.addEventListener('activate', () => {
  console.log('Service worker activate event called');
});
self.addEventListener('push', (event) => {
  if (!(self.Notification && self.Notification.permission === 'granted')) {
    return;
  }
  const data = event.data?.json() ?? {};
  if (data.type === 'ContentPersonalization') {
    const body = data.body,
      contentPersonalizationId = body.id,
      contentStreamUrl =
        body.contentStreamShortUrl !== null
          ? body.contentStreamShortUrl
          : body.contentStreamUrl,
      title = `${data.title} from ${body.firstname}`;
    const message = `Press ${contentStreamUrl} to stream.`;
    const icon = '/img/knoxxi-logo-2.png';
    self.registration.showNotification(title, {
      body: message,
      tag: `content-personalization-push-notification-${contentPersonalizationId}`,
      icon,
      data: {
        url: contentStreamUrl,
        type: data.type,
      },
    });
  }
});
self.addEventListener('notificationclick', (event) => {
  event.notification.close();
  const { type, url } = event.notification?.data ?? {};
  event.waitUntil(
    type === 'ContentPersonalization' ? self.clients.openWindow(url) : undefined
  );
});
