export const FILE_EXTENSION_REGEX = /\.[a-zA-Z0-9]+$/;
export const APP_NAME = 'Knoxxi Radio Content Personalization';
export const DEFAULT_PAGE = 1;
export const DEFAULT_PAGE_SIZE = 10;
export const VALID_PHONE_NUMBER_REGEX = /^[+]?\d{12,15}$/;
export const ZEROES_WITHIN_PHONE_NUMBER_REGEX = /^(0{0,2}[1-9]{1,3})(0+)(.+)$/;
export const DEFAULT_API_TIMEOUT = 60000;
export const DEFAULT_SORT_DIRECTION = 'DESC';
export const SEARCH_MININUM_LENGTH = 2;
export const SEARCH_QUERY_DEBOUNCE_PERIOD = 0;
export const CKYC_TENANT = 'DFS';
export const APP_LOGO =
  process.env.NEXT_PUBLIC_BASE_PATH || '' + '/img/logo-knoxxi@2x.png';
export const APP_DASHBOARD_LOGO =
  process.env.NEXT_PUBLIC_BASE_PATH ||
  '' + '/img/Replugg logo in monochrome black on white.png';
export const SPLASH_IMAGE =
  process.env.NEXT_PUBLIC_BASE_PATH ||
  '' + '/img/replugg-electronics-splash-image-2.jpg';
export const IMAGE_PLACEHOLDER =
  process.env.NEXT_PUBLIC_BASE_PATH || '' + '/img/image-placeholder.jpeg';
export const GENDERS = ['MALE', 'FEMALE'];
export const KNOXXI_RADIO_SERVICE_API_URL =
  process.env.NEXT_PUBLIC_KNOXXI_RADIO_SERVICE_API_URL || '';
export const RECAPTCHA_SITE_KEY =
  process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY || '';
export const DEFAULT_SELECT_SIZE = 20;
export const ADVERT_PLAY_INTERVAL = parseInt(
  process.env.NEXT_PUBLIC_ADVERT_PLAY_INTERVAL || 1
);
export const FILE_PLAYER_CONFIG = {
  file: {
    attributes: {
      controlslist: 'nodownload',
    },
  },
};
export const ENHANCED_STREAM_PLAYER_POSTER =
  process.env.NEXT_PUBLIC_BASE_PATH || '' + '/img/knoxxi-logo-2.png';
export const ENHANCED_STREAM_PLAYER_CONTROLSLIST = 'nodownload';
export const KNOXXI_CKYC_SERVICE_API_URL =
  process.env.NEXT_PUBLIC_KNOXXI_CKYC_SERVICE_API_URL || '';
export const CKYC_SERVICE_RESPONSE_CODE = {
  C004: 'C004',
  C006: 'C006',
};
export const WEB_PUSH_NOTIFICATION_APPLICATION_SERVER_KEY =
  process.env.NEXT_PUBLIC_WEB_PUSH_NOTIFICATION_APPLICATION_SERVER_KEY || '';
