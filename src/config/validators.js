// validators.js

import * as yup from 'yup';

const FIRST_NAME_INPUT_VALIDATION = {
  regex: {
    value: /^[\w-]+$/,
    errorMessage:
      'Invalid first name. Must be alphabetic characters or hypenated',
  },
  required: {
    errorMessage: 'First name required',
  },
};

const PASSWORD_INPUT_VALIDATION = {
  regex: {
    value: /^[\W\w\d\D]+$/,
    errorMessage:
      'Invalid password. Must be alphanumeric characters and any special character.',
  },
  length: { max: 20, min: 8 },
  required: {
    errorMessage: 'Password required',
  },
};

const PHONE_NUMBER_INPUT_VALIDATION = {
  regex: {
    value: /^[+]?\d{12,15}$/,
    errorMessage: 'Invalid phone number. Must be at least 12 digits long.',
  },
  length: { max: 15, min: 0 },
  required: {
    errorMessage: 'Mobile number required',
  },
};

const CONTENT_CATEGORY_INPUT_VALIDATION = {
  required: {
    errorMessage: 'Content category required',
  },
};

const CONTENT_SUB_CATEGORY_INPUT_VALIDATION = {
  required: {
    errorMessage: 'Content sub category required',
  },
};

const RECAPTCHA_INPUT_VALIDATION = {
  required: {
    errorMessage: 'Recaptcha required',
  },
};

export const YUP_FIRSTNAME_VALIDATOR = yup
  .string()
  .matches(
    FIRST_NAME_INPUT_VALIDATION.regex.value,
    FIRST_NAME_INPUT_VALIDATION.regex.errorMessage
  )
  .required(FIRST_NAME_INPUT_VALIDATION.required.errorMessage);

export const YUP_PASSWORD_VALIDATOR = yup
  .string()
  .max(PASSWORD_INPUT_VALIDATION.length.max)
  .min(PASSWORD_INPUT_VALIDATION.length.min)
  .matches(
    PASSWORD_INPUT_VALIDATION.regex.value,
    PASSWORD_INPUT_VALIDATION.regex.errorMessage
  )
  .required(PASSWORD_INPUT_VALIDATION.required.errorMessage);

export const YUP_CONTENT_CATEGORY_VALIDATOR = yup
  .object()
  .nullable()
  .required(CONTENT_CATEGORY_INPUT_VALIDATION.required.errorMessage);

export const YUP_CONTENT_SUB_CATEGORY_VALIDATOR = yup
  .object()
  .nullable()
  .required(CONTENT_SUB_CATEGORY_INPUT_VALIDATION.required.errorMessage);

export const YUP_RECAPTCHA_VALIDATOR = yup
  .string()
  .required(RECAPTCHA_INPUT_VALIDATION.required.errorMessage);

export const YUP_PHONE_NUMBER_VALIDATOR = yup
  .string()
  .nullable()
  .required(PHONE_NUMBER_INPUT_VALIDATION.required.errorMessage)
  .max(PHONE_NUMBER_INPUT_VALIDATION.length.max)
  .min(PHONE_NUMBER_INPUT_VALIDATION.length.min)
  .matches(
    PHONE_NUMBER_INPUT_VALIDATION.regex.value,
    PHONE_NUMBER_INPUT_VALIDATION.regex.errorMessage
  );
