// index.js

import SortOrderJson from './sort-order.json';
import NotificationJson from './notification.json';
import AccountJson from './account.json';
import RoleJson from './role.json';
import RepairJson from './repair.json';
import ServiceDeliveryModeJson from './service-delivery-mode.json';
import WorkingDayJson from './working-day.json';
import FileStorageProviderJson from './file-storage-provider.json';
import FileLocationJson from './file-location.json';
import PaymentJson from './payment.json';
import PaymentProviderJson from './payment-provider.json';
import ApiActionsJson from './api-actions.json';

const { SortOrder } = SortOrderJson;
const { NotificationChannel, NotificationStatus, NotificationType } =
  NotificationJson;
const { AccountStatus } = AccountJson;
const { RoleType } = RoleJson;
const { RepairStatus, RepairPaymentStatus, RepairDeliveryStatus } = RepairJson;
const { ServiceDeliveryMode } = ServiceDeliveryModeJson;
const { WorkingDay } = WorkingDayJson;
const { FileStorageProvider } = FileStorageProviderJson;
const { FileLocation } = FileLocationJson;
const { PaymentRequestStatus } = PaymentJson;
const { PaymentProvider } = PaymentProviderJson;
const { ApiActions } = ApiActionsJson;

export {
  SortOrder,
  NotificationChannel,
  NotificationStatus,
  NotificationType,
  AccountStatus,
  RoleType,
  RepairStatus,
  RepairPaymentStatus,
  RepairDeliveryStatus,
  ServiceDeliveryMode,
  WorkingDay,
  FileStorageProvider,
  FileLocation,
  PaymentRequestStatus,
  PaymentProvider,
  ApiActions,
};
