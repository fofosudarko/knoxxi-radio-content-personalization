import { useCallback, useRef, useState } from 'react';
import useRoutes from 'src/hooks/routes';

export function useRedirectRoute() {
  const [error, setError] = useState(null);
  const { useHomeRoute } = useRoutes();
  const { handleHomeRoute } = useHomeRoute();

  const handleRedirectRoute = useCallback(() => {
    try {
      handleHomeRoute();
    } catch (error) {
      setError(error);
    }
  }, [handleHomeRoute]);

  return {
    error,
    setError,
    handleRedirectRoute,
  };
}

export function usePager(options = null) {
  const {
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging,
    resetPage,
    listKey,
  } = options ?? {};

  const pageRef = useRef(_page);
  const isPageNull = _page === null || _page === undefined;

  let page = isPageNull ? 1 : _page,
    currentPage = isPageNull ? 1 : pageRef.current,
    endPaging = isPageNull ? false : _endPaging;

  const handleInitializePage = useCallback(() => {
    listKey ? _setPage({ key: listKey, item: 1 }) : _setPage(1);
    listKey ? setEndPaging({ key: listKey, item: false }) : setEndPaging(false);
  }, [_setPage, listKey, setEndPaging]);

  const handleEndPaging = useCallback(() => {
    listKey ? setEndPaging({ key: listKey, item: true }) : setEndPaging(true);
  }, [listKey, setEndPaging]);

  const setPage = useCallback(
    (page) => {
      listKey ? _setPage({ key: listKey, item: page }) : _setPage(page);
    },
    [_setPage, listKey]
  );

  const handleResetPage = useCallback(() => {
    listKey ? resetPage({ key: listKey }) : resetPage();
  }, [listKey, resetPage]);

  return {
    page,
    currentPage,
    endPaging,
    isPageNull,
    pageRef,
    handleInitializePage,
    handleEndPaging,
    setPage,
    handleResetPage,
  };
}
