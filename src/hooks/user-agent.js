import { useEffect, useState } from 'react';

export default function useUserAgent() {
  const [userAgent, setUserAgent] = useState(null);
  useEffect(() => {
    if (window.navigator.userAgent) {
      setUserAgent(window.navigator.userAgent);
    }
  }, []);
  return userAgent;
}
