export default function useAppleDeviceTester(userAgent = null) {
  return new RegExp('(Mac|Macintosh|iPhone|iPad)', 'i').test(userAgent);
}
