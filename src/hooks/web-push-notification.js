import { useCallback, useEffect, useRef } from 'react';

import { urlBase64ToUint8Array, alertWebError } from 'src/utils';
import { WEB_PUSH_NOTIFICATION_APPLICATION_SERVER_KEY } from 'src/config';
import { useWebPushNotificationStore } from 'src/stores/web-push-notification';
import { useAuthStore } from 'src/stores/auth';
import useRoutes from './routes';
import { useCreateOrUpdateWebPushClient } from './api';
import useUserAgent from './user-agent';

export default function useWebPushNotification() {
  const { router } = useRoutes();
  const {
    setWebPushNotificationSubscription,
    setWebPushSubscription,
    webPushNotificationSubscription,
    webPushSubscription,
  } = useWebPushNotificationStore((state) => state);
  const { appUser } = useAuthStore((state) => state);
  const {
    handleCreateOrUpdateWebPushClient,
    webPushClient,
    webPushClientCreated,
    error: webPushClientError,
    setError: setWebPushClientError,
    setWebPushClient,
    setWebPushClientCreated,
  } = useCreateOrUpdateWebPushClient();
  const userAgent = useUserAgent();
  const webPushClientRef = useRef(webPushClient);
  const webPushClientCreatingRef = useRef(false);
  let handleApiResult;

  useEffect(() => {
    if (webPushClientError) {
      webPushClientCreatingRef.current = false;
    }
    return () => {
      setWebPushClientError(null);
    };
  }, [setWebPushClientError, webPushClientError]);

  useEffect(() => {
    if (webPushClientCreated) {
      handleApiResult();
    }
  }, [handleApiResult, webPushClientCreated]);

  useEffect(() => {
    if (
      appUser &&
      webPushNotificationSubscription &&
      !webPushClientCreatingRef.current
    ) {
      webPushClientCreatingRef.current = true;
      (async () => {
        await handleCreateOrUpdateWebPushClient(
          webPushNotificationSubscription
        );
      })();
    }
  }, [
    appUser,
    handleCreateOrUpdateWebPushClient,
    webPushNotificationSubscription,
  ]);

  handleApiResult = useCallback(() => {
    webPushClientRef.current = webPushClient;
    setWebPushClient(null);
    setWebPushClientCreated(false);
    setWebPushNotificationSubscription(null);
    webPushClientCreatingRef.current = false;
  }, [
    setWebPushClient,
    setWebPushClientCreated,
    setWebPushNotificationSubscription,
    webPushClient,
  ]);

  const handlePushNotificationSubscriptionAction = useCallback(
    (serviceWorkerRegistration) => {
      if (appUser && serviceWorkerRegistration.pushManager) {
        serviceWorkerRegistration.pushManager
          .subscribe({
            userVisibleOnly: true,
            applicationServerKey: urlBase64ToUint8Array(
              WEB_PUSH_NOTIFICATION_APPLICATION_SERVER_KEY
            ),
          })
          .then(async (pushSubscription) => {
            console.log('Push subscription:', pushSubscription);
            const key = pushSubscription.getKey
              ? pushSubscription.getKey('p256dh')
              : '';
            const auth = pushSubscription.getKey
              ? pushSubscription.getKey('auth')
              : '';
            const keyAsString = key
              ? window.btoa(
                  String.fromCharCode.apply(null, new window.Uint8Array(key))
                )
              : '';
            const authAsString = auth
              ? window.btoa(
                  String.fromCharCode.apply(null, new window.Uint8Array(auth))
                )
              : '';
            const subscriptionInfo = {
              endpoint: pushSubscription.endpoint,
              expirationTime: pushSubscription.expirationTime,
              key: keyAsString,
              auth: authAsString,
            };
            const body = {
              name: `${appUser.account.customerName}'s browser`,
              userAgent,
              uuid: appUser.webPushUuid ?? null,
              ownedById: Number.parseInt(appUser.account.customerId),
              subscriptionInfo,
            };
            setWebPushSubscription({
              endpoint: pushSubscription.endpoint,
              auth: authAsString,
              p256dh: keyAsString,
              expirationTime: pushSubscription.expirationTime,
            });
            setWebPushNotificationSubscription(body);
          })
          .catch((error) => {
            alertWebError(error);
          });
      }
    },
    [
      appUser,
      setWebPushNotificationSubscription,
      setWebPushSubscription,
      userAgent,
    ]
  );
  const handleWebPushNotificationSubscription = useCallback(
    (serviceWorkerRegistration) => {
      if (window.Notification && window.Notification.permission === 'granted') {
        if (
          serviceWorkerRegistration.pushManager &&
          serviceWorkerRegistration.pushManager.getSubscription
        ) {
          serviceWorkerRegistration.pushManager
            .getSubscription()
            .then((pushSubscription) => {
              console.log('Push subscription:', pushSubscription);
              if (pushSubscription == null) {
                handlePushNotificationSubscriptionAction(
                  serviceWorkerRegistration
                );
              } else {
                const key = pushSubscription.getKey
                  ? pushSubscription.getKey('p256dh')
                  : '';
                const keyAsString = key
                  ? window.btoa(
                      String.fromCharCode.apply(
                        null,
                        new window.Uint8Array(key)
                      )
                    )
                  : '';
                if (keyAsString !== webPushSubscription?.p256dh) {
                  pushSubscription
                    .unsubscribe()
                    .then((successful) => {
                      // You've successfully unsubscribed
                      if (successful) {
                        handlePushNotificationSubscriptionAction(
                          serviceWorkerRegistration
                        );
                      }
                    })
                    .catch((error) => {
                      // Unsubscribing failed
                      alertWebError(error);
                    });
                }
              }
            })
            .catch((error) => {
              alertWebError(error);
            });
        }
      } else {
        handlePushNotificationSubscriptionAction(serviceWorkerRegistration);
      }
    },
    [handlePushNotificationSubscriptionAction, webPushSubscription]
  );
  const handleInitializeWebNotification = useCallback(
    (serviceWorkerRegistration) => {
      if (
        appUser &&
        window.Notification &&
        (window.Notification.permission === 'default' ||
          window.Notification.permission === 'denied')
      ) {
        window.Notification.requestPermission()
          .then((permission) => {
            if (permission === 'denied') {
              alert(
                'Kindly enable notifications so we can send you push notifications.'
              );
            } else if (permission === 'granted') {
              serviceWorkerRegistration.showNotification(
                'Knoxxi Radio Content Personalization',
                {
                  body: 'Push notifications enabled. Thank you.',
                }
              );
              router.reload();
            }
          })
          .catch((error) => {
            alertWebError(new Error(`Notification permission error: ${error}`));
          });
      }
    },
    [appUser, router]
  );

  return {
    handleWebPushNotificationSubscription,
    handleInitializeWebNotification,
  };
}
