import { useCallback } from 'react';

import { NotificationService } from 'src/api/app/radio';
import useItem from '../item';

export function useCreateOrUpdateWebPushClient() {
  const {
    error,
    setError,
    processing,
    setProcessing,
    item: webPushClient,
    setItem: setWebPushClient,
    itemCreated: webPushClientCreated,
    setItemCreated: setWebPushClientCreated,
  } = useItem();

  const handleCreateOrUpdateWebPushClient = useCallback(
    async (body = null, params = null) => {
      try {
        setProcessing(true);
        const response = await NotificationService.createOrUpdateWebPushClient(
          body,
          params
        );
        const data = await response.json();
        if (response.ok) {
          const newWebPushClient = data.data;
          setWebPushClient(newWebPushClient);
          setWebPushClientCreated(true);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [setWebPushClient, setWebPushClientCreated, setError, setProcessing]
  );

  return {
    handleCreateOrUpdateWebPushClient,
    error,
    setError,
    processing,
    setProcessing,
    webPushClient,
    setWebPushClient,
    webPushClientCreated,
    setWebPushClientCreated,
  };
}
