import { useCallback } from 'react';

import { AdvertPlayService } from 'src/api/app/radio';

import useItem from '../item';

export function useCreateAdvertPlay() {
  const {
    error,
    setError,
    processing,
    setProcessing,
    item: advertPlay,
    setItem: setAdvertPlay,
    itemCreated: advertPlayCreated,
    setItemCreated: setAdvertPlayCreated,
  } = useItem();

  const handleCreateAdvertPlay = useCallback(
    async (body = null) => {
      try {
        setProcessing(true);
        const response = await AdvertPlayService.createAdvertPlay(body);
        const data = await response.json();
        if (response.ok) {
          const newAdvertPlay = data.data;
          setAdvertPlay(newAdvertPlay);
          setAdvertPlayCreated(true);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [setAdvertPlay, setAdvertPlayCreated, setError, setProcessing]
  );

  return {
    handleCreateAdvertPlay,
    error,
    setError,
    processing,
    setProcessing,
    advertPlay,
    setAdvertPlay,
    advertPlayCreated,
    setAdvertPlayCreated,
  };
}
