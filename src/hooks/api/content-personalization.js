import { useCallback, useEffect } from 'react';

import { ContentPersonalizationService } from 'src/api/app/radio';
import { useContentPersonalizationStore } from 'src/stores/content-personalization';
import { useContentPersonalizationPublicStore } from 'src/stores/content-personalization-public';
import useItem from '../item';
import { generateListKey } from 'src/utils';
import { usePager } from '../page';

export function useListContentPersonalizations(options = {}) {
  const { carrierDriverInfoId } = options;
  const listKey = generateListKey({ carrierDriverInfoId });
  const {
    contentPersonalizations: _contentPersonalizations,
    contentPersonalizationPage: __page,
    setContentPersonalizationPage: _setPage,
    contentPersonalizationEndPaging: __endPaging,
    setContentPersonalizationEndPaging: _setEndPaging,
    resetContentPersonalizationPage: resetPage,
    listContentPersonalizations,
  } = useContentPersonalizationStore((state) => state);
  const { error, setError } = useItem();
  const contentPersonalizations = _contentPersonalizations
    ? _contentPersonalizations[listKey]
    : _contentPersonalizations;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!contentPersonalizations?.length) {
      (async () => {
        await handleListContentPersonalizations({
          page,
          carrierDriverInfoId,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contentPersonalizations?.length]);

  useEffect(() => {
    if (contentPersonalizations?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListContentPersonalizations({
          page,
          carrierDriverInfoId,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListContentPersonalizations = useCallback(
    async ({ page = null, carrierDriverInfoId }) => {
      try {
        const response =
          await ContentPersonalizationService.listContentPersonalizations({
            page,
            carrierDriverInfoId,
          });
        const data = await response.json();
        if (response.ok) {
          const contentPersonalizations = data.data;
          if (contentPersonalizations?.length || page === 1) {
            listContentPersonalizations({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listContentPersonalizations,
      listKey,
      setError,
    ]
  );

  return {
    handleListContentPersonalizations,
    contentPersonalizations,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useCreateContentPersonalization() {
  const {
    error,
    setError,
    processing,
    setProcessing,
    item: contentPersonalization,
    setItem: setContentPersonalization,
    itemCreated: contentPersonalizationCreated,
    setItemCreated: setContentPersonalizationCreated,
  } = useItem();

  const handleCreateContentPersonalization = useCallback(
    async (body = null, params = null) => {
      try {
        setProcessing(true);
        const response =
          await ContentPersonalizationService.createContentPersonalization(
            body,
            params
          );
        const data = await response.json();
        if (response.ok) {
          const newContentPersonalization = data.data;
          setContentPersonalization(newContentPersonalization);
          setContentPersonalizationCreated(true);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      setContentPersonalization,
      setContentPersonalizationCreated,
      setError,
      setProcessing,
    ]
  );

  return {
    handleCreateContentPersonalization,
    error,
    setError,
    processing,
    setProcessing,
    contentPersonalization,
    setContentPersonalization,
    contentPersonalizationCreated,
    setContentPersonalizationCreated,
  };
}

export function useGetContentPersonalizationPublic({
  contentPersonalization: _contentPersonalization = null,
  ignoreLoadOnMount = false,
}) {
  const { contentPersonalization, setContentPersonalization } =
    useContentPersonalizationPublicStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!ignoreLoadOnMount) {
      (async () => {
        await handleGetContentPersonalizationPublic(_contentPersonalization);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetContentPersonalizationPublic = useCallback(
    async (contentPersonalization = null) => {
      try {
        const response =
          await ContentPersonalizationService.getContentPersonalizationPublic(
            contentPersonalization
          );
        const data = await response.json();
        if (response.ok) {
          const contentPersonalization = data.data;
          setContentPersonalization(contentPersonalization);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setContentPersonalization, setError]
  );

  return {
    handleGetContentPersonalizationPublic,
    error,
    setError,
    processing,
    setProcessing,
    contentPersonalization,
  };
}

export function useResetContentPersonalizations(options = {}) {
  const { carrierDriverInfoId } = options;
  const listKey = generateListKey({ carrierDriverInfoId });
  const {
    resetContentPersonalizationPage: resetPage,
    clearContentPersonalizations,
  } = useContentPersonalizationStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetContentPersonalizations = useCallback(() => {
    handleResetPage();
    clearContentPersonalizations({ key: listKey });
  }, [handleResetPage, clearContentPersonalizations, listKey]);

  return handleResetContentPersonalizations;
}
