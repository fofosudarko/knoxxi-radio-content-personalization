import { useCallback, useReducer } from 'react';
import { produce } from 'immer';

import { EnumService } from 'src/api/app/radio';

const types = {
  SET_CONTENT_CATEGORY: 'SET_CONTENT_CATEGORY',
  SET_CONTENT_SUB_CATEGORY: 'SET_CONTENT_SUB_CATEGORY',
  CLEAR_ENUM_STATE: 'CLEAR_ENUM_STATE',
  SET_ERROR: 'SET_ERROR',
};

const initialState = {
  contentCategories: null,
  contentSubCategories: null,
  error: null,
};

const reducer = produce((draft, action) => {
  switch (action.type) {
    case types.SET_CONTENT_CATEGORY:
      draft.contentCategories = action.payload;
      return;
    case types.SET_CONTENT_SUB_CATEGORY:
      draft.contentSubCategories = action.payload;
      return;
    case types.SET_ERROR:
      draft.error = action.payload;
      return;
    case types.CLEAR_ENUM_STATE:
      return initialState;
    default:
      return draft;
  }
});

export function useListEnums() {
  const [{ error, contentCategories, contentSubCategories }, dispatch] =
    useReducer(reducer, initialState);

  const setContentCategory = useCallback((contentCategories) => {
    dispatch({ type: types.SET_CONTENT_CATEGORY, payload: contentCategories });
  }, []);
  const setContentSubCategory = useCallback((contentSubCategories) => {
    dispatch({
      type: types.SET_CONTENT_SUB_CATEGORY,
      payload: contentSubCategories,
    });
  }, []);
  const setError = useCallback((error) => {
    dispatch({ type: types.SET_ERROR, payload: error });
  }, []);

  const handleListContentCategory = useCallback(
    async (isFiltered) => {
      try {
        const response = await EnumService.listContentCategory(isFiltered);
        const data = await response.json();
        if (response.ok) {
          setContentCategory(data.data);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setContentCategory, setError]
  );

  const handleListContentSubCategory = useCallback(
    async (contentCategory, isFiltered) => {
      try {
        const response = await EnumService.listContentSubCategory(
          contentCategory,
          isFiltered
        );
        const data = await response.json();
        if (response.ok) {
          setContentSubCategory(data.data);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [setError, setContentSubCategory]
  );

  return {
    handleListContentCategory,
    contentCategories,
    setContentCategory,
    handleListContentSubCategory,
    contentSubCategories,
    setContentSubCategory,
    error,
    setError,
  };
}
