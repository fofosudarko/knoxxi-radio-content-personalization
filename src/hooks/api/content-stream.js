import { useCallback } from 'react';

import { ContentStreamService } from 'src/api/app/radio';

import useItem from '../item';

export function useCreateContentStream() {
  const {
    error,
    setError,
    processing,
    setProcessing,
    item: contentStream,
    setItem: setContentStream,
    itemCreated: contentStreamCreated,
    setItemCreated: setContentStreamCreated,
  } = useItem();

  const handleCreateContentStream = useCallback(
    async (contentStream = null, streamedBy = null) => {
      try {
        setProcessing(true);
        const response = await ContentStreamService.createContentStream(
          contentStream,
          streamedBy
        );
        const data = await response.json();
        if (response.ok) {
          const newContentStream = data.data;
          setContentStream(newContentStream);
          setContentStreamCreated(true);
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [setContentStream, setContentStreamCreated, setError, setProcessing]
  );

  return {
    handleCreateContentStream,
    error,
    setError,
    processing,
    setProcessing,
    contentStream,
    setContentStream,
    contentStreamCreated,
    setContentStreamCreated,
  };
}
