import { useCallback, useEffect } from 'react';

import { CarrierCampaignService } from 'src/api/app/radio';

import { usePager } from '../page';
import { useCarrierCampaignStore } from 'src/stores/carrier-campaign';
import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListCarrierCampaigns(options = {}) {
  const { carrierId, status } = options;
  const listKey = generateListKey({ carrierId, status });
  const {
    carrierCampaigns: _carrierCampaigns,
    carrierCampaignPage: __page,
    setCarrierCampaignPage: _setPage,
    carrierCampaignEndPaging: __endPaging,
    setCarrierCampaignEndPaging: _setEndPaging,
    resetCarrierCampaignPage: resetPage,
    listCarrierCampaigns,
  } = useCarrierCampaignStore((state) => state);
  const { error, setError } = useItem();
  const carrierCampaigns = _carrierCampaigns
    ? _carrierCampaigns[listKey]
    : _carrierCampaigns;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!carrierCampaigns?.length) {
      (async () => {
        await handleListCarrierCampaigns({
          carrierId,
          status,
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [carrierCampaigns?.length]);

  useEffect(() => {
    if (carrierCampaigns?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListCarrierCampaigns({
          carrierId,
          status,
          page,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, carrierId]);

  const handleListCarrierCampaigns = useCallback(
    async ({ page = null, carrierId, status }) => {
      try {
        const response =
          await CarrierCampaignService.listCarrierCampaignsPublic({
            carrierId,
            status,
            page,
          });
        const data = await response.json();
        if (response.ok) {
          const carrierCampaigns = data.data;
          if (carrierCampaigns?.length || page === 1) {
            listCarrierCampaigns({ key: listKey, items: data.data });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listCarrierCampaigns,
      listKey,
      setError,
    ]
  );

  return {
    handleListCarrierCampaigns,
    carrierCampaigns,
    error,
    page,
    setPage,
    setError,
    endPaging,
  };
}

export function useResetCarrierCampaigns(options = null) {
  const { carrierId, status } = options;
  const listKey = generateListKey({ carrierId, status });
  const { resetCarrierCampaignPage: resetPage, clearCarrierCampaigns } =
    useCarrierCampaignStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetCarrierCampaigns = useCallback(() => {
    handleResetPage();
    clearCarrierCampaigns({ key: listKey });
  }, [handleResetPage, clearCarrierCampaigns, listKey]);

  return handleResetCarrierCampaigns;
}
