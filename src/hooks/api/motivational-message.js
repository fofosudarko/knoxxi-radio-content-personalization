import { useCallback, useEffect } from 'react';

import { usePager } from '../page';
import { useMotivationalMessageStreamListStore } from 'src/stores/motivational-message-stream-list';
import { MotivationalMessageService } from 'src/api/app/radio';

import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListMotivationalMessageStreamList(options = {}) {
  const { streamedById } = options;
  const listKey = generateListKey({ streamedById });
  const {
    motivationalMessageStreamList: _motivationalMessageStreamList,
    motivationalMessageStreamListPage: __page,
    setMotivationalMessageStreamListPage: _setPage,
    motivationalMessageStreamListEndPaging: __endPaging,
    setMotivationalMessageStreamListEndPaging: _setEndPaging,
    resetMotivationalMessageStreamListPage: resetPage,
    listMotivationalMessageStreamList,
    motivationalMessageStreamAdverts: _motivationalMessageStreamAdverts,
    setMotivationalMessageStreamAdverts,
    motivationalMessageStreamScripts: _motivationalMessageStreamScripts,
    setMotivationalMessageStreamScripts,
  } = useMotivationalMessageStreamListStore((state) => state);
  const { error, setError } = useItem();
  const motivationalMessageStreamList = _motivationalMessageStreamList
    ? _motivationalMessageStreamList[listKey]
    : _motivationalMessageStreamList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;
  const motivationalMessageStreamAdverts = _motivationalMessageStreamAdverts
    ? _motivationalMessageStreamAdverts[listKey]
    : _motivationalMessageStreamAdverts;
  const motivationalMessageStreamScripts = _motivationalMessageStreamScripts
    ? _motivationalMessageStreamScripts[listKey]
    : _motivationalMessageStreamScripts;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!motivationalMessageStreamList?.length) {
      (async () => {
        await handleListMotivationalMessageStreamList({
          page,
          streamedById,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [motivationalMessageStreamList?.length]);

  useEffect(() => {
    if (
      motivationalMessageStreamList?.length &&
      currentPage &&
      currentPage < page
    ) {
      (async () => {
        await handleListMotivationalMessageStreamList({
          page,
          streamedById,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListMotivationalMessageStreamList = useCallback(
    async ({ page = null, streamedById }) => {
      try {
        const response =
          await MotivationalMessageService.listMotivationalMessageStreamList({
            streamedById,
            page,
          });
        const data = await response.json();
        if (response.ok) {
          const motivationalMessageStreamList =
            data.data.motivationalMessageStreamList;
          const motivationalMessageStreamAdverts =
            data.data.motivationalMessageStreamAdverts;
          const motivationalMessageStreamScripts = data.data.streamedBy.scripts;
          if (motivationalMessageStreamList?.length || page === 1) {
            listMotivationalMessageStreamList({
              key: listKey,
              items: motivationalMessageStreamList,
            });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
          setMotivationalMessageStreamAdverts({
            key: listKey,
            items: motivationalMessageStreamAdverts,
          });
          setMotivationalMessageStreamScripts({
            key: listKey,
            items: motivationalMessageStreamScripts,
          });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listMotivationalMessageStreamList,
      setError,
      setMotivationalMessageStreamAdverts,
      setMotivationalMessageStreamScripts,
    ]
  );

  return {
    handleListMotivationalMessageStreamList,
    motivationalMessageStreamList,
    error,
    page,
    setPage,
    setError,
    endPaging,
    motivationalMessageStreamAdverts,
    motivationalMessageStreamScripts,
  };
}

export function useResetMotivationalMessageStreamList(options = {}) {
  const { streamedById } = options;
  const listKey = generateListKey({ streamedById });
  const {
    resetMotivationalMessageStreamListPage: resetPage,
    clearMotivationalMessageStreamList,
  } = useMotivationalMessageStreamListStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetMotivationalMessageStreamList = useCallback(() => {
    handleResetPage();
    clearMotivationalMessageStreamList({ key: listKey });
  }, [handleResetPage, clearMotivationalMessageStreamList, listKey]);

  return handleResetMotivationalMessageStreamList;
}
