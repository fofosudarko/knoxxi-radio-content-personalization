import { useCallback } from 'react';

import CkycService from 'src/api/app/ckyc';

import { useAuthStore } from 'src/stores/auth';

import useItem from '../item';
import { useWebPushUuid } from '../web-push-uuid';

export function useCKYCLogout() {
  const { ckycLogout, setCkycLogout } = useAuthStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();

  const handleCKYCLogout = useCallback(
    async (body = null) => {
      try {
        setProcessing(true);
        const response = await CkycService.logOut(body);
        const data = await response.json();
        if (response.ok) {
          setCkycLogout(data);
        } else {
          setError(data?.possibleCause);
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [setCkycLogout, setError, setProcessing]
  );

  return {
    handleCKYCLogout,
    error,
    setError,
    processing,
    setProcessing,
    ckycLogout,
  };
}

export function useCKYCSignIn() {
  const { ckycSignIn, setCkycSignIn: _setCkycSignIn } = useAuthStore(
    (state) => state
  );
  const { error, setError, processing, setProcessing } = useItem();

  const setCkycSignIn = useCallback(
    (ckycSignIn) => {
      _setCkycSignIn(ckycSignIn);
    },
    [_setCkycSignIn]
  );

  const handleCKYCSignIn = useCallback(
    async (body = null) => {
      try {
        setProcessing(true);
        const response = await CkycService.signIn(body);
        const data = await response.json();
        if (response.ok) {
          setCkycSignIn(data);
        } else {
          setError(new Error(data?.possibleCause));
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [setCkycSignIn, setError, setProcessing]
  );

  return {
    handleCKYCSignIn,
    error,
    setError,
    processing,
    setProcessing,
    ckycSignIn,
    setCkycSignIn,
  };
}

export function useCKYCProcessSignIn(options = {}) {
  const { appUser = null } = options;
  const { setAppUser, setAppUserAccessToken, clearAppUserRegistration } =
    useAuthStore((state) => state);
  const { error, setError, processing, setProcessing } = useItem();
  const webPushUuidRef = useWebPushUuid();

  const handleCKYCProcessSignIn = useCallback(
    async (signInResponse = null) => {
      try {
        setProcessing(true);
        const { securityToken, result, customerId, customerName, requestId } =
          signInResponse ?? {};
        if (result) {
          console.log('Sign In Request ID: ', requestId);
          setAppUserAccessToken({ account: { token: securityToken } });
          setAppUser({
            account: {
              ...appUser,
              customerId,
              customerName,
            },
            webPushUuid: webPushUuidRef.current,
          });
        }
      } catch (error) {
        setError(error);
      }
      setProcessing(false);
    },
    [
      appUser,
      setAppUser,
      setAppUserAccessToken,
      setError,
      setProcessing,
      webPushUuidRef,
    ]
  );

  const handleClearAppUserRegistration = useCallback(() => {
    clearAppUserRegistration();
  }, [clearAppUserRegistration]);

  return {
    handleCKYCProcessSignIn,
    handleClearAppUserRegistration,
    error,
    setError,
    processing,
    setProcessing,
  };
}
