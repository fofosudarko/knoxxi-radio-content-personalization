import { useCallback, useEffect } from 'react';

import { usePager } from '../page';
import { useMusicStreamListStore } from 'src/stores/music-stream-list';
import { MusicService } from 'src/api/app/radio';

import useItem from '../item';
import { generateListKey } from 'src/utils';

export function useListMusicStreamList(options = {}) {
  const { streamedById } = options;
  const listKey = generateListKey({ streamedById });
  const {
    musicStreamList: _musicStreamList,
    musicStreamListPage: __page,
    setMusicStreamListPage: _setPage,
    musicStreamListEndPaging: __endPaging,
    setMusicStreamListEndPaging: _setEndPaging,
    resetMusicStreamListPage: resetPage,
    listMusicStreamList,
    musicStreamAdverts: _musicStreamAdverts,
    setMusicStreamAdverts,
    musicStreamScripts: _musicStreamScripts,
    setMusicStreamScripts,
  } = useMusicStreamListStore((state) => state);
  const { error, setError } = useItem();
  const musicStreamList = _musicStreamList
    ? _musicStreamList[listKey]
    : _musicStreamList;
  const _page = __page ? __page[listKey] : __page;
  const _endPaging = __endPaging ? __endPaging[listKey] : __endPaging;
  const musicStreamAdverts = _musicStreamAdverts
    ? _musicStreamAdverts[listKey]
    : _musicStreamAdverts;
  const musicStreamScripts = _musicStreamScripts
    ? _musicStreamScripts[listKey]
    : _musicStreamScripts;

  const {
    page,
    endPaging,
    pageRef,
    currentPage,
    handleInitializePage,
    isPageNull,
    handleEndPaging,
    setPage,
  } = usePager({
    page: _page,
    endPaging: _endPaging,
    setPage: _setPage,
    setEndPaging: _setEndPaging,
    resetPage,
    listKey,
  });

  useEffect(() => {
    if (!musicStreamList?.length) {
      (async () => {
        await handleListMusicStreamList({
          page,
          streamedById,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [musicStreamList?.length]);

  useEffect(() => {
    if (musicStreamList?.length && currentPage && currentPage < page) {
      (async () => {
        await handleListMusicStreamList({
          page,
          streamedById,
        });
        pageRef.current = page;
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const handleListMusicStreamList = useCallback(
    async ({ page = null, streamedById }) => {
      try {
        const response = await MusicService.listMusicStreamList({
          streamedById,
          page,
        });
        const data = await response.json();
        if (response.ok) {
          const musicStreamList = data.data.musicStreamList;
          const musicStreamAdverts = data.data.musicStreamAdverts;
          const musicStreamScripts = data.data.streamedBy.scripts;
          if (musicStreamList?.length || page === 1) {
            listMusicStreamList({ key: listKey, items: musicStreamList });
            isPageNull && handleInitializePage();
          } else {
            handleEndPaging();
          }
          setMusicStreamAdverts({ key: listKey, items: musicStreamAdverts });
          setMusicStreamScripts({ key: listKey, items: musicStreamScripts });
        } else {
          setError(data.error);
        }
      } catch (error) {
        setError(error);
      }
    },
    [
      handleEndPaging,
      handleInitializePage,
      isPageNull,
      listKey,
      listMusicStreamList,
      setError,
      setMusicStreamAdverts,
      setMusicStreamScripts,
    ]
  );

  return {
    handleListMusicStreamList,
    musicStreamList,
    error,
    page,
    setPage,
    setError,
    endPaging,
    musicStreamAdverts,
    musicStreamScripts,
  };
}

export function useResetMusicStreamList(options = {}) {
  const { streamedById } = options;
  const listKey = generateListKey({ streamedById });
  const { resetMusicStreamListPage: resetPage, clearMusicStreamList } =
    useMusicStreamListStore((state) => state);

  const { handleResetPage } = usePager({
    resetPage,
    listKey,
  });

  const handleResetMusicStreamList = useCallback(() => {
    handleResetPage();
    clearMusicStreamList({ key: listKey });
  }, [handleResetPage, clearMusicStreamList, listKey]);

  return handleResetMusicStreamList;
}
