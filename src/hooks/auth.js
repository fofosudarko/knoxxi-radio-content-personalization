// auth.js

import { useEffect, useState, useCallback, useContext } from 'react';
import httpStatus from 'http-status';

import { isAxiosError } from 'src/utils';
import useRoutes from './routes';
import useNotificationHandler from './notification';
import { useRedirectRoute } from './page';
import { useAuthStore } from 'src/stores/auth';
import { useCarrierCampaignStore } from 'src/stores/carrier-campaign';
import { useContentPersonalizationStore } from 'src/stores/content-personalization';
import { useWebPushNotificationStore } from 'src/stores/web-push-notification';
import useHasMounted from './has-mounted';

import { AppContext } from 'src/context/app';

export function useAuth() {
  const handleSignOut = useSignOut();
  const { appUser } = useAuthStore((state) => state);
  const hasMounted = useHasMounted();

  const [error, setError] = useState(null);

  useEffect(() => {
    if (!appUser) {
      (async () => {
        try {
          await handleSignOut();
        } catch (error) {
          setError(error);
        }
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [appUser]);

  return { error, appUser, hasMounted };
}

export function useSignOut() {
  const { clearAppState } = useContext(AppContext);
  const { handleSignInRoute } = useRoutes().useSignInRoute();
  const handleNotification = useNotificationHandler();
  const clearAuthState = useAuthStore((state) => state.clearAuthState);
  const clearCarrierCampaignState = useCarrierCampaignStore(
    (state) => state.clearCarrierCampaignState
  );
  const clearContentPersonalizationState = useContentPersonalizationStore(
    (state) => state.clearContentPersonalizationState
  );
  const clearWebPushNotificationState = useWebPushNotificationStore(
    (state) => state.clearWebPushNotificationState
  );

  const handleSignOut = useCallback(async () => {
    try {
      handleSignInRoute();
    } catch (error) {
      handleNotification(error);
    }

    clearAuthState();
    clearCarrierCampaignState();
    clearContentPersonalizationState();
    clearWebPushNotificationState();
    clearAppState();
  }, [
    clearAuthState,
    clearCarrierCampaignState,
    clearContentPersonalizationState,
    clearWebPushNotificationState,
    clearAppState,
    handleSignInRoute,
    handleNotification,
  ]);

  return handleSignOut;
}

export function useHomeRedirect() {
  const appUser = useAuthStore((state) => state.appUser);
  const {
    handleRedirectRoute,
    error: redirectRouteError,
    setError: setRedirectRouteError,
  } = useRedirectRoute();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (redirectRouteError) {
      handleNotification(redirectRouteError);
    }

    return () => {
      setRedirectRouteError(null);
    };
  }, [handleNotification, redirectRouteError, setRedirectRouteError]);

  useEffect(() => {
    if (appUser?.account) {
      handleRedirectRoute();
    }
  }, [appUser, handleRedirectRoute]);
}

export function useForceSignOut() {
  const handleSignOut = useSignOut();

  const handleForceSignOut = useCallback(
    (error = null) => {
      if (
        error &&
        isAxiosError(error) &&
        error.response.status === httpStatus.FORBIDDEN
      ) {
        handleSignOut();
      }
    },
    [handleSignOut]
  );

  return handleForceSignOut;
}
