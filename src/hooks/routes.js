import { useCallback } from 'react';
import { useRouter } from 'next/router';
import { useAuthStore } from 'src/stores/auth';

const routes = {
  INDEX_ROUTE: '/',
  SIGNIN_ROUTE: '/auth/signin',
  SIGNOUT_ROUTE: '/auth/signout',
  HOME_ROUTE: '/home',
  CONTENT_PERSONALIZATION_NEW_ROUTE: '/content-personalizations/new',
  CONTENT_PERSONALIZATION_SHOW_ROUTE:
    '/content-personalizations/{{APP_CONTENT_PERSONALIZATION_ID}}',
  CARRIER_CAMPAIGNS_ROUTE: '/carrier-campaigns',
};

export default function useRoutes() {
  const router = useRouter();
  const appUser = useAuthStore((state) => state.appUser);
  const { account = null } = appUser ?? {};
  const username = account?.customerName;

  // handle back
  const handleBack = useCallback(() => {
    router.back();
  }, [router]);
  // handle reload
  const handleReload = useCallback(() => {
    router.reload();
  }, [router]);
  // use index route
  const useIndexRoute = useCallback(() => {
    const indexRoute = routes.INDEX_ROUTE;

    const handleIndexRoute = (options = {}) => {
      const { query = null } = options;
      router.push({ pathname: indexRoute, query });
    };

    return { indexRoute, handleIndexRoute };
  }, [router]);
  // use sign in route
  const useSignInRoute = useCallback(() => {
    const signInRoute = routes.SIGNIN_ROUTE;

    const handleSignInRoute = (options = {}) => {
      const { query = null } = options;
      router.push({ pathname: signInRoute, query });
    };

    return { signInRoute, handleSignInRoute };
  }, [router]);
  // use sign out route
  const useSignOutRoute = useCallback(() => {
    const signOutRoute = routes.SIGNOUT_ROUTE;

    const handleSignOutRoute = (options = {}) => {
      const { query = null } = options;
      router.replace({ pathname: signOutRoute, query });
    };

    return { signOutRoute, handleSignOutRoute };
  }, [router]);
  // use home route
  const useHomeRoute = useCallback(() => {
    const homeRoute = handleNamedRoute({
      route: routes.HOME_ROUTE,
      username,
    });

    const handleHomeRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: handleNamedRoute({
          route: homeRoute,
        }),
        query,
      });
    };

    return { homeRoute, handleHomeRoute };
  }, [router, username]);
  // use content personalization new route
  const useContentPersonalizationNewRoute = useCallback(() => {
    const contentPersonalizationNewRoute = handleNamedRoute({
      route: routes.CONTENT_PERSONALIZATION_NEW_ROUTE,
    });

    const handleContentPersonalizationNewRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: contentPersonalizationNewRoute,
        query,
      });
    };

    return {
      contentPersonalizationNewRoute,
      handleContentPersonalizationNewRoute,
    };
  }, [router]);
  // use content personalization show route
  const useContentPersonalizationShowRoute = useCallback(
    (contentPersonalization = null) => {
      const contentPersonalizationShowRoute = handleNamedRoute({
        route: routes.CONTENT_PERSONALIZATION_SHOW_ROUTE,
        contentPersonalization,
      });

      const handleContentPersonalizationShowRoute = (options = {}) => {
        const { query = null } = options;
        router.push({
          pathname: contentPersonalizationShowRoute,
          query,
        });
      };

      return {
        contentPersonalizationShowRoute,
        handleContentPersonalizationShowRoute,
      };
    },
    [router]
  );
  // use carrier campaigns route
  const useCarrierCampaignsRoute = useCallback(() => {
    const carrierCampaignsRoute = handleNamedRoute({
      route: routes.CARRIER_CAMPAIGNS_ROUTE,
    });

    const handleCarrierCampaignsRoute = (options = {}) => {
      const { query = null } = options;
      router.push({
        pathname: carrierCampaignsRoute,
        query,
      });
    };

    return {
      carrierCampaignsRoute,
      handleCarrierCampaignsRoute,
    };
  }, [router]);

  return {
    router,
    handleBack,
    handleReload,
    useIndexRoute,
    useSignInRoute,
    useSignOutRoute,
    useHomeRoute,
    useContentPersonalizationNewRoute,
    useContentPersonalizationShowRoute,
    useCarrierCampaignsRoute,
    routes,
  };
}

function handleNamedRoute({
  username,
  route = null,
  contentPersonalization = null,
}) {
  if (!route) {
    route = '';
  }

  const { id: contentPersonalizationId } = contentPersonalization ?? {};

  if (username) {
    route = route.replace('{{APP_USERNAME}}', username);
  }
  if (contentPersonalizationId) {
    route = route.replace(
      '{{APP_CONTENT_PERSONALIZATION_ID}}',
      contentPersonalizationId
    );
  }

  return route;
}
