import { useEffect } from 'react';

import { alertWebError } from 'src/utils';
import useWebPushNotification from './web-push-notification';

export default function useServiceWorkerLoader() {
  const {
    handleWebPushNotificationSubscription,
    handleInitializeWebNotification,
  } = useWebPushNotification();
  useEffect(() => {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker
        .register('/js/sw.js')
        .then((serviceWorkerRegistration) => {
          if (serviceWorkerRegistration.installing) {
            console.log('Installing service worker');
          } else if (serviceWorkerRegistration.waiting) {
            console.log('Service worker installed');
          } else if (serviceWorkerRegistration.active) {
            console.log('Service worker is now active');
            handleInitializeWebNotification(serviceWorkerRegistration);
            handleWebPushNotificationSubscription(serviceWorkerRegistration);
          }
        })
        .catch((error) => {
          alertWebError(
            new Error(`Loading service worker failed with ${error}`)
          );
        });
    }
  }, [handleInitializeWebNotification, handleWebPushNotificationSubscription]);
}
