import { useEffect, useRef } from 'react';
import { generateUUID } from 'src/utils';

import { useWebPushUuidStore } from 'src/stores/web-push-uuid';

export function useWebPushUuid() {
  const { webPushUuid, setWebPushUuid } = useWebPushUuidStore((state) => state);
  const webPushUuidRef = useRef(null);

  useEffect(() => {
    if (webPushUuid) {
      webPushUuidRef.current = webPushUuid;
    } else {
      setWebPushUuid(generateUUID());
    }
  }, [setWebPushUuid, webPushUuid]);

  return webPushUuidRef;
}
