import { useState, useCallback, useReducer } from 'react';
import { produce } from 'immer';

const initialState = {
  item: null,
  items: null,
  mediaItem: null,
  itemCreated: false,
  itemsCreated: false,
  mediaItemCreated: false,
  itemUpdated: false,
  itemsUpdated: false,
  mediaItemUpdated: false,
  itemRemoved: false,
  itemsRemoved: false,
  mediaItemRemoved: false,
};
const types = {
  SET_ITEM: 'SET_ITEM',
  SET_MEDIA_ITEM: 'SET_MEDIA_ITEM',
  SET_ITEMS: 'SET_ITEMS',
  SET_ITEM_CREATED: 'SET_ITEM_CREATED',
  SET_MEDIA_ITEM_CREATED: 'SET_MEDIA_ITEM_CREATED',
  SET_ITEMS_CREATED: 'SET_ITEMS_CREATED',
  SET_ITEM_UPDATED: 'SET_ITEM_UPDATED',
  SET_MEDIA_ITEM_UPDATED: 'SET_MEDIA_ITEM_UPDATED',
  SET_ITEMS_UPDATED: 'SET_ITEMS_UPDATED',
  SET_ITEM_REMOVED: 'SET_ITEM_REMOVED',
  SET_MEDIA_ITEM_REMOVED: 'SET_MEDIA_ITEM_REMOVED',
  SET_ITEMS_REMOVED: 'SET_ITEMS_REMOVED',
  CLEAR_ITEM_STATE: 'CLEAR_ITEM_STATE',
};
const reducer = produce((draft, action) => {
  switch (action.type) {
    case types.SET_ITEM:
      draft.item = action.payload;
      return;
    case types.SET_MEDIA_ITEM:
      draft.mediaItem = action.payload;
      return;
    case types.SET_ITEMS:
      draft.items = action.payload;
      return;
    case types.SET_ITEM_CREATED:
      draft.itemCreated = action.payload;
      return;
    case types.SET_MEDIA_ITEM_CREATED:
      draft.mediaItemCreated = action.payload;
      return;
    case types.SET_ITEMS_CREATED:
      draft.itemsCreated = action.payload;
      return;
    case types.SET_ITEM_UPDATED:
      draft.itemUpdated = action.payload;
      return;
    case types.SET_MEDIA_ITEM_UPDATED:
      draft.mediaItemUpdated = action.payload;
      return;
    case types.SET_ITEMS_UPDATED:
      draft.itemsUpdated = action.payload;
      return;
    case types.SET_ITEM_REMOVED:
      draft.itemRemoved = action.payload;
      return;
    case types.SET_MEDIA_ITEM_REMOVED:
      draft.mediaItemRemoved = action.payload;
      return;
    case types.SET_ITEMS_REMOVED:
      draft.itemsRemoved = action.payload;
      return;
    case types.CLEAR_ITEM_STATE:
      return initialState;
    default:
      return draft;
  }
});

export default function useItem(options = {}) {
  const { isMedia = false } = options;
  const [state, dispatch] = useReducer(reducer, initialState);
  const item = isMedia ? state.mediaItem : state.item,
    items = state.items,
    itemCreated = isMedia ? state.mediaItemCreated : state.itemCreated,
    itemsCreated = state.itemsCreated,
    itemUpdated = isMedia ? state.mediaItemUpdated : state.itemUpdated,
    itemsUpdated = state.itemsUpdated,
    itemRemoved = isMedia ? state.mediaItemRemoved : state.itemRemoved,
    itemsRemoved = state.itemsRemoved;

  const [error, setError] = useState(null);
  const [processing, setProcessing] = useState(false);

  const setItem = useCallback(
    (item = null) => {
      isMedia
        ? dispatch({ type: types.SET_MEDIA_ITEM, payload: item })
        : dispatch({ type: types.SET_ITEM, payload: item });
    },
    [isMedia]
  );
  const setItems = useCallback((items = null) => {
    dispatch({ type: types.SET_ITEMS, payload: items });
  }, []);
  const setItemCreated = useCallback(
    (itemCreated = false) => {
      isMedia
        ? dispatch({ type: types.SET_MEDIA_ITEM_CREATED, payload: itemCreated })
        : dispatch({ type: types.SET_ITEM_CREATED, payload: itemCreated });
    },
    [isMedia]
  );
  const setItemsCreated = useCallback((itemsCreated = false) => {
    dispatch({ type: types.SET_ITEMS_CREATED, payload: itemsCreated });
  }, []);
  const setItemUpdated = useCallback(
    (itemUpdated = false) => {
      isMedia
        ? dispatch({ type: types.SET_MEDIA_ITEM_UPDATED, payload: itemUpdated })
        : dispatch({ type: types.SET_ITEM_UPDATED, payload: itemUpdated });
    },
    [isMedia]
  );
  const setItemsUpdated = useCallback((itemsUpdated = false) => {
    dispatch({ type: types.SET_ITEMS_UPDATED, payload: itemsUpdated });
  }, []);
  const setItemRemoved = useCallback(
    (itemRemoved = false) => {
      isMedia
        ? dispatch({ type: types.SET_MEDIA_ITEM_REMOVED, payload: itemRemoved })
        : dispatch({ type: types.SET_ITEM_REMOVED, payload: itemRemoved });
    },
    [isMedia]
  );
  const setItemsRemoved = useCallback((itemsRemoved = false) => {
    dispatch({ type: types.SET_ITEMS_REMOVED, payload: itemsRemoved });
  }, []);

  return {
    setItem,
    setItems,
    setItemCreated,
    setItemsCreated,
    setItemUpdated,
    setItemsUpdated,
    setItemRemoved,
    setItemsRemoved,
    item,
    items,
    itemCreated,
    itemsCreated,
    itemUpdated,
    itemsUpdated,
    itemRemoved,
    itemsRemoved,
    error,
    setError,
    processing,
    setProcessing,
  };
}
