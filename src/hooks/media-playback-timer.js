import { useState, useCallback, useRef } from 'react';

export default function useMediaPlaybackTimer(isAudio = false) {
  const [currentTime, setCurrentTime] = useState();
  const [duration, setDuration] = useState();
  const videoElementRef = useRef(null);
  const audioElementRef = useRef(null);

  const handleTimeUpdate = useCallback(() => {
    setCurrentTime(
      Math.floor(
        isAudio
          ? audioElementRef.current?.currentTime
          : videoElementRef.current?.currentTime
      )
    );
    setDuration(
      Math.floor(
        isAudio
          ? audioElementRef.current?.duration
          : videoElementRef.current?.duration
      )
    );
  }, [isAudio, setCurrentTime, setDuration]);
  const handleVideoElementInit = useCallback((videoElement) => {
    videoElementRef.current = videoElement;
  }, []);
  const handleAudioElementInit = useCallback((audioElement) => {
    audioElementRef.current = audioElement;
  }, []);

  return {
    currentTime,
    duration,
    setCurrentTime,
    setDuration,
    handleTimeUpdate,
    handleVideoElementInit,
    handleAudioElementInit,
  };
}
