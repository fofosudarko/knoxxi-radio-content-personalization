import { useEffect, useCallback, useState } from 'react';

import useItem from './item';

export default function useGeolocation() {
  const [geolocation, setGeolocation] = useState(null);
  const [currentPosition, setCurrentPosition] = useState(null);
  const { error, setError, processing, setProcessing } = useItem();

  useEffect(() => {
    if (!navigator.geolocation) {
      setError(new Error('Geolocation not supported in your browser'));
    } else {
      setGeolocation(navigator.geolocation);
    }
  }, [setError]);

  const handleGetCurrentPositionSuccess = useCallback(
    (position) => {
      setProcessing(false);
      setCurrentPosition(position);
    },
    [setProcessing]
  );

  const handleGetCurrentPositionError = useCallback(
    (error) => {
      setProcessing(false);
      setError(error);
    },
    [setError, setProcessing]
  );

  const handleGetCurrentPosition = useCallback(() => {
    setProcessing(true);
    geolocation.getCurrentPosition(
      handleGetCurrentPositionSuccess,
      handleGetCurrentPositionError
    );
  }, [
    geolocation,
    handleGetCurrentPositionError,
    handleGetCurrentPositionSuccess,
    setProcessing,
  ]);

  return {
    error,
    setError,
    processing,
    setProcessing,
    handleGetCurrentPosition,
    currentPosition,
    setCurrentPosition,
    setGeolocation,
  };
}
