// upload-stream-record.js

import { useEffect, useState } from 'react';

//import { useSignOut, useNotificationHandler } from 'src/hooks';

export default function useUploadStreamRecord() {
  const [uploadResponse, setUploadResponse] = useState(null);
  const [error, setError] = useState(null);
  //const handleSignOut = useSignOut();
  //const handleNotification = useNotificationHandler();

  useEffect(() => {}, []);

  return { uploadResponse, error, setError, setUploadResponse };
}
