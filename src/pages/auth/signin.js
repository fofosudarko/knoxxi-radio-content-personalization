import Head from 'next/head';

import { useAuth } from 'src/hooks';

import { SignInPage } from 'src/components/auth';
import { getSiteLayout } from 'src/layouts/site/SiteLayout/SiteLayout';

export default function SignIn() {
  const { appUser } = useAuth();
  return (
    <>
      <Head>
        <title>Sign in</title>
      </Head>
      <SignInPage appUser={appUser} />
    </>
  );
}

SignIn.getLayout = getSiteLayout;
