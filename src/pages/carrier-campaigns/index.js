import Head from 'next/head';
import PropTypes from 'prop-types';

import { useHasMounted } from 'src/hooks';
import { getSiteLayout } from 'src/layouts/site/SiteLayout/SiteLayout';
import { CarrierCampaignsPage } from 'src/components/carrier-campaigns';

export default function CarrierCampaigns({ carrierId }) {
  const hasMounted = useHasMounted();

  return (
    <>
      <Head>
        <title>Carrier campaigns</title>
      </Head>
      {hasMounted ? (
        <CarrierCampaignsPage carrierId={carrierId} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

export function getServerSideProps(context) {
  const { carrierId = null } = context.query;
  return {
    props: { carrierId },
  };
}

CarrierCampaigns.getLayout = getSiteLayout;

CarrierCampaigns.propTypes = {
  carrierId: PropTypes.string,
};
CarrierCampaigns.defaultProps = {
  carrierId: undefined,
};
