import PropTypes from 'prop-types';

import { useHasMounted } from 'src/hooks';
import { useGetContentPersonalizationPublic } from 'src/hooks/api';
import { getStreamLayout } from 'src/layouts/stream/StreamLayout/StreamLayout';
import { SiteStreamPage } from 'src/components/site';

export default function SiteStream({ contentCategory, streamedById }) {
  const { contentPersonalization } = useGetContentPersonalizationPublic({
    contentPersonalization: { id: streamedById },
  });
  const hasMounted = useHasMounted();

  return hasMounted && contentPersonalization ? (
    <SiteStreamPage
      contentCategory={contentCategory}
      streamedById={streamedById}
    />
  ) : (
    'Loading'
  );
}

export function getServerSideProps(context) {
  const { contentCategory = null, streamedById = null } = context.query ?? {};
  return { props: { contentCategory, streamedById } };
}

SiteStream.getLayout = getStreamLayout;

SiteStream.propTypes = {
  contentCategory: PropTypes.string,
  streamedById: PropTypes.string,
};
SiteStream.defaultProps = {
  contentCategory: undefined,
  streamedById: undefined,
};
