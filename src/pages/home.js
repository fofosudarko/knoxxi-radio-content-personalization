import Head from 'next/head';

import { useAuth } from 'src/hooks';
import { getSiteLayout } from 'src/layouts/site/SiteLayout/SiteLayout';
import { HomePage } from 'src/components/init';

export default function HomeIndex() {
  const { appUser, hasMounted } = useAuth();

  return (
    <>
      <Head>
        <title>Home</title>
      </Head>
      {appUser && hasMounted ? (
        <HomePage appUser={appUser} />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

HomeIndex.getLayout = getSiteLayout;
