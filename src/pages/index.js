import PropTypes from 'prop-types';
import Head from 'next/head';

import { useHasMounted } from 'src/hooks';
import { getSiteLayout } from 'src/layouts/site/SiteLayout/SiteLayout';
//import { SiteHomePage } from 'src/components/site';
import { ContentPersonalizationNewPage } from 'src/components/content-personalizations';

/*export default function SiteHome({ carrierId, carrierSecurityCode }) {
  return (
    <SiteHomePage
      carrierId={carrierId}
      carrierSecurityCode={carrierSecurityCode}
    />
  );
}*/

export default function SiteHome({ carrierId, carrierSecurityCode }) {
  const hasMounted = useHasMounted();

  return (
    <>
      <Head>
        <title>New content personalization</title>
      </Head>
      {hasMounted ? (
        <ContentPersonalizationNewPage
          carrierId={carrierId}
          carrierSecurityCode={carrierSecurityCode}
        />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

export function getServerSideProps(context) {
  const { carrierId = null, carrierSecurityCode = null } = context.query ?? {};
  return { props: { carrierId, carrierSecurityCode } };
}

SiteHome.getLayout = getSiteLayout;

SiteHome.propTypes = {
  carrierId: PropTypes.string,
  carrierSecurityCode: PropTypes.string,
};
SiteHome.defaultProps = {
  carrierId: undefined,
  carrierSecurityCode: undefined,
};
