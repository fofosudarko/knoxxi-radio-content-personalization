import PropTypes from 'prop-types';
import Head from 'next/head';

import { useHasMounted } from 'src/hooks';
import { useGetContentPersonalizationPublic } from 'src/hooks/api';
import { getStreamLayout } from 'src/layouts/stream/StreamLayout/StreamLayout';
import { ContentPersonalizationShowPage } from 'src/components/content-personalizations';

export default function ContentPersonalizationShow({
  contentPersonalizationId,
  homeRoute,
}) {
  const { contentPersonalization } = useGetContentPersonalizationPublic({
    contentPersonalization: { id: contentPersonalizationId },
  });
  const hasMounted = useHasMounted();

  return (
    <>
      <Head>
        <title>Content personalization</title>
      </Head>
      {hasMounted && contentPersonalization ? (
        <ContentPersonalizationShowPage
          contentPersonalization={contentPersonalization}
          homeRoute={homeRoute}
        />
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
}

ContentPersonalizationShow.getLayout = getStreamLayout;

export function getServerSideProps(context) {
  const { contentPersonalizationId = null } = context.params;
  const { homeRoute = null } = context.query;
  return { props: { contentPersonalizationId, homeRoute } };
}

ContentPersonalizationShow.propTypes = {
  contentPersonalizationId: PropTypes.string,
  homeRoute: PropTypes.string,
};
ContentPersonalizationShow.defaultProps = {
  contentPersonalizationId: undefined,
  homeRoute: undefined,
};
