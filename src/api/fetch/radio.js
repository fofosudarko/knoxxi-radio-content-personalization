import { useAuthStore } from 'src/stores/auth';
import { KNOXXI_RADIO_SERVICE_API_URL } from 'src/config';
import { cleanupResource } from 'src/utils/index';

export async function fetchRadioResource(resource, options = null) {
  const headers = new Headers();
  const url = KNOXXI_RADIO_SERVICE_API_URL + '/' + cleanupResource(resource);
  const state = useAuthStore.getState() ?? null;
  if (options?.headers) {
    const optionsHeaders = options?.headers;
    for (const headerKey of Object.keys(optionsHeaders)) {
      headers.set(headerKey, optionsHeaders[headerKey]);
    }
    delete options?.headers;
  }
  if (state?.appUserAccessToken) {
    headers.set(
      'Authorization',
      `Bearer ${state.appUserAccessToken.account.token}`
    );
  }
  return await fetch(url, { ...options, headers });
}

export default async function fetchRadioResourceNoAuth(
  resource,
  options = null
) {
  const headers = new Headers();
  const useBaseurl =
    options?.useBaseurl === undefined ? true : options?.useBaseurl;
  const url = useBaseurl
    ? KNOXXI_RADIO_SERVICE_API_URL + '/' + cleanupResource(resource)
    : resource;
  if (options?.headers) {
    const optionsHeaders = options?.headers;
    for (const headerKey of Object.keys(optionsHeaders)) {
      headers.set(headerKey, optionsHeaders[headerKey]);
    }
    delete options?.headers;
  }
  delete options?.useBaseurl;
  return await fetch(url, { ...options, headers });
}
