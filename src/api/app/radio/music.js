import fetchRadioResourceNoAuth from 'src/api/fetch/radio';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class MusicService {
  static async listMusicStreamList({
    streamedById,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchRadioResourceNoAuth(
      `music/stream-list${generateQuerystring({
        streamedById,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  }
}
