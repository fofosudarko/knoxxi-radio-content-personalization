import { generateQuerystring } from 'src/utils';
import fetchRadioResourceNoAuth, {
  fetchRadioResource,
} from '../../fetch/radio';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

class ContentPersonalizationService {
  static async listContentPersonalizations({
    userAgent,
    contentCategory,
    contentSubCategory,
    carrierDriverInfoId,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchRadioResource(
      `content-personalizations${generateQuerystring({
        userAgent,
        contentCategory,
        contentSubCategory,
        carrierDriverInfoId,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }

  static async createContentPersonalization(body = null, params = null) {
    return await fetchRadioResourceNoAuth(
      `content-personalizations${generateQuerystring(params)}`,
      {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  }

  static async getContentPersonalizationPublic(contentPersonalization = null) {
    const contentPersonalizationId = contentPersonalization?.id;
    return await fetchRadioResourceNoAuth(
      `content-personalizations/public/${contentPersonalizationId}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  }
}

export default ContentPersonalizationService;
