import fetchRadioResourceNoAuth from '../../fetch/radio';

class AdvertPlayService {
  static async createAdvertPlay(body = null) {
    return await fetchRadioResourceNoAuth(`advert-plays`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}

export default AdvertPlayService;
