import fetchRadioResourceNoAuth from '../../fetch/radio';
import { generateQuerystring } from 'src/utils';

class EnumService {
  static async listContentCategory(isFiltered) {
    return await fetchRadioResourceNoAuth(
      `enums/content-category${generateQuerystring({ isFiltered })}`,
      {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
      }
    );
  }

  static async listContentSubCategory(contentCategory = null, isFiltered) {
    const choicesUrl = contentCategory?.choicesUrl;
    return await fetchRadioResourceNoAuth(
      `${choicesUrl}${generateQuerystring({ isFiltered })}`,
      {
        method: 'GET',
        useBaseurl: false,
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  }
}

export default EnumService;
