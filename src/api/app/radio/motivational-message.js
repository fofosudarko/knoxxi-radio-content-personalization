import fetchRadioResourceNoAuth from 'src/api/fetch/radio';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class MotivationalMessageService {
  static async listMotivationalMessageStreamList({
    streamedById,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchRadioResourceNoAuth(
      `motivational-messages/stream-list${generateQuerystring({
        streamedById,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  }
}
