import fetchRadioResourceNoAuth from '../../fetch/radio';
import { generateQuerystring } from 'src/utils/index';
import { DEFAULT_PAGE_SIZE, DEFAULT_PAGE } from 'src/config';

export default class CarrierCampaignService {
  static async listCarrierCampaignsPublic({
    carrierId,
    status,
    page = DEFAULT_PAGE,
    size = DEFAULT_PAGE_SIZE,
  }) {
    return await fetchRadioResourceNoAuth(
      `carrier-campaigns/public${generateQuerystring({
        carrierId,
        status,
        page,
        size,
      })}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
      }
    );
  }
}
