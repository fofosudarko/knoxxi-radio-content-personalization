import { fetchRadioResource } from '../../fetch/radio';

class NotificationService {
  static async createOrUpdateWebPushClient(body = null) {
    return await fetchRadioResource(`notifications/web-push-clients`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}

export default NotificationService;
