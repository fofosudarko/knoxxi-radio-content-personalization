import { generateQuerystring } from 'src/utils';
import fetchRadioResourceNoAuth from '../../fetch/radio';

class ContentStreamService {
  static async createContentStream(contentCategory = null, streamedBy = null) {
    const streamUrl = contentCategory?.streamUrl,
      streamedById = streamedBy?.id,
      streamedByRecaptcha = streamedBy?.recaptcha;
    return await fetchRadioResourceNoAuth(
      `${streamUrl}${generateQuerystring({
        streamedById,
        streamedByRecaptcha,
      })}`,
      {
        method: 'POST',
        useBaseurl: false,
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
  }
}

export default ContentStreamService;
