import PropTypes from 'prop-types';
import { MdOutlineReplay } from 'react-icons/md';

import {
  useDeviceDimensions,
  useHasMounted,
  //useMediaPlaybackTimer,
  useUserAgent,
  useAppleDeviceTester,
  useMediaType,
} from 'src/hooks';
//import { FILE_PLAYER_CONFIG } from 'src/config';
import {
  ENHANCED_STREAM_PLAYER_POSTER,
  ENHANCED_STREAM_PLAYER_CONTROLSLIST,
} from 'src/config';

//import { humanizeMediaTime } from 'src/utils';
import { RefreshButton, EnhancedStreamPlayer } from 'src/components/lib';
import MediaStreamListRestart from './MediaStreamListRestart';
import { useMediaStreamPlayerItemDetails } from './MediaStreamPlayerItem';

function MediaStreamAdvertsPlayer({
  loadingText,
  advertUrl,
  onAdvertEnded,
  currentAdvertIndex,
  mediaStreamAdvertsLength,
  onAdvertReady,
  onAdvertPlay,
  onAdvertBuffer,
  onAdvertError,
  onAdvertPlayerError,
  bufferingAdvert,
  onMediaStreamListRestart,
  onAdvertReplay,
  showRestart,
  playingAdvert,
  loadingAdvert,
  currentMediaStreamAdvertItem,
}) {
  const { isLargeDevice } = useDeviceDimensions();
  const hasMounted = useHasMounted();
  const { posterUrl = ENHANCED_STREAM_PLAYER_POSTER, content } =
    useMediaStreamPlayerItemDetails(currentMediaStreamAdvertItem);
  const isAppleDevice = useAppleDeviceTester(useUserAgent());
  const { isVideo } = useMediaType(content);
  const isAudio = !isVideo;
  /*const { currentTime, duration, setCurrentTime, setDuration,handleTimeUpdate: handleAdvertTimeUpdate,handleVideoElementInit } =
    useMediaPlaybackTimer(isAudio);*/

  return (
    <div>
      {hasMounted ? (
        <div>
          <div className="d-flex justify-content-center site-fs-5 fw-light text-secondary py-1">
            playing ad{' '}
            <span className="fw-bold px-1">{currentAdvertIndex + 1}</span> of{' '}
            <span className="fw-bold px-1">{mediaStreamAdvertsLength}</span>
            {/*<span>
              ({humanizeMediaTime(currentTime)}/{humanizeMediaTime(duration)})
            </span>*/}
            ...
          </div>
          <EnhancedStreamPlayer
            controls={isAudio}
            controlsList={ENHANCED_STREAM_PLAYER_CONTROLSLIST}
            url={advertUrl}
            playing={playingAdvert}
            height={isLargeDevice ? 350 : 300}
            onEnded={onAdvertEnded}
            onReady={onAdvertReady}
            onPlay={onAdvertPlay}
            onBuffer={onAdvertBuffer}
            onError={onAdvertError}
            //onTimeUpdate={handleAdvertTimeUpdate}
            //onVideoElementInit={handleVideoElementInit}
            onPlayerError={onAdvertPlayerError}
            className={`${
              isAudio
                ? 'bg-transparent'
                : !isVideo && isAppleDevice
                ? 'advert-playback-placeholder'
                : 'bg-black'
            }`}
            //config={{ ...FILE_PLAYER_CONFIG }}
            poster={posterUrl}
            playsInline={isAppleDevice}
            isAudio={isAudio}
          />
          {bufferingAdvert ? (
            <div className="d-flex justify-content-center site-fs-5 fw-light text-secondary py-1">
              ad buffering...
            </div>
          ) : null}
          {loadingAdvert ? (
            <div className="d-flex justify-content-center site-fs-5 fw-light text-secondary py-1">
              ad loading...
            </div>
          ) : null}
          <div className="my-2 d-flex justify-content-between">
            {showRestart ? (
              <MediaStreamListRestart
                useTooltip
                onMediaStreamListRestart={onMediaStreamListRestart}
                text="Restart"
              />
            ) : null}
            <RefreshButton
              onClick={onAdvertReplay}
              useTooltip
              text="Replay"
              Icon={<MdOutlineReplay size={25} />}
            />
          </div>
        </div>
      ) : (
        <>{loadingText}</>
      )}
    </div>
  );
}

export function MediaStreamAdvertsPlayerEmpty() {
  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      Stream list is empty
    </div>
  );
}

MediaStreamAdvertsPlayer.propTypes = {
  loadingText: PropTypes.string,
  advertUrl: PropTypes.string,
  onAdvertEnded: PropTypes.func,
  currentAdvertIndex: PropTypes.number,
  mediaStreamAdvertsLength: PropTypes.number,
  onAdvertReady: PropTypes.func,
  onAdvertPlay: PropTypes.func,
  onAdvertBuffer: PropTypes.func,
  onAdvertError: PropTypes.func,
  onAdvertPlayerError: PropTypes.func,
  bufferingAdvert: PropTypes.bool,
  onMediaStreamListRestart: PropTypes.func,
  onAdvertReplay: PropTypes.func,
  showRestart: PropTypes.bool,
  playingAdvert: PropTypes.bool,
  loadingAdvert: PropTypes.bool,
  currentMediaStreamAdvertItem: PropTypes.object,
};
MediaStreamAdvertsPlayer.defaultProps = {
  loadingText: 'Loading media stream adverts player...',
  advertUrl: undefined,
  onAdvertEnded: undefined,
  currentAdvertIndex: undefined,
  mediaStreamAdvertsLength: undefined,
  onAdvertReady: undefined,
  onAdvertPlay: undefined,
  onAdvertBuffer: undefined,
  onAdvertError: undefined,
  onAdvertPlayerError: undefined,
  bufferingAdvert: false,
  onMediaStreamListRestart: undefined,
  onAdvertReplay: undefined,
  showRestart: false,
  playingAdvert: true,
  loadingAdvert: false,
  currentMediaStreamAdvertItem: null,
};

export default MediaStreamAdvertsPlayer;
