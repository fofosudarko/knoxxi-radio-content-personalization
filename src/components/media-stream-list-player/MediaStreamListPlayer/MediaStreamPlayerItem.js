export function useMediaStreamPlayerItemDetails(mediaStreamPlayerItem) {
  const { poster = null, content = null } = mediaStreamPlayerItem ?? {};
  const posterUrl = poster?.url,
    posterName = poster?.name;
  const contentUrl = content?.url,
    contentType = content?.type,
    contentSize = content?.size;

  return {
    posterUrl,
    posterName,
    contentUrl,
    contentType,
    contentSize,
    content,
  };
}
