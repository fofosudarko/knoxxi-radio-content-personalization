import { useContext, useRef, useEffect, useCallback, useState } from 'react';

import { useNotificationHandler } from 'src/hooks';
import { useCreateContentStream, useCreateAdvertPlay } from 'src/hooks/api';
import { MediaStreamListContext } from 'src/context';
import { getMediaStreamListItemDetails } from 'src/utils';
//import { getNotification } from 'src/utils/notification';

import MediaStreamListPlayer from './MediaStreamListPlayer';
import MediaStreamAdvertsPlayer from './MediaStreamAdvertsPlayer';
import MediaStreamScriptsPlayer from './MediaStreamScriptsPlayer';

export function useContentStreamAndAdvertPlaySequence(options = {}) {
  const {
    contentPersonalization,
    mediaStreamList,
    mediaStreamAdverts,
    currentStreamIndex,
    currentAdvertIndex,
  } = options;
  const [streamEnded, setStreamEnded] = useState(false);
  const [advertEnded, setAdvertEnded] = useState(false);
  const [previousStreamIndex, setPreviousStreamIndex] = useState();
  const [previousAdvertIndex, setPreviousAdvertIndex] = useState();
  const processingContentStreamRef = useRef(false);
  const processingAdvertPlayRef = useRef(false);
  let handleContentStreamCreated,
    handleAdvertPlayCreated,
    handleAdvertPlayCreate,
    handleContentStreamCreate;

  const {
    handleCreateContentStream,
    error: contentStreamError,
    setError: setContentStreamError,
    contentStreamCreated,
    setContentStreamCreated,
  } = useCreateContentStream();
  const {
    handleCreateAdvertPlay,
    error: advertPlayError,
    setError: setAdvertPlayError,
    advertPlayCreated,
    setAdvertPlayCreated,
  } = useCreateAdvertPlay();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (
      streamEnded &&
      !processingContentStreamRef.current &&
      previousStreamIndex !== undefined
    ) {
      processingContentStreamRef.current = true;
      const mediaStreamListItem = getMediaStreamListItemDetails(
        mediaStreamList[previousStreamIndex]
      );
      mediaStreamListItem.streamUrl &&
        handleContentStreamCreate(mediaStreamListItem, contentPersonalization);
    }

    if (
      advertEnded &&
      !processingAdvertPlayRef.current &&
      previousAdvertIndex !== undefined
    ) {
      processingAdvertPlayRef.current = true;
      handleAdvertPlayCreate(
        mediaStreamAdverts[previousAdvertIndex],
        contentPersonalization
      );
    }
  }, [
    contentPersonalization,
    handleAdvertPlayCreate,
    handleContentStreamCreate,
    advertEnded,
    mediaStreamAdverts,
    mediaStreamList,
    previousAdvertIndex,
    previousStreamIndex,
    streamEnded,
    currentStreamIndex,
    currentAdvertIndex,
  ]);

  useEffect(() => {
    if (contentStreamError) {
      setStreamEnded(false);
      processingContentStreamRef.current = false;
      handleNotification(contentStreamError);
    }
    if (advertPlayError) {
      setAdvertEnded(false);
      processingAdvertPlayRef.current = false;
      handleNotification(advertPlayError);
    }

    return () => {
      contentStreamError && setContentStreamError(null);
      advertPlayError && setAdvertPlayError(null);
    };
  }, [
    advertPlayError,
    contentStreamError,
    handleNotification,
    setAdvertEnded,
    setAdvertPlayError,
    setContentStreamError,
    setStreamEnded,
  ]);

  useEffect(() => {
    if (contentStreamCreated) {
      handleContentStreamCreated();
    }
    if (advertPlayCreated) {
      handleAdvertPlayCreated();
    }
  }, [
    advertPlayCreated,
    contentStreamCreated,
    handleAdvertPlayCreated,
    handleContentStreamCreated,
  ]);

  // handle content stream created
  handleContentStreamCreated = useCallback(() => {
    /*handleNotification(
      getNotification(
        'Content stream created successfully'
      ).getSuccessNotification()
    );*/
    setContentStreamCreated(false);
    setStreamEnded(false);
    processingContentStreamRef.current = false;
  }, [setContentStreamCreated, setStreamEnded]);
  // handle advert play created
  handleAdvertPlayCreated = useCallback(() => {
    /*handleNotification(
      getNotification(
        'Advert play created successfully'
      ).getSuccessNotification()
    );*/
    setAdvertPlayCreated(false);
    setAdvertEnded(false);
    processingAdvertPlayRef.current = false;
  }, [setAdvertEnded, setAdvertPlayCreated]);
  // handle advert play create
  handleAdvertPlayCreate = useCallback(
    async (advert = null, streamedBy = null) => {
      await handleCreateAdvertPlay({
        advertId: advert?.id ?? null,
        streamedById: streamedBy?.id ?? null,
      });
    },
    [handleCreateAdvertPlay]
  );
  // handle content stream create
  handleContentStreamCreate = useCallback(
    async (contentStream = null, streamedBy = null) => {
      await handleCreateContentStream(contentStream, streamedBy);
    },
    [handleCreateContentStream]
  );

  return {
    setPreviousStreamIndex,
    setPreviousAdvertIndex,
    setStreamEnded,
    setAdvertEnded,
  };
}

function MediaStreamPlayerContainer() {
  const {
    streamUrl,
    advertUrl,
    scriptUrl,
    currentStreamIndex,
    handleStreamEnded,
    shouldPlayAdvert,
    currentScriptIndex,
    handleScriptEnded,
    shouldPlayScript,
    handleSelectPreviousMediaStreamListItem,
    handleSelectNextMediaStreamListItem,
    handleAdvertEnded,
    currentAdvertIndex,
    mediaStreamAdverts,
    mediaStreamList,
    handleScriptReady,
    handleScriptPlay,
    handleScriptBuffer,
    handleScriptError,
    handleScriptPlayerError,
    handleAdvertReady,
    handleAdvertPlay,
    handleAdvertBuffer,
    handleAdvertError,
    handleAdvertPlayerError,
    handleStreamReady,
    handleStreamPlay,
    handleStreamBuffer,
    handleStreamError,
    handleStreamPlayerError,
    bufferingScript,
    bufferingAdvert,
    bufferingStream,
    handleMediaStreamListRestart,
    currentMediaStreamListItem,
    currentMediaStreamAdvertItem,
    currentMediaStreamScriptItem,
    handleStreamReplay,
    handleAdvertReplay,
    handleScriptReplay,
    showRestart,
    playingStream,
    playingAdvert,
    playingScript,
    loadingStream,
    loadingAdvert,
    loadingScript,
    contentPersonalization,
  } = useContext(MediaStreamListContext);
  const {
    setAdvertEnded,
    setStreamEnded,
    setPreviousAdvertIndex,
    setPreviousStreamIndex,
  } = useContentStreamAndAdvertPlaySequence({
    contentPersonalization,
    mediaStreamList,
    mediaStreamAdverts,
    currentStreamIndex,
    currentAdvertIndex,
  });

  const mediaStreamAdvertsLength = mediaStreamAdverts?.length,
    mediaStreamListLength = mediaStreamList?.length;

  return (
    <div className="mt-2">
      {shouldPlayScript ? (
        <MediaStreamScriptsPlayer
          scriptUrl={scriptUrl}
          onScriptEnded={() => handleScriptEnded(currentScriptIndex)}
          loadingText="Loading media stream scripts player..."
          onScriptReady={handleScriptReady}
          onScriptPlay={handleScriptPlay}
          onScriptBuffer={handleScriptBuffer}
          onScriptError={handleScriptError}
          onScriptPlayerError={handleScriptPlayerError}
          bufferingScript={bufferingScript}
          onMediaStreamListRestart={handleMediaStreamListRestart}
          onScriptReplay={() =>
            handleScriptReplay(currentMediaStreamScriptItem)
          }
          showRestart={showRestart}
          playingScript={playingScript}
          loadingScript={loadingScript}
          currentMediaStreamScriptItem={currentMediaStreamScriptItem}
        />
      ) : shouldPlayAdvert ? (
        <MediaStreamAdvertsPlayer
          advertUrl={advertUrl}
          onAdvertEnded={() => {
            setAdvertEnded(true);
            setPreviousAdvertIndex(currentAdvertIndex);
            handleAdvertEnded(currentStreamIndex);
          }}
          loadingText="Loading media stream adverts player..."
          currentAdvertIndex={currentAdvertIndex}
          mediaStreamAdvertsLength={mediaStreamAdvertsLength}
          onAdvertReady={handleAdvertReady}
          onAdvertPlay={handleAdvertPlay}
          onAdvertBuffer={handleAdvertBuffer}
          onAdvertError={handleAdvertError}
          onAdvertPlayerError={handleAdvertPlayerError}
          bufferingAdvert={bufferingAdvert}
          onMediaStreamListRestart={handleMediaStreamListRestart}
          onAdvertReplay={() =>
            handleAdvertReplay(currentMediaStreamAdvertItem)
          }
          showRestart={showRestart}
          playingAdvert={playingAdvert}
          loadingAdvert={loadingAdvert}
          currentMediaStreamAdvertItem={currentMediaStreamAdvertItem}
        />
      ) : (
        <MediaStreamListPlayer
          streamUrl={streamUrl}
          onSelectPreviousItem={() =>
            handleSelectPreviousMediaStreamListItem(currentStreamIndex)
          }
          onSelectNextItem={() =>
            handleSelectNextMediaStreamListItem(currentStreamIndex)
          }
          onStreamEnded={() => {
            setStreamEnded(true);
            setPreviousStreamIndex(currentStreamIndex);
            handleStreamEnded(currentStreamIndex);
          }}
          loadingText="Loading media stream list player..."
          currentStreamIndex={currentStreamIndex}
          mediaStreamListLength={mediaStreamListLength}
          onStreamReady={handleStreamReady}
          onStreamPlay={handleStreamPlay}
          onStreamBuffer={handleStreamBuffer}
          onStreamError={handleStreamError}
          onStreamPlayerError={handleStreamPlayerError}
          bufferingStream={bufferingStream}
          onMediaStreamListRestart={handleMediaStreamListRestart}
          onStreamReplay={() => handleStreamReplay(currentMediaStreamListItem)}
          showRestart={showRestart}
          playingStream={playingStream}
          loadingStream={loadingStream}
          currentMediaStreamListItem={currentMediaStreamListItem}
        />
      )}
    </div>
  );
}

export function MediaStreamPlayerContainerEmpty() {
  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      Stream list is empty
    </div>
  );
}

export default MediaStreamPlayerContainer;
