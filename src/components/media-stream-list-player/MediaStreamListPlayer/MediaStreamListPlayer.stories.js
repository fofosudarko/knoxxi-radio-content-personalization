import MediaStreamListPlayer from './MediaStreamListPlayer';

const Story = {
  title: 'MediaStreamList/Player',
  component: MediaStreamListPlayer,
};

const Template = (args) => {
  return <MediaStreamListPlayer {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
