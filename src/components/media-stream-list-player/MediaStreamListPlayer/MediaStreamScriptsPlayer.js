import PropTypes from 'prop-types';
import { MdOutlineReplay } from 'react-icons/md';

import {
  useDeviceDimensions,
  useHasMounted,
  //useMediaPlaybackTimer,
  useUserAgent,
  useAppleDeviceTester,
  useMediaType,
} from 'src/hooks';
//import { FILE_PLAYER_CONFIG } from 'src/config';
import {
  ENHANCED_STREAM_PLAYER_POSTER,
  ENHANCED_STREAM_PLAYER_CONTROLSLIST,
} from 'src/config';

//import { humanizeMediaTime } from 'src/utils';
import { RefreshButton, EnhancedStreamPlayer } from 'src/components/lib';
import MediaStreamListRestart from './MediaStreamListRestart';
import { useMediaStreamPlayerItemDetails } from './MediaStreamPlayerItem';

function MediaStreamScriptsPlayer({
  loadingText,
  scriptUrl,
  onScriptEnded,
  onScriptReady,
  onScriptPlay,
  onScriptBuffer,
  onScriptError,
  onScriptPlayerError,
  bufferingScript,
  onMediaStreamListRestart,
  onScriptReplay,
  showRestart,
  playingScript,
  loadingScript,
  currentMediaStreamScriptItem,
}) {
  const { isLargeDevice } = useDeviceDimensions();
  const hasMounted = useHasMounted();
  const { posterUrl = ENHANCED_STREAM_PLAYER_POSTER, content } =
    useMediaStreamPlayerItemDetails(currentMediaStreamScriptItem);
  const isAppleDevice = useAppleDeviceTester(useUserAgent());
  const { isVideo } = useMediaType(content);
  const isAudio = !isVideo;
  /*const { currentTime, duration, setCurrentTime, setDuration,handleTimeUpdate: handleScriptTimeUpdate,handleVideoElementInit } =
    useMediaPlaybackTimer(isAudio);*/

  return (
    <div>
      {hasMounted ? (
        <div>
          <div className="d-flex justify-content-center site-fs-5 fw-light text-secondary py-1">
            {/*<span>
              ({humanizeMediaTime(currentTime)}/{humanizeMediaTime(duration)})
            </span>*/}
          </div>
          <EnhancedStreamPlayer
            controls={isAudio}
            controlsList={ENHANCED_STREAM_PLAYER_CONTROLSLIST}
            url={scriptUrl}
            playing={playingScript}
            height={isLargeDevice ? 350 : 300}
            onEnded={onScriptEnded}
            onReady={onScriptReady}
            onPlay={onScriptPlay}
            onBuffer={onScriptBuffer}
            onError={onScriptError}
            //onTimeUpdate={handleScriptTimeUpdate}
            //onVideoElementInit={handleVideoElementInit}
            onPlayerError={onScriptPlayerError}
            className={`${
              isAudio
                ? 'bg-transparent'
                : !isVideo && isAppleDevice
                ? 'script-playback-placeholder'
                : 'bg-black'
            }`}
            //config={{ ...FILE_PLAYER_CONFIG }}
            poster={posterUrl}
            playsInline={isAppleDevice}
            isAudio={isAudio}
          />
          {bufferingScript ? (
            <div className="d-flex justify-content-center site-fs-5 fw-light text-secondary py-1">
              script buffering...
            </div>
          ) : null}
          {loadingScript ? (
            <div className="d-flex justify-content-center site-fs-5 fw-light text-secondary py-1">
              script loading...
            </div>
          ) : null}
          <div className="my-2 d-flex justify-content-between">
            {showRestart ? (
              <MediaStreamListRestart
                useTooltip
                onMediaStreamListRestart={onMediaStreamListRestart}
                text="Restart"
              />
            ) : null}
            <RefreshButton
              onClick={onScriptReplay}
              useTooltip
              text="Replay"
              Icon={<MdOutlineReplay size={25} />}
            />
          </div>
        </div>
      ) : (
        <>{loadingText}</>
      )}
    </div>
  );
}

export function MediaStreamScriptsPlayerEmpty() {
  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      Stream list is empty
    </div>
  );
}

MediaStreamScriptsPlayer.propTypes = {
  loadingText: PropTypes.string,
  scriptUrl: PropTypes.string,
  onScriptEnded: PropTypes.func,
  onScriptReady: PropTypes.func,
  onScriptPlay: PropTypes.func,
  onScriptBuffer: PropTypes.func,
  onScriptError: PropTypes.func,
  onScriptPlayerError: PropTypes.func,
  bufferingScript: PropTypes.bool,
  onMediaStreamListRestart: PropTypes.func,
  onScriptReplay: PropTypes.func,
  showRestart: PropTypes.bool,
  playingScript: PropTypes.bool,
  loadingScript: PropTypes.bool,
  currentMediaStreamScriptItem: PropTypes.object,
};
MediaStreamScriptsPlayer.defaultProps = {
  loadingText: 'Loading media stream scripts player...',
  scriptUrl: undefined,
  onScriptEnded: undefined,
  onScriptReady: undefined,
  onScriptPlay: undefined,
  onScriptBuffer: undefined,
  onScriptError: undefined,
  onScriptPlayerError: undefined,
  bufferingScript: false,
  onMediaStreamListRestart: undefined,
  onScriptReplay: undefined,
  showRestart: false,
  playingScript: true,
  loadingScript: false,
  currentMediaStreamScriptItem: null,
};

export default MediaStreamScriptsPlayer;
