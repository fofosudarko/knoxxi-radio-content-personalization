import PropTypes from 'prop-types';
import { BiRefresh } from 'react-icons/bi';

import { RefreshButton } from 'src/components/lib';

function MediaStreamListRestart({
  useTooltip,
  text,
  onMediaStreamListRestart,
}) {
  return (
    <RefreshButton
      variant="transparent"
      onClick={onMediaStreamListRestart}
      text={text}
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
      Icon={<BiRefresh size={25} />}
    />
  );
}

MediaStreamListRestart.propTypes = {
  useTooltip: PropTypes.bool,
  text: PropTypes.string,
  onMediaStreamListRestart: PropTypes.func,
};
MediaStreamListRestart.defaultProps = {
  useTooltip: false,
  text: 'Restart music stream list',
  onMediaStreamListRestart: undefined,
};

export default MediaStreamListRestart;
