import { useCallback } from 'react';
import PropTypes from 'prop-types';
import { MdOutlineReplay } from 'react-icons/md';

import {
  useDeviceDimensions,
  useHasMounted,
  //useMediaPlaybackTimer,
  useUserAgent,
  useAppleDeviceTester,
  useMediaType,
} from 'src/hooks';
//import { FILE_PLAYER_CONFIG } from 'src/config';
import {
  ENHANCED_STREAM_PLAYER_POSTER,
  ENHANCED_STREAM_PLAYER_CONTROLSLIST,
} from 'src/config';

import {
  EnhancedStreamPlayer,
  //NextButton,
  //PreviousButton,
  RefreshButton,
} from 'src/components/lib';
import {
  getMediaStreamListItemDetails,
  //humanizeMediaTime
} from 'src/utils';
import MediaStreamListRestart from './MediaStreamListRestart';
import { useMediaStreamPlayerItemDetails } from './MediaStreamPlayerItem';

function MediaStreamListPlayer({
  loadingText,
  streamUrl,
  //onSelectPreviousItem,
  //onSelectNextItem,
  onStreamEnded,
  currentStreamIndex,
  mediaStreamListLength,
  onStreamReady,
  onStreamPlay,
  onStreamBuffer,
  onStreamError,
  onStreamPlayerError,
  bufferingStream,
  onMediaStreamListRestart,
  onStreamReplay,
  showRestart,
  playingStream,
  loadingStream,
  currentMediaStreamListItem,
}) {
  const { isLargeDevice } = useDeviceDimensions();
  const hasMounted = useHasMounted();
  const { posterUrl = ENHANCED_STREAM_PLAYER_POSTER, content } =
    useMediaStreamPlayerItemDetails(currentMediaStreamListItem);
  const isAppleDevice = useAppleDeviceTester(useUserAgent());
  const { isVideo } = useMediaType(content);
  const isAudio = !isVideo;
  //const { currentTime, duration, setCurrentTime, setDuration,handleTimeUpdate: handleStreamTimeUpdate,handleVideoElementInit,handleAudioElementInit } =
  //  useMediaPlaybackTimer(isAudio);

  const handlePlayerInit = useCallback(
    (player = null) => {
      const isNotLargeDevice = !isLargeDevice;
      player?.configure({
        streaming: {
          lowLatencyMode: isNotLargeDevice,
          inaccurateManifestTolerance: isNotLargeDevice ? 0 : undefined,
          rebufferingGoal: isNotLargeDevice ? 0.01 : undefined,
        },
      });
    },
    [isLargeDevice]
  );

  return (
    <div>
      {hasMounted ? (
        <div>
          <div className="d-flex justify-content-center site-fs-5 fw-light text-secondary pt-2">
            playing track{' '}
            <span className="fw-bold px-1">{currentStreamIndex + 1}</span> of{' '}
            <span className="fw-bold px-1">{mediaStreamListLength}</span>
            {/*<span>
              ({humanizeMediaTime(currentTime)}/{humanizeMediaTime(duration)})
            </span>*/}
            ...
          </div>
          <MediaStreamListItem
            mediaStreamListItem={currentMediaStreamListItem}
          />
          <div className="">
            <EnhancedStreamPlayer
              controls={isAudio}
              controlsList={ENHANCED_STREAM_PLAYER_CONTROLSLIST}
              url={streamUrl}
              playing={playingStream}
              height={isLargeDevice ? 350 : 300}
              onEnded={onStreamEnded}
              onReady={onStreamReady}
              onPlay={onStreamPlay}
              onBuffer={onStreamBuffer}
              onError={onStreamError}
              onPlayerError={onStreamPlayerError}
              onPlayerInit={handlePlayerInit}
              //onTimeUpdate={handleStreamTimeUpdate}
              //onVideoElementInit={handleVideoElementInit}
              //onAudioElementInit={handleAudioElementInit}
              className={`${
                isAudio
                  ? 'bg-transparent'
                  : !isVideo && isAppleDevice
                  ? 'stream-playback-placeholder'
                  : 'bg-black'
              }`}
              //config={{ ...FILE_PLAYER_CONFIG }}
              poster={posterUrl}
              playsInline={isAppleDevice}
              isAudio={isAudio}
            />
          </div>
          {bufferingStream ? (
            <div className="d-flex justify-content-center site-fs-5 fw-light text-secondary py-1">
              track buffering...
            </div>
          ) : null}
          {loadingStream ? (
            <div className="d-flex justify-content-center site-fs-5 fw-light text-secondary py-1">
              track loading...
            </div>
          ) : null}
          <div className="my-2 d-flex justify-content-between">
            {/*<PreviousButton
              useTooltip={isLargeDevice}
              onClick={onSelectPreviousItem}
            />*/}
            {showRestart ? (
              <MediaStreamListRestart
                useTooltip
                onMediaStreamListRestart={onMediaStreamListRestart}
                text="Restart"
              />
            ) : null}
            <RefreshButton
              onClick={onStreamReplay}
              useTooltip
              text="Replay"
              Icon={<MdOutlineReplay size={25} />}
            />
            {/*<NextButton useTooltip={isLargeDevice} onClick={onSelectNextItem} />*/}
          </div>
        </div>
      ) : (
        <>{loadingText}</>
      )}
    </div>
  );
}

export function MediaStreamListPlayerEmpty() {
  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      Stream list is empty
    </div>
  );
}

export function MediaStreamListItem({ mediaStreamListItem }) {
  const { name } = getMediaStreamListItemDetails(mediaStreamListItem);

  return (
    <div className="text-center site-fs-5 fw-light text-secondary pb-2">
      {name}
    </div>
  );
}

MediaStreamListPlayer.propTypes = {
  loadingText: PropTypes.string,
  streamUrl: PropTypes.string,
  onSelectPreviousItem: PropTypes.func,
  onSelectNextItem: PropTypes.func,
  onStreamEnded: PropTypes.func,
  MediaStreamListPlayerEmpty: PropTypes.func,
  currentStreamIndex: PropTypes.number,
  mediaStreamListLength: PropTypes.number,
  onStreamReady: PropTypes.func,
  onStreamPlay: PropTypes.func,
  onStreamBuffer: PropTypes.func,
  onStreamError: PropTypes.func,
  onStreamPlayerError: PropTypes.func,
  bufferingStream: PropTypes.bool,
  onMediaStreamListRestart: PropTypes.func,
  onStreamReplay: PropTypes.func,
  showRestart: PropTypes.bool,
  playingStream: PropTypes.bool,
  loadingStream: PropTypes.bool,
  currentMediaStreamListItem: PropTypes.object,
};
MediaStreamListPlayer.defaultProps = {
  loadingText: 'Loading media stream list player...',
  streamUrl: undefined,
  onSelectPreviousItem: undefined,
  onSelectNextItem: undefined,
  onStreamEnded: undefined,
  MediaStreamListPlayerEmpty: null,
  currentStreamIndex: undefined,
  mediaStreamListLength: undefined,
  onStreamReady: undefined,
  onStreamPlay: undefined,
  onStreamBuffer: undefined,
  onStreamError: undefined,
  onStreamPlayerError: undefined,
  bufferingStream: false,
  onMediaStreamListRestart: undefined,
  onStreamReplay: undefined,
  showRestart: false,
  playingStream: true,
  loadingStream: false,
  currentMediaStreamListItem: null,
};
MediaStreamListItem.propTypes = {
  mediaStreamListItem: PropTypes.object,
};
MediaStreamListItem.defaultProps = {
  mediaStreamListItem: null,
};

export default MediaStreamListPlayer;
