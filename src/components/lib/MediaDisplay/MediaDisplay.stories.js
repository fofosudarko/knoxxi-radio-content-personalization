import MediaDisplay from './MediaDisplay';

const Story = {
  title: 'Lib/MediaDisplay',
  component: MediaDisplay,
};

const Template = (args) => {
  return <MediaDisplay {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
