import PropTypes from 'prop-types';
import { Card } from 'react-bootstrap';
import ReactPlayer from 'react-player';
import Image from 'next/image';

import { IMAGE_PLACEHOLDER } from 'src/config';
import { useMediaType } from 'src/hooks';

function MediaDisplay({
  media,
  mediaHeight,
  mediaWidth,
  videoHeight,
  videoWidth,
  isCardImage,
  onClick,
}) {
  const { name, url = IMAGE_PLACEHOLDER } = media ?? {};
  const { isVideo } = useMediaType(media);

  return (
    <>
      {isVideo ? (
        <div onClick={onClick}>
          <ReactPlayer
            url={url}
            controls
            width={`${videoWidth}px`}
            height={`${videoHeight}px`}
          />
        </div>
      ) : isCardImage ? (
        <Card.Img
          variant="top"
          height={mediaHeight}
          width={mediaWidth}
          as={Image}
          src={url}
          alt={name}
          onClick={onClick}
        />
      ) : (
        <Image
          src={url}
          height={mediaHeight}
          width={mediaWidth}
          alt={name}
          onClick={onClick}
        />
      )}
    </>
  );
}

MediaDisplay.propTypes = {
  media: PropTypes.object,
  mediaHeight: PropTypes.number,
  mediaWidth: PropTypes.number,
  videoHeight: PropTypes.number,
  videoWidth: PropTypes.number,
  isCardImage: PropTypes.bool,
  onClick: PropTypes.func,
};
MediaDisplay.defaultProps = {
  media: null,
  mediaHeight: 400,
  mediaWidth: 400,
  videoHeight: undefined,
  videoWidth: undefined,
  isCardImage: false,
  onClick: undefined,
};

export default MediaDisplay;
