import dynamic from 'next/dynamic';

const AppShakaPlayer = dynamic(() => import('./AppShakaPlayer'), {
  ssr: false,
});

export default AppShakaPlayer;
