import AppShakaPlayer from './AppShakaPlayer';

const Story = {
  title: 'Lib/AppShakaPlayer',
  component: AppShakaPlayer,
};

const Template = (args) => {
  return <AppShakaPlayer {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
