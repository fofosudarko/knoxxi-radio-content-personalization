import StreamPlayer from './StreamPlayer';

const Story = {
  title: 'Lib/StreamPlayer',
  component: StreamPlayer,
};

const Template = (args) => {
  return <StreamPlayer {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
