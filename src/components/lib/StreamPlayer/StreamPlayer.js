import { forwardRef } from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';

const StreamPlayer = forwardRef(function ({ height, width, ...props }, ref) {
  return (
    <ReactPlayer
      width={`${width}px`}
      height={`${height}px`}
      {...props}
      ref={ref}
    />
  );
});

StreamPlayer.displayName = 'StreamPlayer';

StreamPlayer.propTypes = {
  height: PropTypes.number,
  width: PropTypes.number,
};
StreamPlayer.defaultProps = {
  height: 400,
  width: 400,
};

export default StreamPlayer;
