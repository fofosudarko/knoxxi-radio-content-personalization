import EnhancedStreamPlayer from './_EnhancedStreamPlayer';

const Story = {
  title: 'Lib/EnhancedStreamPlayer',
  component: EnhancedStreamPlayer,
};

const Template = (args) => {
  return <EnhancedStreamPlayer {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
