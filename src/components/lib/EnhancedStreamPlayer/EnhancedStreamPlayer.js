import { useEffect, useRef, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import 'shaka-player/dist/controls.css';

import AppShakaPlayer from '../AppShakaPlayer/AppShakaPlayer';

function EnhancedStreamPlayer({
  height,
  width,
  url,
  playing,
  controls,
  controlsList,
  poster,
  onEnded,
  onReady,
  onPlay,
  onBuffer,
  onError,
  onTimeUpdate,
  onPlayerInit,
  onPlayerError,
  onProgress,
  onUiInit,
  onVideoElementInit,
  onAudioElementInit,
  playsInline,
  crossOrigin,
  disablePictureInPicture,
  isAudio,
  ...props
}) {
  const [src, setSrc] = useState(url);
  const [autoPlay, setAutoPlay] = useState(playing);
  const controllerRef = useRef(null);
  const playerRef = useRef(null);
  const uiRef = useRef(null);
  const videoElementRef = useRef(null);
  const audioElementRef = useRef(null);
  let handleEnded,
    handleReady,
    handlePlay,
    handleProgress,
    handleBuffer,
    handleError,
    handleTimeUpdate,
    handlePlayerError;

  useEffect(() => {
    setSrc(url);
    setAutoPlay(playing);
    const {
      player = null,
      ui = null,
      videoElement = null,
      audioElement = null,
    } = controllerRef.current ?? {};
    playerRef.current = player;
    uiRef.current = ui;
    videoElementRef.current = videoElement;
    audioElementRef.current = audioElement;
    onPlayerInit && onPlayerInit(playerRef.current);
    onUiInit && onUiInit(uiRef.current);
    onVideoElementInit && onVideoElementInit(videoElementRef.current);
    onAudioElementInit && onAudioElementInit(audioElementRef.current);

    if (videoElementRef.current) {
      videoElementRef.current.controls = controls;
      videoElementRef.current.controlsList = controlsList;
      videoElementRef.current.poster = poster;
      videoElementRef.current.playsInline = playsInline;
      videoElementRef.current.crossOrigin = crossOrigin;
      videoElementRef.current.disablePictureInPicture = disablePictureInPicture;
      // add event listeners
      onEnded && handleEnded();
      onReady && handleReady();
      onPlay && handlePlay();
      onProgress && handleProgress();
      onBuffer && handleBuffer();
      onError && handleError();
      onTimeUpdate && handleTimeUpdate();
    }

    if (audioElementRef.current) {
      audioElementRef.current.controls = controls;
      audioElementRef.current.controlsList = controlsList;
      audioElementRef.current.crossOrigin = crossOrigin;
      // add event listeners
      onEnded && handleEnded();
      onReady && handleReady();
      onPlay && handlePlay();
      onProgress && handleProgress();
      onBuffer && handleBuffer();
      onError && handleError();
      onTimeUpdate && handleTimeUpdate();
    }

    if (playerRef.current) {
      onPlayerError && handlePlayerError();
    }
  }, [
    controls,
    controlsList,
    crossOrigin,
    disablePictureInPicture,
    handleBuffer,
    handleEnded,
    handleError,
    handlePlay,
    handlePlayerError,
    handleProgress,
    handleReady,
    handleTimeUpdate,
    onAudioElementInit,
    onBuffer,
    onEnded,
    onError,
    onPlay,
    onPlayerError,
    onPlayerInit,
    onProgress,
    onReady,
    onTimeUpdate,
    onUiInit,
    onVideoElementInit,
    playing,
    playsInline,
    poster,
    url,
  ]);

  handleEnded = useCallback(() => {
    isAudio
      ? audioElementRef.current.addEventListener('ended', onEnded)
      : videoElementRef.current.addEventListener('ended', onEnded);
  }, [isAudio, onEnded]);
  handleReady = useCallback(() => {
    isAudio
      ? audioElementRef.current.addEventListener('playing', onReady)
      : videoElementRef.current.addEventListener('playing', onReady);
  }, [isAudio, onReady]);
  handlePlay = useCallback(() => {
    isAudio
      ? audioElementRef.current.addEventListener('play', onPlay)
      : videoElementRef.current.addEventListener('play', onPlay);
  }, [isAudio, onPlay]);
  handleProgress = useCallback(() => {
    isAudio
      ? audioElementRef.current.addEventListener('progress', onProgress)
      : videoElementRef.current.addEventListener('progress', onProgress);
  }, [isAudio, onProgress]);
  handleBuffer = useCallback(() => {
    isAudio
      ? audioElementRef.current.addEventListener('waiting', onBuffer)
      : videoElementRef.current.addEventListener('waiting', onBuffer);
  }, [isAudio, onBuffer]);
  handleError = useCallback(() => {
    isAudio
      ? audioElementRef.current.addEventListener('error', onError)
      : videoElementRef.current.addEventListener('error', onError);
  }, [isAudio, onError]);
  handleTimeUpdate = useCallback(() => {
    isAudio
      ? audioElementRef.current.addEventListener('timeupdate', onTimeUpdate)
      : videoElementRef.current.addEventListener('timeupdate', onTimeUpdate);
  }, [isAudio, onTimeUpdate]);
  handlePlayerError = useCallback(() => {
    playerRef.current.addEventListener('error', (event) => {
      console.log('Player error event:', event);
      onPlayerError(new Error(JSON.stringify(event.detail)));
    });
  }, [onPlayerError]);

  return (
    <AppShakaPlayer
      ref={controllerRef}
      src={src}
      autoPlay={autoPlay}
      width={`${width}px`}
      height={`${height}px`}
      isAudio={isAudio}
      chromeless={isAudio}
      {...props}
    />
  );
}

/*<ShakaPlayer
      ref={controllerRef}
      src={src}
      autoPlay={autoPlay}
      width={`${width}px`}
      height={`${height}px`}
      {...props}
    />*/

EnhancedStreamPlayer.propTypes = {
  height: PropTypes.number,
  width: PropTypes.number,
  url: PropTypes.string,
  playing: PropTypes.bool,
  controls: PropTypes.bool,
  controlsList: PropTypes.string,
  onEnded: PropTypes.func,
  onReady: PropTypes.func,
  onPlay: PropTypes.func,
  onProgress: PropTypes.func,
  onPause: PropTypes.func,
  onBuffer: PropTypes.func,
  onError: PropTypes.func,
  onTimeUpdate: PropTypes.func,
  onPlayerInit: PropTypes.func,
  onPlayerError: PropTypes.func,
  onUiInit: PropTypes.func,
  onVideoElementInit: PropTypes.func,
  onAudioElementInit: PropTypes.func,
  poster: PropTypes.string,
  playsInline: PropTypes.bool,
  crossOrigin: PropTypes.string,
  disablePictureInPicture: PropTypes.bool,
  isAudio: PropTypes.bool,
};
EnhancedStreamPlayer.defaultProps = {
  height: 400,
  width: 400,
  url: undefined,
  playing: false,
  controls: undefined,
  controlsList: undefined,
  onEnded: undefined,
  onReady: undefined,
  onPlay: undefined,
  onProgress: undefined,
  onPause: undefined,
  onBuffer: undefined,
  onError: undefined,
  onTimeUpdate: undefined,
  onPlayerInit: undefined,
  onPlayerError: undefined,
  onUiInit: undefined,
  onVideoElementInit: undefined,
  onAudioElementInit: undefined,
  poster: undefined,
  playsInline: false,
  crossOrigin: undefined,
  disablePictureInPicture: false,
  isAudio: false,
};

export default EnhancedStreamPlayer;
