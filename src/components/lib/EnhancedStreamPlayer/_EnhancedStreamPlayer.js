import dynamic from 'next/dynamic';

const EnhancedStreamPlayer = dynamic(() => import('./EnhancedStreamPlayer'), {
  ssr: false,
});

export default EnhancedStreamPlayer;
