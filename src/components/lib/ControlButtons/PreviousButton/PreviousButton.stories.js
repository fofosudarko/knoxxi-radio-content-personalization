import PreviousButton from './PreviousButton';

const Story = {
  title: 'Lib/ControlButtons/PreviousButton',
  component: PreviousButton,
};

const Template = (args) => {
  return <PreviousButton {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
