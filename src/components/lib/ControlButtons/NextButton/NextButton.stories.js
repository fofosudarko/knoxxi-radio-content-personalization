import NextButton from './NextButton';

const Story = {
  title: 'Lib/ControlButtons/NextButton',
  component: NextButton,
};

const Template = (args) => {
  return <NextButton {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
