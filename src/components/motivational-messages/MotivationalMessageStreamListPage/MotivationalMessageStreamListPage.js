import { useEffect } from 'react';
import PropTypes from 'prop-types';

import { useResetMotivationalMessageStreamList } from 'src/hooks/api';
import { useMediaStreamListStore } from 'src/stores/media-stream-list';

import { MotivationalMessageStreamListListView } from '../MotivationalMessageStreamList/MotivationalMessageStreamListList';

function MotivationalMessageStreamListPage({ streamedById }) {
  const query = { streamedById };
  const handleResetMotivationalMessageStreamList =
    useResetMotivationalMessageStreamList({ streamedById });
  const { clearMediaStreamListState } = useMediaStreamListStore(
    (state) => state
  );

  useEffect(() => {
    handleResetMotivationalMessageStreamList();
    clearMediaStreamListState();
  }, [clearMediaStreamListState, handleResetMotivationalMessageStreamList]);

  return (
    <div>
      <MotivationalMessageStreamListListView query={query} />
    </div>
  );
}

MotivationalMessageStreamListPage.propTypes = {
  streamedById: PropTypes.string,
};
MotivationalMessageStreamListPage.defaultProps = {
  streamedById: undefined,
};

export default MotivationalMessageStreamListPage;
