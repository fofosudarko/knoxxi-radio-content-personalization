import MotivationalMessageStreamListPage from './MotivationalMessageStreamListPage';

const Story = {
  title: 'Pages/MotivationalMessageStreamList',
  component: MotivationalMessageStreamListPage,
};

const Template = (args) => {
  return <MotivationalMessageStreamListPage {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
