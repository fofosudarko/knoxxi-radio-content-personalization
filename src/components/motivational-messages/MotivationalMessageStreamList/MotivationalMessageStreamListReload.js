import PropTypes from 'prop-types';

import { useResetMotivationalMessageStreamList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function MotivationalMessageStreamListReload({ useTooltip, text, query }) {
  const handleResetMotivationalMessageStreamList =
    useResetMotivationalMessageStreamList({ ...query });

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetMotivationalMessageStreamList}
      text={text}
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

MotivationalMessageStreamListReload.propTypes = {
  useTooltip: PropTypes.bool,
  text: PropTypes.string,
  query: PropTypes.object,
};
MotivationalMessageStreamListReload.defaultProps = {
  useTooltip: false,
  text: 'Reload music stream list',
  query: null,
};

export default MotivationalMessageStreamListReload;
