import MotivationalMessageStreamListReload from './MotivationalMessageStreamListReload';

const Story = {
  title: 'MotivationalMessageStreamList/Reload',
  component: MotivationalMessageStreamListReload,
};

const Template = (args) => {
  return <MotivationalMessageStreamListReload {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
