import { useEffect, useCallback, useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { BsFolder2Open } from 'react-icons/bs';
import { TbPlaylist } from 'react-icons/tb';
import { Stack, Offcanvas } from 'react-bootstrap';

//import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions, useNotificationHandler } from 'src/hooks';
import { useListMotivationalMessageStreamList } from 'src/hooks/api';

import { NewHOC, ShowButton } from 'src/components/lib';
import { MotivationalMessageStreamListGridItem } from './MotivationalMessageStreamListItem';
import MotivationalMessageStreamListReload from './MotivationalMessageStreamListReload';
import { MediaStreamListProvider, MediaStreamListContext } from 'src/context';
import { MediaStreamPlayerContainer } from 'src/components/media-stream-list-player';

import { useHasMounted } from 'src/hooks';

function MotivationalMessageStreamListListContainer({ query }) {
  const {
    motivationalMessageStreamList,
    error: motivationalMessageStreamListError,
    setError,
    setPage,
    page,
    endPaging,
    motivationalMessageStreamAdverts,
    motivationalMessageStreamScripts,
  } = useListMotivationalMessageStreamList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (motivationalMessageStreamListError) {
      handleNotification(motivationalMessageStreamListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, motivationalMessageStreamListError, setError]);

  const handleLoadMoreMotivationalMessageStreamList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <Stack direction="vertical" gap={1}>
      <MediaStreamListProvider
        mediaStreamList={motivationalMessageStreamList}
        mediaStreamAdverts={motivationalMessageStreamAdverts}
        mediaStreamScripts={motivationalMessageStreamScripts}
      >
        <MediaStreamPlayerContainer />
        <MotivationalMessageStreamListListToggle
          motivationalMessageStreamList={motivationalMessageStreamList}
          onLoadMore={handleLoadMoreMotivationalMessageStreamList}
          loadingText="Loading motivational message stream list..."
          MotivationalMessageStreamListListEmpty={
            MotivationalMessageStreamListListEmpty
          }
          isDisabled={endPaging}
          query={query}
          hideToggle
        />
      </MediaStreamListProvider>
    </Stack>
  );
}

function MotivationalMessageStreamListList({
  motivationalMessageStreamList,
  loadingText,
  //onLoadMore,
  MotivationalMessageStreamListListEmpty,
  query,
  //isDisabled,
}) {
  const {
    currentMediaStreamListItem: currentMotivationalMessageStreamListItem,
    handleSelectMediaStreamListItem:
      handleSelectMotivationalMessageStreamListItem,
    shouldPlayScript,
    shouldPlayAdvert,
  } = useContext(MediaStreamListContext);
  const showStreamPlayerAction = !shouldPlayScript && !shouldPlayAdvert;
  const hasMounted = useHasMounted();

  if (!hasMounted) {
    return <div>{loadingText}</div>;
  }

  if (!motivationalMessageStreamList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(MotivationalMessageStreamListListEmpty);

  return motivationalMessageStreamList.length ? (
    <Stack direction="vertical" gap={2}>
      <div>
        <MotivationalMessageStreamListListGrid
          motivationalMessageStreamList={motivationalMessageStreamList}
          showStreamPlayerAction={showStreamPlayerAction}
          currentMotivationalMessageStreamListItem={
            currentMotivationalMessageStreamListItem
          }
          onSelectMotivationalMessageStreamListItem={
            handleSelectMotivationalMessageStreamListItem
          }
          query={query}
        />
      </div>
      {/*showStreamPlayerAction &&
      motivationalMessageStreamList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more motivational messages"
          isDisabled={isDisabled}
        />
      ) : null*/}
    </Stack>
  ) : (
    <ListEmpty />
  );
}

export function MotivationalMessageStreamListListGrid({
  motivationalMessageStreamList,
  showStreamPlayerAction,
  currentMotivationalMessageStreamListItem,
  onSelectMotivationalMessageStreamListItem,
  query,
}) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div>
      {showStreamPlayerAction ? (
        <div className="d-flex justify-content-end">
          <MotivationalMessageStreamListReload
            useTooltip={isLargeDevice}
            text="Reload"
            query={query}
          />
        </div>
      ) : null}
      <Stack
        direction="vertical"
        gap={2}
        className={`${!showStreamPlayerAction ? 'pt-4' : ''}`}
      >
        {motivationalMessageStreamList.map((item) => (
          <MotivationalMessageStreamListGridItem
            motivationalMessageStreamListItem={item}
            key={item.id}
            isSelected={
              item.id === currentMotivationalMessageStreamListItem?.id
            }
            onSelect={
              showStreamPlayerAction
                ? onSelectMotivationalMessageStreamListItem
                : undefined
            }
          />
        ))}
      </Stack>
    </div>
  );
}

export function MotivationalMessageStreamListListView({ query }) {
  return (
    <div>
      <div className="text-primary text-center site-fs-4 fw-bold mb-2">
        Streaming motivational messages
      </div>
      <MotivationalMessageStreamListListContainer query={query} />
    </div>
  );
}

export function MotivationalMessageStreamListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <BsFolder2Open size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no motivational message stream list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

function MotivationalMessageStreamListListToggle({
  motivationalMessageStreamList,
  loadingText,
  onLoadMore,
  MotivationalMessageStreamListListEmpty,
  isDisabled,
  query,
  hideToggle,
}) {
  const { isLargeDevice } = useDeviceDimensions();
  const [
    showMotivationalMessageStreamList,
    setShowMotivationalMessageStreamList,
  ] = useState(false);

  const handleClose = () => setShowMotivationalMessageStreamList(false);
  const handleShow = () => setShowMotivationalMessageStreamList(true);

  return !hideToggle ? (
    <div>
      <ShowButton
        Icon={<TbPlaylist size={25} />}
        onClick={handleShow}
        useTooltip={isLargeDevice}
        autoWidth
        text="Playlist"
      />
      <Offcanvas
        show={showMotivationalMessageStreamList}
        onHide={handleClose}
        placement="end"
      >
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Motivational message stream list</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <MotivationalMessageStreamListList
            motivationalMessageStreamList={motivationalMessageStreamList}
            onLoadMore={onLoadMore}
            loadingText={loadingText}
            MotivationalMessageStreamListListEmpty={
              MotivationalMessageStreamListListEmpty
            }
            isDisabled={isDisabled}
            query={query}
          />
        </Offcanvas.Body>
      </Offcanvas>
    </div>
  ) : null;
}

MotivationalMessageStreamListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
MotivationalMessageStreamListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
MotivationalMessageStreamListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  motivationalMessageStreamList: PropTypes.array,
  MotivationalMessageStreamListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
  query: PropTypes.object,
};
MotivationalMessageStreamListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading motivational message stream list...',
  onLoadMore: undefined,
  motivationalMessageStreamList: null,
  MotivationalMessageStreamListListEmpty: null,
  isDisabled: false,
  query: null,
};
MotivationalMessageStreamListListGrid.propTypes = {
  appUser: PropTypes.object,
  motivationalMessageStreamList: PropTypes.array,
  showStreamPlayerAction: PropTypes.bool,
  currentMotivationalMessageStreamListItem: PropTypes.object,
  onSelectMotivationalMessageStreamListItem: PropTypes.func,
  query: PropTypes.object,
};
MotivationalMessageStreamListListGrid.defaultProps = {
  appUser: null,
  motivationalMessageStreamList: null,
  showStreamPlayerAction: false,
  currentMotivationalMessageStreamListItem: null,
  onSelectMotivationalMessageStreamListItem: undefined,
  query: null,
};
MotivationalMessageStreamListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
MotivationalMessageStreamListListView.defaultProps = {
  appUser: null,
  query: null,
};
MotivationalMessageStreamListListEmpty.propTypes = {
  appUser: PropTypes.object,
};
MotivationalMessageStreamListListEmpty.defaultProps = {
  appUser: null,
};
MotivationalMessageStreamListListToggle.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  motivationalMessageStreamList: PropTypes.array,
  MotivationalMessageStreamListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
  query: PropTypes.object,
  hideToggle: PropTypes.bool,
};
MotivationalMessageStreamListListToggle.defaultProps = {
  appUser: null,
  loadingText: 'Loading motivationalMessage stream list...',
  onLoadMore: undefined,
  motivationalMessageStreamList: null,
  MotivationalMessageStreamListListEmpty: null,
  isDisabled: false,
  query: null,
  hideToggle: false,
};

export default MotivationalMessageStreamListList;
