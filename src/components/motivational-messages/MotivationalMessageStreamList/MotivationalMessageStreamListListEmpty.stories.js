import { MotivationalMessageStreamListListEmpty } from './MotivationalMessageStreamListList';

const Story = {
  title: 'MotivationalMessageStreamList/ListEmpty',
  component: MotivationalMessageStreamListListEmpty,
};

const Template = (args) => {
  return <MotivationalMessageStreamListListEmpty {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
