import PropTypes from 'prop-types';
import { Dropdown, Row, Col } from 'react-bootstrap';
import { FaEllipsisV } from 'react-icons/fa';

import { useDeviceDimensions } from 'src/hooks';
import { getMediaStreamListItemDetails } from 'src/utils';

export function useMotivationalMessageStreamListItemDetails(
  mediaStreamListItem
) {
  return getMediaStreamListItemDetails(mediaStreamListItem);
}

export function MotivationalMessageStreamListGridItem({
  motivationalMessageStreamListItem,
  isSelected,
  onSelect,
}) {
  const { name, genre } = useMotivationalMessageStreamListItemDetails(
    motivationalMessageStreamListItem
  );

  return (
    <Row>
      <Col>
        <div
          className={`site-fs-5 fw-bold ${
            isSelected ? 'text-primary' : 'text-secondary'
          }`}
          onClick={() => {
            onSelect && onSelect(motivationalMessageStreamListItem);
          }}
          style={{ cursor: 'pointer' }}
        >
          {name}
        </div>
        <div className="d-flex justify-content-between">
          <div
            className={`site-fs-5 fw-light ${
              isSelected ? 'text-primary' : 'text-secondary'
            }`}
          >
            {genre}
          </div>
        </div>
        {/*<td>
        <div className="py-1 w-100 justify-end">
          <MotivationalMessageStreamListItemActions
            motivationalMessageStreamListItem={motivationalMessageStreamListItem}
          />
        </div>
      </td>*/}
      </Col>
    </Row>
  );
}

export function MotivationalMessageStreamListItemActions() {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item></Dropdown.Item>
          <Dropdown.Item></Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

MotivationalMessageStreamListGridItem.propTypes = {
  motivationalMessageStreamListItem: PropTypes.object,
  isSelected: PropTypes.bool,
  onSelect: PropTypes.func,
};
MotivationalMessageStreamListGridItem.defaultProps = {
  motivationalMessageStreamListItem: null,
  isSelected: false,
  onSelect: undefined,
};
MotivationalMessageStreamListItemActions.propTypes = {
  motivationalMessageStreamListItem: PropTypes.object,
};
MotivationalMessageStreamListItemActions.defaultProps = {
  motivationalMessageStreamListItem: null,
};
