import MotivationalMessageStreamListList from './MotivationalMessageStreamListList';

const Story = {
  title: 'MotivationalMessageStreamList/List',
  component: MotivationalMessageStreamListList,
};

const Template = (args) => {
  return <MotivationalMessageStreamListList {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
