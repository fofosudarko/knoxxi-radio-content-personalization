import { MotivationalMessageStreamListListView } from './MotivationalMessageStreamListList';

const Story = {
  title: 'MotivationalMessageStreamList/ListView',
  component: MotivationalMessageStreamListListView,
};

const Template = (args) => {
  return <MotivationalMessageStreamListListView {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
