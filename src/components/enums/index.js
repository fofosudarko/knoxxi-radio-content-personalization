// index.js

import ContentCategorySelect from './ContentCategorySelect/ContentCategorySelect';
import ContentSubCategorySelect from './ContentSubCategorySelect/ContentSubCategorySelect';

export { ContentCategorySelect, ContentSubCategorySelect };
