import { forwardRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListEnums } from 'src/hooks/api';
import { humanizeString } from 'src/utils';

import { ItemSelect } from 'src/components/lib';

function getValueFromContentCategory(contentCategory = null) {
  if (!contentCategory) {
    return '';
  }

  let value;
  if (Array.isArray(contentCategory)) {
    value = contentCategory.map((i) =>
      JSON.stringify({
        name: i.name,
        choicesUrl: i.choicesUrl,
      })
    );
  } else {
    value = JSON.stringify({
      name: contentCategory.name,
      choicesUrl: contentCategory.choicesUrl,
    });
  }

  return value;
}

function getContentCategoryOptionItem(contentCategory = null) {
  if (!contentCategory) {
    return {};
  }

  const value = getValueFromContentCategory(contentCategory);
  const label = humanizeString({
    string: contentCategory.name,
    capitalize: true,
  });

  return {
    value,
    label,
  };
}

const ContentCategorySelect = forwardRef(function ContentCategorySelect(
  {
    name,
    onChange,
    onBlur,
    errors,
    value,
    onFocus,
    isClearable,
    isDisabled,
    placeholder,
    helpText,
    hideLabel,
    label,
    labelClass,
    isFiltered,
  },
  ref
) {
  const selectProps = {
    name,
    onBlur,
    errors,
    value,
    onFocus,
    isClearable,
    onChange,
    isDisabled,
    placeholder,
    helpText,
    hideLabel,
    label,
    labelClass,
  };
  const {
    error,
    setError,
    contentCategories,
    setContentCategory,
    handleListContentCategory,
  } = useListEnums();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (!contentCategories?.length) {
      (async () => {
        await handleListContentCategory(isFiltered);
      })();
    }
  }, [contentCategories, handleListContentCategory, isFiltered]);

  useEffect(() => {
    return () => {
      setContentCategory(null);
    };
  }, [setContentCategory]);

  return (
    <ItemSelect
      {...selectProps}
      ref={ref}
      items={contentCategories}
      getOptionItem={getContentCategoryOptionItem}
    />
  );
});

ContentCategorySelect.displayName = 'ContentCategorySelect';

ContentCategorySelect.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  value: PropTypes.any,
  isClearable: PropTypes.bool,
  isDisabled: PropTypes.bool,
  helpText: PropTypes.string,
  placeholder: PropTypes.string,
  errors: PropTypes.object,
  hideLabel: PropTypes.bool,
  label: PropTypes.string,
  labelClass: PropTypes.string,
  isFiltered: PropTypes.bool,
};
ContentCategorySelect.defaultProps = {
  name: undefined,
  onChange: undefined,
  onBlur: undefined,
  onFocus: undefined,
  value: undefined,
  isClearable: false,
  isDisabled: false,
  helpText: undefined,
  placeholder: undefined,
  errors: null,
  hideLabel: false,
  label: undefined,
  labelClass: undefined,
  isFiltered: false,
};

export default ContentCategorySelect;
