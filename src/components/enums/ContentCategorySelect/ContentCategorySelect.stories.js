// ContentCategorySelect.stories.js

import ContentCategorySelect from './ContentCategorySelect';

const Story = {
  title: 'Enums/ContentCategorySelect',
  component: ContentCategorySelect,
};

const Template = (args) => {
  return <ContentCategorySelect {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
