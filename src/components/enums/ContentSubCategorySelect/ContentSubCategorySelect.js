import { forwardRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListEnums } from 'src/hooks/api';
import { humanizeString } from 'src/utils';

import { ItemSelect } from 'src/components/lib';

function getValueFromContentSubCategory(contentSubCategory = null) {
  if (!contentSubCategory) {
    return '';
  }

  let value;
  if (Array.isArray(contentSubCategory)) {
    value = contentSubCategory.map((i) =>
      JSON.stringify({
        name: i.name,
      })
    );
  } else {
    value = JSON.stringify({
      name: contentSubCategory.name,
    });
  }

  return value;
}

function getContentSubCategoryOptionItem(contentSubCategory = null) {
  if (!contentSubCategory) {
    return {};
  }

  return {
    value: getValueFromContentSubCategory(contentSubCategory),
    label: (
      <div className="d-flex flex-row">
        <div className="d-flex align-self-center">
          {humanizeString({
            string: contentSubCategory.name,
            capitalize: true,
          })}
        </div>
      </div>
    ),
  };
}

const ContentSubCategorySelect = forwardRef(function ContentSubCategorySelect(
  {
    name,
    onChange,
    onBlur,
    errors,
    value,
    onFocus,
    isClearable,
    isDisabled,
    isMultiple,
    label,
    labelClass,
    helpText,
    placeholder,
    contentCategory,
    isAsync,
    isFiltered,
  },
  ref
) {
  const selectProps = {
    name,
    onBlur,
    errors,
    value,
    onFocus,
    isClearable,
    onChange,
    isDisabled,
    isMultiple,
    label,
    labelClass,
    helpText,
    placeholder,
    isSync: !isAsync,
  };
  const {
    error,
    setError,
    contentSubCategories,
    setContentSubCategory,
    handleListContentSubCategory,
  } = useListEnums();
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (error) {
      handleNotification(error);
    }

    return () => {
      setError(null);
    };
  }, [error, handleNotification, setError]);

  useEffect(() => {
    if (!contentSubCategories?.length) {
      (async () => {
        await handleListContentSubCategory(contentCategory, isFiltered);
      })();
    }
  }, [
    contentCategory,
    contentSubCategories?.length,
    handleListContentSubCategory,
    isFiltered,
  ]);

  useEffect(() => {
    if (contentCategory) {
      (async () => {
        await handleListContentSubCategory(contentCategory, isFiltered);
      })();
    }
  }, [contentCategory, handleListContentSubCategory, isFiltered]);

  useEffect(() => {
    return () => {
      setContentSubCategory(null);
    };
  }, [setContentSubCategory]);

  return (
    <ItemSelect
      {...selectProps}
      ref={ref}
      items={contentSubCategories}
      getOptionItem={getContentSubCategoryOptionItem}
    />
  );
});

ContentSubCategorySelect.displayName = 'ContentSubCategorySelect';

ContentSubCategorySelect.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  value: PropTypes.any,
  isClearable: PropTypes.bool,
  isDisabled: PropTypes.bool,
  helpText: PropTypes.string,
  placeholder: PropTypes.string,
  isMultiple: PropTypes.bool,
  label: PropTypes.string,
  labelClass: PropTypes.string,
  contentCategory: PropTypes.object,
  errors: PropTypes.object,
  isAsync: PropTypes.bool,
  isFiltered: PropTypes.bool,
};
ContentSubCategorySelect.defaultProps = {
  name: undefined,
  onChange: undefined,
  onBlur: undefined,
  onFocus: undefined,
  value: undefined,
  isClearable: false,
  isDisabled: false,
  helpText: undefined,
  placeholder: undefined,
  isMultiple: false,
  label: undefined,
  labelClass: undefined,
  errors: null,
  contentCategory: null,
  isAsync: false,
  isFiltered: false,
};

export default ContentSubCategorySelect;
