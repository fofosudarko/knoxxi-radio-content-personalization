// BrandsSelect.stories.js

import BrandsSelect from './ContentSubCategorySelect';

const Story = {
  title: 'Brands/Select',
  component: BrandsSelect,
};

const Template = (args) => {
  return <BrandsSelect {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
