import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import { BsFolder2Open } from 'react-icons/bs';

import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import { NewHOC, LoadMore } from 'src/components/lib';
import { CarrierCampaignGridItem } from './CarrierCampaignItem';

export function CarrierCampaignsList({
  carrierCampaigns,
  loadingText,
  appUser,
  onLoadMore,
  CarrierCampaignsListEmpty,
  isDisabled,
}) {
  if (!carrierCampaigns) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(CarrierCampaignsListEmpty);

  return carrierCampaigns.length ? (
    <div>
      <CarrierCampaignsListGrid
        carrierCampaigns={carrierCampaigns}
        appUser={appUser}
      />

      {carrierCampaigns.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more campaigns"
          isDisabled={isDisabled}
        />
      ) : null}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function CarrierCampaignsListGrid({ carrierCampaigns, appUser }) {
  return (
    <>
      <Row xs={{ cols: 1 }} md={{ cols: 2 }} className="gx-2 gy-2">
        {carrierCampaigns.map((item) => (
          <Col key={item.id}>
            <CarrierCampaignGridItem carrierCampaign={item} appUser={appUser} />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function CarrierCampaignsListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <BsFolder2Open size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there are no campaigns yet.
          </div>
        </div>
      </div>
    </div>
  );
}

CarrierCampaignsList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  carrierCampaigns: PropTypes.array,
  CarrierCampaignsListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
CarrierCampaignsList.defaultProps = {
  appUser: null,
  loadingText: 'Loading carrier campaigns...',
  onLoadMore: undefined,
  carrierCampaigns: null,
  CarrierCampaignsListEmpty: null,
  isDisabled: false,
};
CarrierCampaignsListGrid.propTypes = {
  appUser: PropTypes.object,
  carrierCampaigns: PropTypes.array,
};
CarrierCampaignsListGrid.defaultProps = {
  appUser: null,
  carrierCampaigns: null,
};
CarrierCampaignsListEmpty.propTypes = {
  appUser: PropTypes.object,
};
CarrierCampaignsListEmpty.defaultProps = {
  appUser: null,
};
