import PropTypes from 'prop-types';
import Link from 'next/link';

import { useDeviceDimensions } from 'src/hooks';

import { MediaDisplay } from 'src/components/lib';

export function useCarrierCampaignDetails(carrierCampaign) {
  const { carrier = null, campaign = null, status } = carrierCampaign ?? {};
  const carrierVehicleInfoId = carrier.vehicleInfo?.id,
    carrierVehicleInfoName = carrier.vehicleInfo?.name,
    carrierVehicleInfoVin = carrier.vehicleInfo?.vin,
    carrierVehicleInfoMakeYear = carrier.vehicleInfo?.makeYear,
    carrierVehicleInfoBrand = carrier.vehicleInfo?.brand,
    carrierVehicleInfoModel = carrier.vehicleInfo?.model,
    carrierVehicleInfoRegistrationPlate =
      carrier.vehicleInfo?.registrationPlate,
    carrierVehicleInfoMedia = carrier.vehicleInfo?.media;
  const carrierDriverInfoId = carrier.driverInfo?.id,
    carrierDriverInfoStatus = carrier.driverInfo?.status,
    carrierDriverInfoFullname = carrier.driverInfo?.fullname,
    carrierDriverInfoUsername = carrier.driverInfo?.username,
    carrierDriverInfoMobileNumber = carrier.driverInfo?.mobileNumber,
    carrierDriverInfoProfilePicture = carrier.driverInfo?.profilePicture;
  const campaignId = campaign.id,
    campaignName = campaign.name,
    campaignChannels = campaign.channels,
    campaignSubscribedBy = campaign.subscribedBy,
    campaignDescription = campaign.description,
    campaignInstructions = campaign.instructions,
    campaignLandingPageUrl = campaign.landingPageUrl,
    campaignBanner = campaign.banner,
    campaignQrCodePayload = campaign.qrCodePayload;

  return {
    carrierVehicleInfoId,
    carrierVehicleInfoName,
    carrierVehicleInfoVin,
    carrierVehicleInfoMakeYear,
    carrierVehicleInfoBrand,
    carrierVehicleInfoModel,
    carrierVehicleInfoRegistrationPlate,
    carrierVehicleInfoMedia,
    carrierDriverInfoId,
    carrierDriverInfoStatus,
    carrierDriverInfoFullname,
    carrierDriverInfoUsername,
    carrierDriverInfoMobileNumber,
    carrierDriverInfoProfilePicture,
    campaignId,
    campaignName,
    campaignChannels,
    campaignSubscribedBy,
    campaignDescription,
    campaignInstructions,
    campaignLandingPageUrl,
    campaignBanner,
    campaignQrCodePayload,
    status,
  };
}

export function CarrierCampaignGridItem({ carrierCampaign }) {
  const { isLargeDevice } = useDeviceDimensions();
  const {
    campaignName,
    campaignDescription,
    campaignInstructions,
    campaignBanner,
    campaignQrCodePayload,
  } = useCarrierCampaignDetails(carrierCampaign);

  return (
    <div>
      <Link
        href={campaignQrCodePayload ? campaignQrCodePayload : '#'}
        target="_blank"
        style={{ cursor: 'pointer' }}
      >
        <div>
          <MediaDisplay
            media={campaignBanner}
            mediaHeight={300}
            mediaWidth={isLargeDevice ? 360 : 305}
          />
        </div>
      </Link>
      <div className="w-100 p-2">
        <div className="item-title">{campaignName ? campaignName : 'N/A'}</div>
        <div>{campaignDescription ? campaignDescription : 'N/A'}</div>
        <div>{campaignInstructions ? campaignInstructions : 'N/A'}</div>
      </div>
    </div>
  );
}

CarrierCampaignGridItem.propTypes = {
  carrierCampaign: PropTypes.object,
};
CarrierCampaignGridItem.defaultProps = {
  carrierCampaign: null,
};
