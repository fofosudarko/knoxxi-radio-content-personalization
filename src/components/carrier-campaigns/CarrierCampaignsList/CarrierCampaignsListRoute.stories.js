import CarrierCampaignsListRoute from './CarrierCampaignsListRoute';

const Story = {
  title: 'CarrierCampaigns/ListRoute',
  component: CarrierCampaignsListRoute,
};

const Template = (args) => {
  return <CarrierCampaignsListRoute {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
