import CarrierCampaignsReload from './CarrierCampaignsReload';

const Story = {
  title: 'CarrierCampaigns/Reload',
  component: CarrierCampaignsReload,
};

const Template = (args) => {
  return <CarrierCampaignsReload {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
