import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { EditButton } from 'src/components/lib';

export default function CarrierCampaignsListRoute({
  useTooltip,
  carrierId,
  text,
}) {
  const { handleCarrierCampaignsRoute } =
    useRoutes().useCarrierCampaignsRoute();

  return (
    <EditButton
      onClick={() => handleCarrierCampaignsRoute({ query: { carrierId } })}
      variant="warning"
      text={text}
      textColor="black"
      useTooltip={useTooltip}
    />
  );
}

CarrierCampaignsListRoute.propTypes = {
  useTooltip: PropTypes.bool,
  appUser: PropTypes.object,
  carrierId: PropTypes.string,
  text: PropTypes.string,
};
CarrierCampaignsListRoute.defaultProps = {
  useTooltip: false,
  appUser: null,
  carrierId: undefined,
  text: 'Manage campaigns',
};
