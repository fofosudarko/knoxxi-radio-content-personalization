import { CarrierCampaignsListEmpty } from './CarrierCampaignsList';

const Story = {
  title: 'CarrierCampaigns/ListEmpty',
  component: CarrierCampaignsListEmpty,
};

const Template = (args) => {
  return <CarrierCampaignsListEmpty {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
