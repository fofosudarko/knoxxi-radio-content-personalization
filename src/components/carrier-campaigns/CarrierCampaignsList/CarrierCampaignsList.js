import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListCarrierCampaigns } from 'src/hooks/api';

import { GoBack } from 'src/components/lib';
import CarrierCampaignsReload from './CarrierCampaignsReload';
import {
  CarrierCampaignsList,
  CarrierCampaignsListEmpty,
} from './_CarrierCampaignsList';

function CarrierCampaignsListContainer({ appUser, query }) {
  const {
    carrierCampaigns,
    error: carrierCampaignsError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListCarrierCampaigns({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (carrierCampaignsError) {
      handleNotification(carrierCampaignsError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, carrierCampaignsError, setError]);

  const handleLoadMoreCarrierCampaigns = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <CarrierCampaignsList
      carrierCampaigns={carrierCampaigns}
      onLoadMore={handleLoadMoreCarrierCampaigns}
      loadingText="Loading carrier campaigns..."
      appUser={appUser}
      CarrierCampaignsListEmpty={CarrierCampaignsListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function CarrierCampaignsListView({ appUser, query }) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-between">
        <GoBack hideText />
        <CarrierCampaignsReload useTooltip query={query} />
      </div>
      <div className="fw-bold text-primary fs-5 my-2 text-center">
        Ad campaigns
      </div>
      <div className="mt-4">
        <CarrierCampaignsListContainer appUser={appUser} query={query} />
      </div>
    </div>
  );
}

CarrierCampaignsListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
CarrierCampaignsListContainer.defaultProps = {
  appUser: null,
  query: null,
};
CarrierCampaignsList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  carrierCampaigns: PropTypes.array,
  CarrierCampaignsListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
CarrierCampaignsList.defaultProps = {
  appUser: null,
  loadingText: 'Loading campaigns...',
  onLoadMore: undefined,
  carrierCampaigns: null,
  CarrierCampaignsListEmpty: null,
  isDisabled: false,
};
CarrierCampaignsListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
CarrierCampaignsListView.defaultProps = {
  appUser: null,
  query: null,
};

export default CarrierCampaignsList;
