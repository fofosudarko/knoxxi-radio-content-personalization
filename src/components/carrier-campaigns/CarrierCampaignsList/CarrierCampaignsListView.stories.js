import { useAuthStore } from 'src/stores/auth';
import { CarrierCampaignsListView } from './CarrierCampaignsList';

const Story = {
  title: 'CarrierCampaigns/ListView',
  component: CarrierCampaignsListView,
};

const Template = (args) => {
  const appUser = useAuthStore((state) => state.appUser);
  return <CarrierCampaignsListView {...args} appUser={appUser} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
