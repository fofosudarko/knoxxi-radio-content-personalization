import CarrierCampaignItem from './CarrierCampaignItem';

const Story = {
  title: 'CarrierCampaigns/Item',
  component: CarrierCampaignItem,
};

const Template = (args) => {
  return <CarrierCampaignItem {...args} />;
};

export const Main = Template.bind({});
Main.args = {
  carrierCampaign: {
    id: '62e454d8fbf633bb97198f3a',
    createdOn: 1659131096233,
    updatedOn: null,
    number: 200,
    carrierCampaign:
      'Good morning... why dream when the day gives an opportunity of a reality. Magnify your dreams and put in the action. From knoxxi with love, today was meant to be won.',
  },
};

export default Story;
