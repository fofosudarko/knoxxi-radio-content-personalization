import CarrierCampaignsList from './CarrierCampaignsList';

const Story = {
  title: 'CarrierCampaigns/List',
  component: CarrierCampaignsList,
};

const Template = (args) => {
  return <CarrierCampaignsList {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
