import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetCarrierCampaigns } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function CarrierCampaignsReload({ useTooltip, query }) {
  const _handleResetCarrierCampaigns = useResetCarrierCampaigns({
    ...query,
  });

  const handleResetCarrierCampaigns = useCallback(() => {
    _handleResetCarrierCampaigns();
  }, [_handleResetCarrierCampaigns]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetCarrierCampaigns}
      text="Reload campaigns"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

CarrierCampaignsReload.propTypes = {
  useTooltip: PropTypes.bool,
  query: PropTypes.object,
};
CarrierCampaignsReload.defaultProps = {
  useTooltip: false,
  query: null,
};

export default CarrierCampaignsReload;
