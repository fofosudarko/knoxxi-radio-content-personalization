import CarrierCampaignsPage from './CarrierCampaignsPage';

const Story = {
  title: 'Pages/CarrierCampaigns',
  component: CarrierCampaignsPage,
};

const Template = (args) => {
  return <CarrierCampaignsPage {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
