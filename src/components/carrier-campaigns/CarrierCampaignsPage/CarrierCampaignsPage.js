import PropTypes from 'prop-types';

import { CarrierCampaignsListView } from '../CarrierCampaignsList/CarrierCampaignsList';

function CarrierCampaignsPage({ appUser, carrierId }) {
  const query = { carrierId, status: 'ACTIVE' };
  return (
    <div>
      <CarrierCampaignsListView appUser={appUser} query={query} />
    </div>
  );
}

CarrierCampaignsPage.propTypes = {
  appUser: PropTypes.object,
  carrierId: PropTypes.string,
};
CarrierCampaignsPage.defaultProps = {
  appUser: null,
  carrierId: undefined,
};

export default CarrierCampaignsPage;
