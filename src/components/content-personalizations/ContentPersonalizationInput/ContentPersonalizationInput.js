import { useCallback, useMemo, useEffect, useReducer } from 'react';
import PropTypes from 'prop-types';
import { Form, Row, Col, Stack } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { object as Yup } from 'yup';
import { produce } from 'immer';
import { BsArrowLeft, BsArrowRight, BsCheckCircle } from 'react-icons/bs';
import ReCAPTCHA from 'react-google-recaptcha';

import { RECAPTCHA_SITE_KEY } from 'src/config';
import {
  YUP_CONTENT_SUB_CATEGORY_VALIDATOR,
  YUP_CONTENT_CATEGORY_VALIDATOR,
  YUP_FIRSTNAME_VALIDATOR,
  YUP_RECAPTCHA_VALIDATOR,
} from 'src/config/validators';
import { useFormReset, useUserAgent } from 'src/hooks';

import { CreateButton, EditButton } from 'src/components/lib';
import {
  ContentCategorySelect,
  ContentSubCategorySelect,
} from 'src/components/enums';

const initialState = {
  firstname: undefined,
  contentCategory: null,
  contentSubCategory: null,
  recaptcha: undefined,
  showFirstnameInput: true,
  showContentCategoryInput: false,
  showContentSubCategoryInput: false,
  showSubmitInput: false,
};

const types = {
  SET_FIRSTNAME: 'SET_FIRSTNAME',
  SET_CONTENT_CATEGORY: 'SET_CONTENT_CATEGORY',
  SET_CONTENT_SUB_CATEGORY: 'SET_CONTENT_SUB_CATEGORY',
  SET_RECAPTCHA: 'SET_RECAPTCHA',
  SET_SHOW_FIRSTNAME_INPUT: 'SET_SHOW_FIRSTNAME_INPUT',
  SET_SHOW_CONTENT_CATEGORY_INPUT: 'SET_SHOW_CONTENT_CATEGORY_INPUT',
  SET_SHOW_CONTENT_SUB_CATEGORY_INPUT: 'SET_SHOW_CONTENT_SUB_CATEGORY_INPUT',
  SET_SHOW_SUBMIT_INPUT: 'SET_SHOW_SUBMIT_INPUT',
  CLEAR_FORM_STATE: 'CLEAR_FORM_STATE',
};

const reducer = produce((draft, action) => {
  switch (action.type) {
    case types.SET_FIRSTNAME:
      draft.firstname = action.payload;
      return;
    case types.SET_CONTENT_CATEGORY:
      draft.contentCategory = action.payload;
      return;
    case types.SET_CONTENT_SUB_CATEGORY:
      draft.contentSubCategory = action.payload;
      return;
    case types.SET_RECAPTCHA:
      draft.recaptcha = action.payload;
      return;
    case types.SET_SHOW_FIRSTNAME_INPUT:
      draft.showFirstnameInput = action.payload;
      return;
    case types.SET_SHOW_CONTENT_CATEGORY_INPUT:
      draft.showContentCategoryInput = action.payload;
      return;
    case types.SET_SHOW_CONTENT_SUB_CATEGORY_INPUT:
      draft.showContentSubCategoryInput = action.payload;
      return;
    case types.SET_SHOW_SUBMIT_INPUT:
      draft.showSubmitInput = action.payload;
      return;
    case types.CLEAR_FORM_STATE:
      return initialState;
    default:
      return draft;
  }
});

function ContentPersonalizationInput({ onSubmit, processing, isSuccess }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const userAgent = useUserAgent();
  let clearFormState;

  useEffect(() => {
    if (isSuccess) {
      clearFormState();
    }
  }, [clearFormState, isSuccess]);

  const setFirstname = useCallback((firstname) => {
    dispatch({ type: types.SET_FIRSTNAME, payload: firstname });
  }, []);
  const setContentCategory = useCallback((contentCategory) => {
    dispatch({ type: types.SET_CONTENT_CATEGORY, payload: contentCategory });
  }, []);
  const setContentSubCategory = useCallback((contentSubCategory) => {
    dispatch({
      type: types.SET_CONTENT_SUB_CATEGORY,
      payload: contentSubCategory,
    });
  }, []);
  const setRecaptcha = useCallback((recaptcha) => {
    dispatch({
      type: types.SET_RECAPTCHA,
      payload: recaptcha,
    });
  }, []);
  const setShowFirstnameInput = useCallback((showFirstnameInput) => {
    dispatch({
      type: types.SET_SHOW_FIRSTNAME_INPUT,
      payload: showFirstnameInput,
    });
  }, []);
  const setShowContentCategoryInput = useCallback(
    (showContentCategoryInput) => {
      dispatch({
        type: types.SET_SHOW_CONTENT_CATEGORY_INPUT,
        payload: showContentCategoryInput,
      });
    },
    []
  );
  const setShowContentSubCategoryInput = useCallback(
    (showContentSubCategoryInput) => {
      dispatch({
        type: types.SET_SHOW_CONTENT_SUB_CATEGORY_INPUT,
        payload: showContentSubCategoryInput,
      });
    },
    []
  );
  const setShowSubmitInput = useCallback((showSubmitInput) => {
    dispatch({ type: types.SET_SHOW_SUBMIT_INPUT, payload: showSubmitInput });
  }, []);
  clearFormState = useCallback(() => {
    dispatch({
      type: types.CLEAR_FORM_STATE,
    });
  }, []);

  const handleSubmitFirstname = useCallback(() => {
    setShowFirstnameInput(false);
    setShowContentCategoryInput(true);
  }, [setShowContentCategoryInput, setShowFirstnameInput]);
  const handleSubmitContentCategory = useCallback(() => {
    setShowContentCategoryInput(false);
    if (state.contentCategory?.choicesUrl) {
      setShowContentSubCategoryInput(true);
    } else {
      setShowSubmitInput(true);
    }
  }, [
    setShowContentCategoryInput,
    setShowContentSubCategoryInput,
    setShowSubmitInput,
    state.contentCategory?.choicesUrl,
  ]);
  const handleSubmitContentSubCategory = useCallback(() => {
    setShowContentCategoryInput(false);
    setShowContentSubCategoryInput(false);
    setShowSubmitInput(true);
  }, [
    setShowContentCategoryInput,
    setShowContentSubCategoryInput,
    setShowSubmitInput,
  ]);
  const handleSubmitContentPersonalization = useCallback(() => {
    const newContentPersonalization = {
      firstname: state.firstname,
      contentCategory: state.contentCategory?.name,
      contentSubCategory: state.contentSubCategory?.name ?? null,
      recaptcha: state.recaptcha,
      userAgent,
    };
    onSubmit && onSubmit(newContentPersonalization);
  }, [
    onSubmit,
    state.contentCategory?.name,
    state.contentSubCategory?.name,
    state.firstname,
    state.recaptcha,
    userAgent,
  ]);
  const handleChangeFirstname = useCallback(() => {
    setShowFirstnameInput(true);
    setShowContentCategoryInput(false);
  }, [setShowContentCategoryInput, setShowFirstnameInput]);
  const handleChangeContentCategory = useCallback(() => {
    setShowContentCategoryInput(true);
    if (state.contentCategory?.choicesUrl) {
      setShowContentSubCategoryInput(false);
    } else {
      setShowSubmitInput(false);
    }
  }, [
    setShowContentCategoryInput,
    setShowContentSubCategoryInput,
    setShowSubmitInput,
    state.contentCategory?.choicesUrl,
  ]);
  const handleChangeContentSubCategory = useCallback(() => {
    setShowContentSubCategoryInput(true);
    setShowContentCategoryInput(false);
    setShowSubmitInput(false);
  }, [
    setShowContentCategoryInput,
    setShowContentSubCategoryInput,
    setShowSubmitInput,
  ]);

  return (
    <div className="mt-4">
      {state.showFirstnameInput ? (
        <FirstnameInput
          onSubmit={handleSubmitFirstname}
          processing={processing}
          firstname={state.firstname}
          setFirstname={setFirstname}
        />
      ) : state.showContentCategoryInput ? (
        <ContentCategoryInput
          onSubmit={handleSubmitContentCategory}
          processing={processing}
          contentCategory={state.contentCategory}
          setContentCategory={setContentCategory}
          onChange={handleChangeFirstname}
        />
      ) : state.showContentSubCategoryInput ? (
        <ContentSubCategoryInput
          onSubmit={handleSubmitContentSubCategory}
          processing={processing}
          contentCategory={state.contentCategory}
          contentSubCategory={state.contentSubCategory}
          setContentSubCategory={setContentSubCategory}
          onChange={handleChangeContentCategory}
        />
      ) : state.showSubmitInput ? (
        <SubmitInput
          onSubmit={handleSubmitContentPersonalization}
          processing={processing}
          recaptcha={state.recaptcha}
          firstname={state.firstname}
          contentCategory={state.contentCategory}
          contentSubCategory={state.contentSubCategory}
          setRecaptcha={setRecaptcha}
          onChange={
            state.contentSubCategory
              ? handleChangeContentSubCategory
              : handleChangeContentCategory
          }
        />
      ) : null}
    </div>
  );
}

function FirstnameInput({ onSubmit, processing, firstname, setFirstname }) {
  const firstnameInputSchema = Yup({
    firstname: YUP_FIRSTNAME_VALIDATOR,
  });
  const defaultValues = useMemo(() => {
    return {
      firstname,
    };
  }, [firstname]);

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    watch,
    reset,
  } = useForm({
    defaultValues,
    resolver: yupResolver(firstnameInputSchema),
  });

  const watchedFirstname = watch('firstname');

  useEffect(() => {
    if (watchedFirstname) {
      setFirstname && setFirstname(watchedFirstname);
    }
  }, [setFirstname, watchedFirstname]);

  useFormReset({ values: defaultValues, item: firstname, reset });

  const handleSubmitFirstname = useCallback(
    ({ firstname = null }) => {
      setFirstname && setFirstname(firstname);
      onSubmit && onSubmit(firstname);
      reset({});
    },
    [onSubmit, reset, setFirstname]
  );

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitFirstname)} noValidate>
        <Row>
          <Form.Group className="my-1" as={Col}>
            <Form.Label className="main-form-label">First name</Form.Label>
            <Controller
              render={({ field }) => (
                <Form.Control
                  type="text"
                  placeholder="Your first name"
                  {...field}
                  isInvalid={!!errors.firstname}
                />
              )}
              name="firstname"
              control={control}
            />
            {errors.firstname ? (
              <Form.Control.Feedback type="invalid">
                {errors.firstname.message}
              </Form.Control.Feedback>
            ) : null}
            <Form.Text>Give your first name if any E.g. Michael</Form.Text>
          </Form.Group>
        </Row>

        <div className="mt-2">
          <CreateButton
            type="submit"
            clicked={processing}
            fullWidth
            text={processing ? 'Please wait...' : 'Select content category'}
            isDisabled={!isValid || processing}
            iconPosition="right"
            Icon={<BsArrowRight size={20} />}
            showIcon
          />
        </div>
      </Form>
    </div>
  );
}

function ContentCategoryInput({
  onSubmit,
  processing,
  contentCategory,
  setContentCategory,
  onChange,
}) {
  const contentCategoryInputSchema = Yup({
    contentCategory: YUP_CONTENT_CATEGORY_VALIDATOR,
  });
  const defaultValues = useMemo(() => {
    return {
      contentCategory,
    };
  }, [contentCategory]);

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    watch,
    reset,
  } = useForm({
    defaultValues,
    resolver: yupResolver(contentCategoryInputSchema),
  });

  const watchedContentCategory = watch('contentCategory');

  useEffect(() => {
    if (watchedContentCategory) {
      setContentCategory && setContentCategory(watchedContentCategory);
    }
  }, [setContentCategory, watchedContentCategory]);

  useFormReset({ values: defaultValues, item: contentCategory, reset });

  const handleSubmitContentCategory = useCallback(
    ({ contentCategory = null }) => {
      setContentCategory && setContentCategory(contentCategory);
      onSubmit && onSubmit(contentCategory);
      reset({});
    },
    [onSubmit, reset, setContentCategory]
  );

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitContentCategory)} noValidate>
        <Row>
          <Form.Group as={Col}>
            <Controller
              name="contentCategory"
              control={control}
              render={({ field }) => (
                <ContentCategorySelect
                  label="Content category"
                  labelClass="main-form-label"
                  placeholder="Select your content category"
                  helpText="Choose your content category. E.g. Music"
                  errors={errors}
                  isClearable
                  {...field}
                  isFiltered
                />
              )}
            />
          </Form.Group>
        </Row>

        <Stack className="mt-2" gap={2}>
          <CreateButton
            type="submit"
            clicked={processing}
            fullWidth
            text={
              processing
                ? 'Please wait...'
                : contentCategory && contentCategory.choicesUrl
                ? 'Select content sub category'
                : 'Personalize your radio'
            }
            isDisabled={!isValid || processing}
            showIcon
            Icon={<BsArrowRight size={20} />}
            iconPosition="right"
          />
          <EditButton
            onClick={onChange}
            clicked={processing}
            fullWidth
            text={`Change first name`}
            isDisabled={processing}
            variant="light"
            showIcon
            Icon={<BsArrowLeft size={20} />}
            iconPosition="left"
          />
        </Stack>
      </Form>
    </div>
  );
}

function ContentSubCategoryInput({
  onSubmit,
  processing,
  contentCategory,
  contentSubCategory,
  setContentSubCategory,
  onChange,
}) {
  const contentSubCategoryInputSchema = Yup({
    contentSubCategory: YUP_CONTENT_SUB_CATEGORY_VALIDATOR,
  });
  const defaultValues = useMemo(() => {
    return {
      contentSubCategory,
    };
  }, [contentSubCategory]);

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    watch,
    reset,
  } = useForm({
    defaultValues,
    resolver: yupResolver(contentSubCategoryInputSchema),
  });

  const watchedContentSubCategory = watch('contentSubCategory');

  useEffect(() => {
    if (watchedContentSubCategory) {
      setContentSubCategory && setContentSubCategory(watchedContentSubCategory);
    }
  }, [setContentSubCategory, watchedContentSubCategory]);

  useFormReset({ values: defaultValues, item: contentSubCategory, reset });

  const handleSubmitContentSubCategory = useCallback(
    ({ contentSubCategory = null }) => {
      setContentSubCategory && setContentSubCategory(contentSubCategory);
      onSubmit && onSubmit(contentSubCategory);
      reset({});
    },
    [onSubmit, reset, setContentSubCategory]
  );

  return (
    <div>
      <Form onSubmit={handleSubmit(handleSubmitContentSubCategory)} noValidate>
        <Row xs={{ cols: 1 }}>
          <Form.Group as={Col}>
            <Controller
              name="contentSubCategory"
              control={control}
              render={({ field }) => (
                <ContentSubCategorySelect
                  label="Content sub category"
                  labelClass="main-form-label"
                  placeholder="Select your content subcategory"
                  helpText="Choose your content sub category. E.g. Pop"
                  errors={errors}
                  contentCategory={contentCategory}
                  isClearable
                  {...field}
                  isFiltered={
                    contentCategory && contentCategory.name === 'Music'
                  }
                />
              )}
            />
          </Form.Group>
        </Row>

        <Stack className="mt-2" gap={2}>
          <CreateButton
            type="submit"
            clicked={processing}
            fullWidth
            text={processing ? 'Please wait...' : 'Personalize your radio'}
            isDisabled={!isValid || processing}
            showIcon
            iconPosition="right"
            Icon={<BsArrowRight size={20} />}
          />
          <EditButton
            onClick={onChange}
            clicked={processing}
            fullWidth
            text={`Change content category`}
            isDisabled={processing}
            variant="light"
            showIcon
            iconPosition="left"
            Icon={<BsArrowLeft size={20} />}
          />
        </Stack>
      </Form>
    </div>
  );
}

function SubmitInput({
  onSubmit,
  processing,
  recaptcha,
  firstname,
  contentCategory,
  contentSubCategory,
  setRecaptcha,
  onChange,
}) {
  const submitInputSchema = Yup({
    recaptcha: YUP_RECAPTCHA_VALIDATOR,
  });
  const defaultValues = useMemo(() => {
    return {
      recaptcha,
    };
  }, [recaptcha]);

  const {
    handleSubmit,
    formState: { errors, isValid },
    control,
    watch,
    reset,
  } = useForm({
    defaultValues,
    resolver: yupResolver(submitInputSchema),
  });

  const watchedRecaptcha = watch('recaptcha');

  useEffect(() => {
    if (watchedRecaptcha) {
      setRecaptcha && setRecaptcha(watchedRecaptcha);
    }
  }, [setRecaptcha, watchedRecaptcha]);

  useFormReset({
    values: defaultValues,
    item: contentCategory || contentSubCategory,
    reset,
  });

  const handleSubmitRecaptcha = useCallback(
    ({ recaptcha = null }) => {
      setRecaptcha && setRecaptcha(recaptcha);
      onSubmit && onSubmit(recaptcha);
      reset({});
    },
    [onSubmit, reset, setRecaptcha]
  );

  return (
    <div>
      <Row xs={{ cols: 1 }} as="div">
        <Col>
          <div className="w-100 justify-start fw-bold text-primary site-fs-5">
            {firstname}&apos;s stream
          </div>
        </Col>
        <Col>
          <div className="d-flex justify-content-between">
            <div className="item-subtitle site-fs-5">
              {contentCategory ? contentCategory.name : 'N/A'}
            </div>
            <div className="item-subtitl site-fs-5">
              {contentSubCategory ? contentSubCategory.name : 'N/A'}
            </div>
          </div>
        </Col>
      </Row>
      <Form onSubmit={handleSubmit(handleSubmitRecaptcha)} noValidate>
        <Row>
          {recaptcha !== undefined ? (
            <Col
              as="div"
              className="bg-success text-white rounded m-3 p-3 fw-bold text-center site-fs-5"
            >
              <BsCheckCircle size={30} /> Authenticated
            </Col>
          ) : (
            <Form.Group className="mt-3" as={Col}>
              <Controller
                render={({ field }) => (
                  <ReCAPTCHA sitekey={RECAPTCHA_SITE_KEY} {...field} />
                )}
                name="recaptcha"
                control={control}
              />
              {errors.recaptcha ? (
                <Form.Control.Feedback type="invalid">
                  {errors.recaptcha.message}
                </Form.Control.Feedback>
              ) : null}
            </Form.Group>
          )}
        </Row>

        <Stack className="mt-2" gap={2}>
          <CreateButton
            type="submit"
            clicked={processing}
            fullWidth
            text={processing ? 'Please wait...' : 'Listen to your radio'}
            isDisabled={!isValid || processing}
          />
          <EditButton
            onClick={onChange}
            clicked={processing}
            fullWidth
            text={`${
              contentSubCategory
                ? 'Change content sub category'
                : 'Change content category'
            }`}
            isDisabled={processing || recaptcha === undefined}
            variant="light"
            showIcon
            iconPosition="left"
            Icon={<BsArrowLeft size={20} />}
          />
        </Stack>
      </Form>
    </div>
  );
}

ContentPersonalizationInput.propTypes = {
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
  isSuccess: PropTypes.bool,
};
ContentPersonalizationInput.defaultProps = {
  onSubmit: undefined,
  processing: false,
  isSuccess: false,
};
FirstnameInput.propTypes = {
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
  firstname: PropTypes.string,
  setFirstname: PropTypes.func,
  onChange: PropTypes.func,
};
FirstnameInput.defaultProps = {
  onSubmit: undefined,
  processing: false,
  firstname: undefined,
  setFirstname: undefined,
  onChange: undefined,
};
ContentCategoryInput.propTypes = {
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
  contentCategory: PropTypes.object,
  setContentCategory: PropTypes.func,
  onChange: PropTypes.func,
};
ContentCategoryInput.defaultProps = {
  onSubmit: undefined,
  processing: false,
  contentCategory: null,
  setContentCategory: undefined,
  onChange: undefined,
};
ContentSubCategoryInput.propTypes = {
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
  contentCategory: PropTypes.object,
  contentSubCategory: PropTypes.object,
  setContentSubCategory: PropTypes.func,
  onChange: PropTypes.func,
};
ContentSubCategoryInput.defaultProps = {
  onSubmit: undefined,
  processing: false,
  contentCategory: null,
  contentSubCategory: null,
  setContentSubCategory: undefined,
  onChange: undefined,
};
SubmitInput.propTypes = {
  onSubmit: PropTypes.func,
  processing: PropTypes.bool,
  recaptcha: PropTypes.string,
  firstname: PropTypes.string,
  contentCategory: PropTypes.object,
  contentSubCategory: PropTypes.object,
  setRecaptcha: PropTypes.func,
  onChange: PropTypes.func,
};
SubmitInput.defaultProps = {
  onSubmit: undefined,
  processing: false,
  recaptcha: undefined,
  firstname: undefined,
  contentCategory: null,
  contentSubCategory: null,
  setRecaptcha: undefined,
  onChange: undefined,
};

export default ContentPersonalizationInput;
