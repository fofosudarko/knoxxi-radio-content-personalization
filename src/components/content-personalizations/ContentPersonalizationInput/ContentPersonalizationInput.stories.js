import ContentPersonalizationInput from './ContentPersonalizationInput';

const Story = {
  title: 'Pages/SiteHomeSection/ContentPersonalizationInput',
  component: ContentPersonalizationInput,
};

const Template = (args) => {
  return <ContentPersonalizationInput {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
