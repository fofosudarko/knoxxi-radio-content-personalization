// ContentPersonalizationShowPage.stories.js

import ContentPersonalizationShowPage from './ContentPersonalizationShowPage';

const Story = {
  title: 'Pages/ContentPersonalizationShow',
  component: ContentPersonalizationShowPage,
};

const Template = (args) => {
  return <ContentPersonalizationShowPage {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
