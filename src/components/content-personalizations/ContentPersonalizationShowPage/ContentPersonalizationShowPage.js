import PropTypes from 'prop-types';

import ContentPersonalizationShow from '../ContentPersonalizationShow/ContentPersonalizationShow';

function ContentPersonalizationShowPage({ contentPersonalization, homeRoute }) {
  return (
    <ContentPersonalizationShow
      contentPersonalization={contentPersonalization}
      homeRoute={homeRoute}
    />
  );
}

ContentPersonalizationShowPage.propTypes = {
  contentPersonalization: PropTypes.object,
  homeRoute: PropTypes.string,
};
ContentPersonalizationShowPage.defaultProps = {
  contentPersonalization: null,
  homeRoute: undefined,
};

export default ContentPersonalizationShowPage;
