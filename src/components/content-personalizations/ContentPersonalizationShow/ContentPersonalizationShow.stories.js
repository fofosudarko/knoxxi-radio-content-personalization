import ContentPersonalizationShow from './ContentPersonalizationShow';

const Story = {
  title: 'ContentPersonalizations/Show',
  component: ContentPersonalizationShow,
};

const Template = (args) => {
  return <ContentPersonalizationShow {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
