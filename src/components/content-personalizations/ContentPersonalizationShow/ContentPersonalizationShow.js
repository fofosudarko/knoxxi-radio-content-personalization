import PropTypes from 'prop-types';
import Link from 'next/link';

import { Stack } from 'react-bootstrap';

function ContentPersonalizationShow({ contentPersonalization, homeRoute }) {
  const { contentStreamUrl } = contentPersonalization ?? {};

  return (
    <Stack direction="vertical" gap={2}>
      <div>
        <Link
          href={contentStreamUrl}
          className="text-decoration-none text-primary fw-bold site-fs-4"
        >
          <div>Play stream</div>
        </Link>
      </div>
      <div>
        <Link
          href={homeRoute}
          className="text-decoration-none text-secondary fw-bold site-fs-4"
        >
          Back to home
        </Link>
      </div>
    </Stack>
  );
}

ContentPersonalizationShow.propTypes = {
  contentPersonalization: PropTypes.object,
  homeRoute: PropTypes.string,
};
ContentPersonalizationShow.defaultProps = {
  contentPersonalization: null,
  homeRoute: undefined,
};

export default ContentPersonalizationShow;
