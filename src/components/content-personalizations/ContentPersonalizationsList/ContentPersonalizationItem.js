import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import { humanizeDate } from 'src/utils';

export function useContentPersonalizationDetails(contentPersonalization) {
  const {
    id,
    createdOn,
    firstname,
    contentCategory,
    contentSubCategory,
    userAgent,
    carrier = null,
    contentStreamUrl,
  } = contentPersonalization ?? {};

  return {
    id,
    createdOn,
    firstname,
    contentCategory,
    contentSubCategory,
    userAgent,
    carrier,
    contentStreamUrl,
  };
}

export function ContentPersonalizationGridItem({ contentPersonalization }) {
  const {
    createdOn,
    firstname,
    contentCategory,
    contentSubCategory,
    contentStreamUrl,
  } = useContentPersonalizationDetails(contentPersonalization);

  return (
    <div className="my-1 p-3 border border-light rounded shadow-sm">
      <a
        style={{ cursor: 'pointer' }}
        href={contentStreamUrl}
        target="_blank"
        rel="noreferrer"
        className="text-decoration-none"
      >
        <Row xs={{ cols: 1 }} as="div">
          <Col>
            <div className="w-100 justify-start fw-bold text-primary">
              {firstname}&apos;s stream
            </div>
          </Col>
          <Col>
            <div className="d-flex justify-content-between">
              <div className="item-subtitle">
                {contentCategory ? contentCategory : 'N/A'}
              </div>
              <div className="item-subtitle">
                {contentSubCategory ? contentSubCategory : 'N/A'}
              </div>
            </div>
          </Col>
          <Col>
            <div className="justify-end item-subtitle">
              {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
            </div>
          </Col>
        </Row>
      </a>
    </div>
  );
}

export function ContentPersonalizationTableItem({ contentPersonalization }) {
  const {
    createdOn,
    firstname,
    contentCategory,
    contentSubCategory,
    userAgent,
  } = useContentPersonalizationDetails(contentPersonalization);

  return (
    <tr className="list-table-row-border">
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {createdOn ? humanizeDate(new Date(createdOn)) : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {firstname ? firstname : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {contentCategory ? contentCategory : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {contentSubCategory ? contentSubCategory : 'N/A'}
        </div>
      </td>
      <td>
        <div style={{ cursor: 'pointer' }} className="item-body">
          {userAgent ? userAgent : 'N/A'}
        </div>
      </td>
    </tr>
  );
}

ContentPersonalizationGridItem.propTypes = {
  contentPersonalization: PropTypes.object,
};
ContentPersonalizationGridItem.defaultProps = {
  contentPersonalization: null,
};
ContentPersonalizationTableItem.propTypes = {
  contentPersonalization: PropTypes.object,
};
ContentPersonalizationTableItem.defaultProps = {
  contentPersonalization: null,
};
