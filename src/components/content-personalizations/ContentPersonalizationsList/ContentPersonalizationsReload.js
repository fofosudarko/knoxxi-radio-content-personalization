import { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useResetContentPersonalizations } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function ContentPersonalizationsReload({ useTooltip, query }) {
  const _handleResetContentPersonalizations = useResetContentPersonalizations({
    ...query,
  });

  const handleResetContentPersonalizations = useCallback(() => {
    _handleResetContentPersonalizations();
  }, [_handleResetContentPersonalizations]);

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetContentPersonalizations}
      text="Reload content personalizations"
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

ContentPersonalizationsReload.propTypes = {
  useTooltip: PropTypes.bool,
  query: PropTypes.object,
};
ContentPersonalizationsReload.defaultProps = {
  useTooltip: false,
  query: null,
};

export default ContentPersonalizationsReload;
