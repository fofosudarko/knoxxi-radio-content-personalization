import { useAuthStore } from 'src/stores/auth';
import { ContentPersonalizationsListView } from './ContentPersonalizationsList';

const Story = {
  title: 'ContentPersonalizations/ListView',
  component: ContentPersonalizationsListView,
};

const Template = (args) => {
  const appUser = useAuthStore((state) => state.appUser);
  return <ContentPersonalizationsListView {...args} appUser={appUser} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
