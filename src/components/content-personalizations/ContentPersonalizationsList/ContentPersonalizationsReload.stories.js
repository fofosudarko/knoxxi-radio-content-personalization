import ContentPersonalizationsReload from './ContentPersonalizationsReload';

const Story = {
  title: 'ContentPersonalizations/Reload',
  component: ContentPersonalizationsReload,
};

const Template = (args) => {
  return <ContentPersonalizationsReload {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
