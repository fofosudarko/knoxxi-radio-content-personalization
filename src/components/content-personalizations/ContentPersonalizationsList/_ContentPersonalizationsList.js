import PropTypes from 'prop-types';
import { Table, Row, Col } from 'react-bootstrap';
import { BsFolder2Open } from 'react-icons/bs';

//import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions } from 'src/hooks';

import {
  NewHOC,
  //LoadMore
} from 'src/components/lib';
import {
  ContentPersonalizationTableItem,
  ContentPersonalizationGridItem,
} from './ContentPersonalizationItem';

export function ContentPersonalizationsList({
  contentPersonalizations,
  loadingText,
  appUser,
  //onLoadMore,
  ContentPersonalizationsListEmpty,
  //isDisabled,
}) {
  if (!contentPersonalizations) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(ContentPersonalizationsListEmpty);

  return contentPersonalizations.length ? (
    <div>
      <ContentPersonalizationsListGrid
        contentPersonalizations={contentPersonalizations}
        appUser={appUser}
      />

      {/*contentPersonalizations.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more content personalizations"
          isDisabled={isDisabled}
        />
      ) : null*/}
    </div>
  ) : (
    <ListEmpty appUser={appUser} />
  );
}

export function ContentPersonalizationsListGrid({
  contentPersonalizations,
  appUser,
}) {
  return (
    <>
      <Row
        xs={{ cols: 1 }}
        //md={{ cols: 2 }}
        //lg={{ cols: 3 }}
        //xl={{ cols: 4 }}
        className="gx-2"
      >
        {contentPersonalizations.map((item) => (
          <Col key={item.id}>
            <ContentPersonalizationGridItem
              contentPersonalization={item}
              appUser={appUser}
            />
          </Col>
        ))}
      </Row>
    </>
  );
}

export function ContentPersonalizationsListTable({
  contentPersonalizations,
  appUser,
}) {
  return (
    <>
      <Table className="list-table" borderless hover responsive>
        <thead className="list-table-head">
          <tr>
            <th>Created</th>
            <th>First name</th>
            <th>Content category</th>
            <th>Content sub category</th>
            <th>User agent</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {contentPersonalizations.map((item, index) => (
            <ContentPersonalizationTableItem
              contentPersonalization={item}
              appUser={appUser}
              key={index}
            />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export function ContentPersonalizationsListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <BsFolder2Open size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there are no content personalizations yet.
          </div>
        </div>
      </div>
    </div>
  );
}

ContentPersonalizationsList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  contentPersonalizations: PropTypes.array,
  ContentPersonalizationsListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
};
ContentPersonalizationsList.defaultProps = {
  appUser: null,
  loadingText: 'Loading content personalizations...',
  onLoadMore: undefined,
  contentPersonalizations: null,
  ContentPersonalizationsListEmpty: null,
  isDisabled: false,
};
ContentPersonalizationsListGrid.propTypes = {
  appUser: PropTypes.object,
  contentPersonalizations: PropTypes.array,
};
ContentPersonalizationsListGrid.defaultProps = {
  appUser: null,
  contentPersonalizations: null,
};
ContentPersonalizationsListTable.propTypes = {
  appUser: PropTypes.object,
  contentPersonalizations: PropTypes.array,
};
ContentPersonalizationsListTable.defaultProps = {
  appUser: null,
  contentPersonalizations: null,
};
ContentPersonalizationsListEmpty.propTypes = {
  appUser: PropTypes.object,
};
ContentPersonalizationsListEmpty.defaultProps = {
  appUser: null,
};
