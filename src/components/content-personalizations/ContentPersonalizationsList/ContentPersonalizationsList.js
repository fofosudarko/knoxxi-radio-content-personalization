import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import { useNotificationHandler } from 'src/hooks';
import { useListContentPersonalizations } from 'src/hooks/api';

import ContentPersonalizationsReload from './ContentPersonalizationsReload';
import {
  ContentPersonalizationsList,
  ContentPersonalizationsListEmpty,
} from './_ContentPersonalizationsList';

function ContentPersonalizationsListContainer({ appUser, query }) {
  const {
    contentPersonalizations,
    error: contentPersonalizationsError,
    setError,
    setPage,
    page,
    endPaging,
  } = useListContentPersonalizations({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (contentPersonalizationsError) {
      handleNotification(contentPersonalizationsError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, contentPersonalizationsError, setError]);

  const handleLoadMoreContentPersonalizations = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <ContentPersonalizationsList
      contentPersonalizations={contentPersonalizations}
      onLoadMore={handleLoadMoreContentPersonalizations}
      loadingText="Loading content personalizations..."
      appUser={appUser}
      ContentPersonalizationsListEmpty={ContentPersonalizationsListEmpty}
      isDisabled={endPaging}
    />
  );
}

export function ContentPersonalizationsListView({ appUser, query }) {
  return (
    <div className="d-block">
      <div className="d-flex justify-content-end">
        <ContentPersonalizationsReload useTooltip query={query} />
      </div>
      <div className="my-2">
        <ContentPersonalizationsListContainer appUser={appUser} query={query} />
      </div>
    </div>
  );
}

ContentPersonalizationsListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
ContentPersonalizationsListContainer.defaultProps = {
  appUser: null,
  query: null,
};
ContentPersonalizationsListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
ContentPersonalizationsListView.defaultProps = {
  appUser: null,
  query: null,
};

export default ContentPersonalizationsList;
