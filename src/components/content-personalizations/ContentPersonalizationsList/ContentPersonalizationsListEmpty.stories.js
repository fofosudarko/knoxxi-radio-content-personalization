import { ContentPersonalizationsListEmpty } from './ContentPersonalizationsList';

const Story = {
  title: 'ContentPersonalizations/ListEmpty',
  component: ContentPersonalizationsListEmpty,
};

const Template = (args) => {
  return <ContentPersonalizationsListEmpty {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
