import ContentPersonalizationsList from './ContentPersonalizationsList';

const Story = {
  title: 'ContentPersonalizations/List',
  component: ContentPersonalizationsList,
};

const Template = (args) => {
  return <ContentPersonalizationsList {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
