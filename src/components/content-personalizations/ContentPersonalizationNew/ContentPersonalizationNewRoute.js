import PropTypes from 'prop-types';

import { useRoutes } from 'src/hooks';
import { CreateButton } from 'src/components/lib';

export default function ContentPersonalizationNewRoute({
  text,
  carrierId,
  carrierSecurityCode,
}) {
  const { handleContentPersonalizationNewRoute } =
    useRoutes().useContentPersonalizationNewRoute();

  return (
    <CreateButton
      variant="primary"
      textColor="white"
      onClick={() =>
        handleContentPersonalizationNewRoute({
          query: { carrierId, carrierSecurityCode },
        })
      }
      text={text}
    />
  );
}

ContentPersonalizationNewRoute.propTypes = {
  text: PropTypes.string,
  carrierId: PropTypes.string,
  carrierSecurityCode: PropTypes.string,
};
ContentPersonalizationNewRoute.defaultProps = {
  text: 'New content personalization',
  carrierId: undefined,
  carrierSecurityCode: undefined,
};
