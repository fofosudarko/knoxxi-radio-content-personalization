import { ContentPersonalizationNewRoute } from './ContentPersonalizationNewRoute';

const Story = {
  title: 'ContentPersonalizations/NewRoute',
  component: ContentPersonalizationNewRoute,
};

const Template = (args) => {
  return <ContentPersonalizationNewRoute {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
