import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import useRoutes from 'src/hooks/routes';

import { useNotificationHandler } from 'src/hooks';
import { useCreateContentPersonalization } from 'src/hooks/api';
import { getNotification } from 'src/utils/notification';

//import { GoBack } from 'src/components/lib';
import ContentPersonalizationInput from '../ContentPersonalizationInput/ContentPersonalizationInput';

function ContentPersonalizationNewPage({ carrierId, carrierSecurityCode }) {
  const { router, useContentPersonalizationShowRoute } = useRoutes();
  const {
    handleCreateContentPersonalization,
    processing,
    error,
    setError,
    contentPersonalization,
    setContentPersonalization,
    contentPersonalizationCreated,
    setContentPersonalizationCreated,
  } = useCreateContentPersonalization();
  const handleNotification = useNotificationHandler();
  let handleApiResult, handlePageReload;

  useEffect(() => {
    if (error) {
      handleNotification(error);
      //handlePageReload();
    }
    return () => {
      setError(null);
    };
  }, [error, handleNotification, handlePageReload, setError]);

  const { handleContentPersonalizationShowRoute } =
    useContentPersonalizationShowRoute(contentPersonalization);

  useEffect(() => {
    if (contentPersonalizationCreated) {
      handleApiResult();
    }
  }, [
    contentPersonalizationCreated,
    handleApiResult,
    setContentPersonalization,
    setContentPersonalizationCreated,
  ]);

  handlePageReload = useCallback(() => {
    setTimeout(() => {
      setContentPersonalization(null);
      setContentPersonalizationCreated(false);
      router.reload();
    }, 3000);
  }, [router, setContentPersonalization, setContentPersonalizationCreated]);

  handleApiResult = useCallback(() => {
    handleNotification(
      getNotification(
        'Content personalization created successfully'
      ).getSuccessNotification()
    );
    setContentPersonalization(null);
    setContentPersonalizationCreated(false);
    setTimeout(() => {
      handleContentPersonalizationShowRoute({
        query: { homeRoute: router.asPath },
      });
    }, 1000);
  }, [
    handleContentPersonalizationShowRoute,
    handleNotification,
    router.asPath,
    setContentPersonalization,
    setContentPersonalizationCreated,
  ]);

  const handleContentPersonalizationCreate = useCallback(
    async (contentPersonalization) => {
      await handleCreateContentPersonalization(
        { ...contentPersonalization, carrierId },
        {
          carrierSecurityCode,
        }
      );
    },
    [carrierId, carrierSecurityCode, handleCreateContentPersonalization]
  );

  return (
    <div className="mt-3">
      {/*<div className="d-flex justify-content-center">
        <GoBack hideText />
      </div>*/}
      <div className="site-fs-5 text-center fw-bold text-secondary">
        <div>Welcome to Knoxxi Radio!</div>
        <div>
          Enjoy our personalized in-car entertainment by providing your name and
          content preference.
        </div>
        <div>Sit back, relax, and let us make your ride enjoyable.</div>
        <div>Thank you for choosing Knoxxi Radio!</div>
      </div>
      <ContentPersonalizationInput
        onSubmit={handleContentPersonalizationCreate}
        processing={processing}
        isSuccess={contentPersonalizationCreated}
      />
    </div>
  );
}

ContentPersonalizationNewPage.propTypes = {
  carrierId: PropTypes.string,
  carrierSecurityCode: PropTypes.string,
};
ContentPersonalizationNewPage.defaultProps = {
  carrierId: undefined,
  carrierSecurityCode: undefined,
};

export default ContentPersonalizationNewPage;
