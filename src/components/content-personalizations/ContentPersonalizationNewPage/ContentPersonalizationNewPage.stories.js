import ContentPersonalization from './ContentPersonalizationNewPage';

const Story = {
  title: 'Pages/ContentPersonalizationNew',
  component: ContentPersonalization,
};

const Template = (args) => {
  return <ContentPersonalization {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
