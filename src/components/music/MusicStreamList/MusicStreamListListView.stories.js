import { MusicStreamListListView } from './MusicStreamListList';

const Story = {
  title: 'MusicStreamList/ListView',
  component: MusicStreamListListView,
};

const Template = (args) => {
  return <MusicStreamListListView {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
