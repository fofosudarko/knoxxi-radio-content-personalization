import PropTypes from 'prop-types';

import { useResetMusicStreamList } from 'src/hooks/api';

import { RefreshButton } from 'src/components/lib';

function MusicStreamListReload({ useTooltip, text, query }) {
  const handleResetMusicStreamList = useResetMusicStreamList({ ...query });

  return (
    <RefreshButton
      variant="transparent"
      onClick={handleResetMusicStreamList}
      text={text}
      useTooltip={useTooltip}
      autoWidth={!useTooltip}
    />
  );
}

MusicStreamListReload.propTypes = {
  useTooltip: PropTypes.bool,
  text: PropTypes.string,
  query: PropTypes.object,
};
MusicStreamListReload.defaultProps = {
  useTooltip: false,
  text: 'Reload music stream list',
  query: null,
};

export default MusicStreamListReload;
