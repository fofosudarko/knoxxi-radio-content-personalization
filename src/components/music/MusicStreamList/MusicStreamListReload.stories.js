import MusicStreamListReload from './MusicStreamListReload';

const Story = {
  title: 'MusicStreamList/Reload',
  component: MusicStreamListReload,
};

const Template = (args) => {
  return <MusicStreamListReload {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
