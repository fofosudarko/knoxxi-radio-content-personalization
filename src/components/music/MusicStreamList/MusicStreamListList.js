import { useEffect, useCallback, useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { BsFolder2Open } from 'react-icons/bs';
import { TbPlaylist } from 'react-icons/tb';
import { Stack, Offcanvas } from 'react-bootstrap';

//import { DEFAULT_PAGE_SIZE } from 'src/config';
import { useDeviceDimensions, useNotificationHandler } from 'src/hooks';
import { useListMusicStreamList } from 'src/hooks/api';

import { NewHOC, ShowButton } from 'src/components/lib';
import { MusicStreamListGridItem } from './MusicStreamListItem';
import MusicStreamListReload from './MusicStreamListReload';
import { MediaStreamListProvider, MediaStreamListContext } from 'src/context';
import { MediaStreamPlayerContainer } from 'src/components/media-stream-list-player';

import { useHasMounted } from 'src/hooks';

function MusicStreamListListContainer({ query }) {
  const {
    musicStreamList,
    error: musicStreamListError,
    setError,
    setPage,
    page,
    endPaging,
    musicStreamAdverts,
    musicStreamScripts,
  } = useListMusicStreamList({
    ...query,
  });
  const handleNotification = useNotificationHandler();

  useEffect(() => {
    if (musicStreamListError) {
      handleNotification(musicStreamListError);
    }

    return () => {
      setError(null);
    };
  }, [handleNotification, musicStreamListError, setError]);

  const handleLoadMoreMusicStreamList = useCallback(() => {
    setPage(page + 1);
  }, [page, setPage]);

  return (
    <Stack direction="vertical" gap={1}>
      <MediaStreamListProvider
        mediaStreamList={musicStreamList}
        mediaStreamAdverts={musicStreamAdverts}
        mediaStreamScripts={musicStreamScripts}
      >
        <MediaStreamPlayerContainer />
        <MusicStreamListListToggle
          musicStreamList={musicStreamList}
          onLoadMore={handleLoadMoreMusicStreamList}
          loadingText="Loading music stream list..."
          MusicStreamListListEmpty={MusicStreamListListEmpty}
          isDisabled={endPaging}
          query={query}
          hideToggle
        />
      </MediaStreamListProvider>
    </Stack>
  );
}

function MusicStreamListList({
  musicStreamList,
  loadingText,
  //onLoadMore,
  MusicStreamListListEmpty,
  //isDisabled,
  query,
}) {
  const {
    currentMediaStreamListItem: currentMusicStreamListItem,
    handleSelectMediaStreamListItem: handleSelectMusicStreamListItem,
    shouldPlayScript,
    shouldPlayAdvert,
  } = useContext(MediaStreamListContext);
  const showStreamPlayerAction = !shouldPlayScript && !shouldPlayAdvert;
  const hasMounted = useHasMounted();

  if (!hasMounted) {
    return <div>{loadingText}</div>;
  }

  if (!musicStreamList) {
    return <div>{loadingText}</div>;
  }

  const ListEmpty = NewHOC(MusicStreamListListEmpty);

  return musicStreamList.length ? (
    <Stack direction="vertical" gap={2}>
      <div className="overflow-auto">
        <MusicStreamListListGrid
          musicStreamList={musicStreamList}
          showStreamPlayerAction={showStreamPlayerAction}
          currentMusicStreamListItem={currentMusicStreamListItem}
          onSelectMusicStreamListItem={handleSelectMusicStreamListItem}
          query={query}
        />
      </div>
      {/*showStreamPlayerAction && musicStreamList.length >= DEFAULT_PAGE_SIZE ? (
        <LoadMore
          onLoadMore={onLoadMore}
          text="Load more music"
          isDisabled={isDisabled}
        />
      ) : null*/}
    </Stack>
  ) : (
    <ListEmpty />
  );
}

export function MusicStreamListListGrid({
  musicStreamList,
  showStreamPlayerAction,
  currentMusicStreamListItem,
  onSelectMusicStreamListItem,
  query,
}) {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div>
      {showStreamPlayerAction ? (
        <div className="d-flex justify-content-end">
          <MusicStreamListReload
            useTooltip={isLargeDevice}
            text="Reload"
            query={query}
          />
        </div>
      ) : null}
      <Stack
        direction="vertical"
        gap={2}
        className={`${!showStreamPlayerAction ? 'pt-4' : ''}`}
      >
        {musicStreamList.map((item) => (
          <MusicStreamListGridItem
            musicStreamListItem={item}
            key={item.id}
            isSelected={item.id === currentMusicStreamListItem?.id}
            onSelect={
              showStreamPlayerAction ? onSelectMusicStreamListItem : undefined
            }
          />
        ))}
      </Stack>
    </div>
  );
}

export function MusicStreamListListView({ query }) {
  return (
    <div>
      <div className="text-primary text-center site-fs-4 fw-bold mb-2">
        Streaming music
      </div>
      <MusicStreamListListContainer query={query} />
    </div>
  );
}

export function MusicStreamListListEmpty() {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <div className="d-flex align-items-center justify-content-center vh-100">
      <div className={`d-block `}>
        <div className="d-block">
          <div className="d-flex justify-content-center my-2">
            <BsFolder2Open size={`${isLargeDevice ? 200 : 150}`} />
          </div>
          <div className="fw-bold fs-5 text-center">
            Oops, there is no music stream list yet.
          </div>
        </div>
      </div>
    </div>
  );
}

function MusicStreamListListToggle({
  musicStreamList,
  loadingText,
  onLoadMore,
  MusicStreamListListEmpty,
  isDisabled,
  query,
  hideToggle,
}) {
  const { isLargeDevice } = useDeviceDimensions();
  const [showMusicStreamList, setShowMusicStreamList] = useState(false);

  const handleClose = () => setShowMusicStreamList(false);
  const handleShow = () => setShowMusicStreamList(true);

  return !hideToggle ? (
    <div>
      <ShowButton
        Icon={<TbPlaylist size={25} />}
        onClick={handleShow}
        useTooltip={isLargeDevice}
        autoWidth
        text="Playlist"
      />
      <Offcanvas
        show={showMusicStreamList}
        onHide={handleClose}
        placement="end"
      >
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Music stream list</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <MusicStreamListList
            musicStreamList={musicStreamList}
            onLoadMore={onLoadMore}
            loadingText={loadingText}
            MusicStreamListListEmpty={MusicStreamListListEmpty}
            isDisabled={isDisabled}
            query={query}
          />
        </Offcanvas.Body>
      </Offcanvas>
    </div>
  ) : null;
}

MusicStreamListListContainer.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
MusicStreamListListContainer.defaultProps = {
  appUser: null,
  query: null,
};
MusicStreamListList.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  musicStreamList: PropTypes.array,
  MusicStreamListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
  query: PropTypes.object,
};
MusicStreamListList.defaultProps = {
  appUser: null,
  loadingText: 'Loading music stream list...',
  onLoadMore: undefined,
  musicStreamList: null,
  MusicStreamListListEmpty: null,
  isDisabled: false,
  query: null,
};
MusicStreamListListGrid.propTypes = {
  appUser: PropTypes.object,
  musicStreamList: PropTypes.array,
  showStreamPlayerAction: PropTypes.bool,
  currentMusicStreamListItem: PropTypes.object,
  onSelectMusicStreamListItem: PropTypes.func,
  query: PropTypes.object,
};
MusicStreamListListGrid.defaultProps = {
  appUser: null,
  musicStreamList: null,
  showStreamPlayerAction: false,
  currentMusicStreamListItem: null,
  onSelectMusicStreamListItem: undefined,
  query: null,
};
MusicStreamListListView.propTypes = {
  appUser: PropTypes.object,
  query: PropTypes.object,
};
MusicStreamListListView.defaultProps = {
  appUser: null,
  query: null,
};
MusicStreamListListEmpty.propTypes = {
  appUser: PropTypes.object,
};
MusicStreamListListEmpty.defaultProps = {
  appUser: null,
};
MusicStreamListListToggle.propTypes = {
  appUser: PropTypes.object,
  loadingText: PropTypes.string,
  onLoadMore: PropTypes.func,
  musicStreamList: PropTypes.array,
  MusicStreamListListEmpty: PropTypes.func,
  isDisabled: PropTypes.bool,
  query: PropTypes.object,
  hideToggle: PropTypes.bool,
};
MusicStreamListListToggle.defaultProps = {
  appUser: null,
  loadingText: 'Loading music stream list...',
  onLoadMore: undefined,
  musicStreamList: null,
  MusicStreamListListEmpty: null,
  isDisabled: false,
  query: null,
  hideToggle: false,
};

export default MusicStreamListList;
