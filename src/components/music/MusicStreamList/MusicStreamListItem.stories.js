import MusicStreamListItem from './MusicStreamListItem';

const Story = {
  title: 'MusicStreamList/Item',
  component: MusicStreamListItem,
};

const Template = (args) => {
  return <MusicStreamListItem {...args} />;
};

export const Main = Template.bind({});
Main.args = {
  birthdayMessage: {
    id: '62e454d8fbf633bb97198f3a',
    createdOn: 1659131096233,
    updatedOn: null,
    number: 200,
    content:
      'Good morning... why dream when the day gives an opportunity of a reality. Magnify your dreams and put in the action. From knoxxi with love, today was meant to be won.',
  },
};

export default Story;
