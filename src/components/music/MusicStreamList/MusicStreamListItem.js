import PropTypes from 'prop-types';
import { Dropdown, Row, Col } from 'react-bootstrap';
import { FaEllipsisV } from 'react-icons/fa';

import { useDeviceDimensions } from 'src/hooks';
import { getMediaStreamListItemDetails } from 'src/utils';

export function useMusicStreamListItemDetails(mediaStreamListItem) {
  return getMediaStreamListItemDetails(mediaStreamListItem);
}

export function MusicStreamListGridItem({
  musicStreamListItem,
  isSelected,
  onSelect,
}) {
  const { name, genre } = useMusicStreamListItemDetails(musicStreamListItem);

  return (
    <Row>
      <Col>
        <div
          className={`site-fs-5 fw-bold ${
            isSelected ? 'text-primary' : 'text-secondary'
          }`}
          onClick={() => {
            onSelect && onSelect(musicStreamListItem);
          }}
          style={{ cursor: 'pointer' }}
        >
          {name}
        </div>
        <div className="d-flex justify-content-between">
          <div
            className={`site-fs-5 fw-light ${
              isSelected ? 'text-primary' : 'text-secondary'
            }`}
          >
            {genre}
          </div>
        </div>
        {/*<td>
        <div className="py-1 w-100 justify-end">
          <MusicStreamListItemActions
            musicStreamListItem={musicStreamListItem}
          />
        </div>
      </td>*/}
      </Col>
    </Row>
  );
}

export function MusicStreamListItemActions() {
  const { isLargeDevice } = useDeviceDimensions();
  return (
    <div className="d-flex justify-content-end justify-content-sm-center">
      <Dropdown
        align={!isLargeDevice ? { sm: 'start' } : { lg: 'end' }}
        className="d-inline mx-2"
      >
        <Dropdown.Toggle
          id="dropdown-autoclose-true"
          variant="white"
          className="d-flex justify-content-center"
        >
          <FaEllipsisV size={15} />
        </Dropdown.Toggle>

        <Dropdown.Menu className="shadow-sm">
          <Dropdown.Item></Dropdown.Item>
          <Dropdown.Item></Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

MusicStreamListGridItem.propTypes = {
  musicStreamListItem: PropTypes.object,
  isSelected: PropTypes.bool,
  onSelect: PropTypes.func,
};
MusicStreamListGridItem.defaultProps = {
  musicStreamListItem: null,
  isSelected: false,
  onSelect: undefined,
};
MusicStreamListItemActions.propTypes = {
  musicStreamListItem: PropTypes.object,
};
MusicStreamListItemActions.defaultProps = {
  musicStreamListItem: null,
};
