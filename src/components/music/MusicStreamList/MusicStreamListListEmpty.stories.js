import { MusicStreamListListEmpty } from './MusicStreamListList';

const Story = {
  title: 'MusicStreamList/ListEmpty',
  component: MusicStreamListListEmpty,
};

const Template = (args) => {
  return <MusicStreamListListEmpty {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
