import MusicStreamListList from './MusicStreamListList';

const Story = {
  title: 'MusicStreamList/List',
  component: MusicStreamListList,
};

const Template = (args) => {
  return <MusicStreamListList {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
