import { useEffect } from 'react';
import PropTypes from 'prop-types';

import { useResetMusicStreamList } from 'src/hooks/api';
import { useMediaStreamListStore } from 'src/stores/media-stream-list';

import { MusicStreamListListView } from '../MusicStreamList/MusicStreamListList';

function MusicStreamListPage({ streamedById }) {
  const query = { streamedById };
  const handleResetMusicStreamList = useResetMusicStreamList({ streamedById });
  const { clearMediaStreamListState } = useMediaStreamListStore(
    (state) => state
  );

  useEffect(() => {
    handleResetMusicStreamList();
    clearMediaStreamListState();
  }, [clearMediaStreamListState, handleResetMusicStreamList]);

  return (
    <div className="overflow-auto">
      <MusicStreamListListView query={query} />
    </div>
  );
}

MusicStreamListPage.propTypes = {
  streamedById: PropTypes.string,
};
MusicStreamListPage.defaultProps = {
  streamedById: undefined,
};

export default MusicStreamListPage;
