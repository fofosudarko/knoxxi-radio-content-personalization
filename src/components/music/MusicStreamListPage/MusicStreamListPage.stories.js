import MusicStreamListPage from './MusicStreamListPage';

const Story = {
  title: 'Pages/MusicStreamList',
  component: MusicStreamListPage,
};

const Template = (args) => {
  return <MusicStreamListPage {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
