import PropTypes from 'prop-types';

import { SignOut } from 'src/components/lib';
import { ContentPersonalizationsListView } from 'src/components/content-personalizations/ContentPersonalizationsList/ContentPersonalizationsList';

function HomePage({ appUser }) {
  const query = { carrierDriverInfoId: appUser.account.customerId };

  return (
    <div>
      <SignOut className="d-flex align-items-end" />
      <ContentPersonalizationsListView query={query} appUser={appUser} />
    </div>
  );
}

HomePage.propTypes = {
  appUser: PropTypes.object,
};
HomePage.defaultProps = {
  appUser: null,
};

export default HomePage;
