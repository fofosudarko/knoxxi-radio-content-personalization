import HomePage from './HomePage';

const Story = {
  title: 'Pages/Home',
  component: HomePage,
};

const Template = (args) => {
  return <HomePage {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
