import { useMemo } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import { VscFileMedia } from 'react-icons/vsc';
import { TbScript } from 'react-icons/tb';
import {
  MdOutlineDeliveryDining,
  MdPersonalVideo,
  MdViewStream,
  MdOutlineBusinessCenter,
} from 'react-icons/md';

import { useRoutes } from 'src/hooks';

import { MenuCard } from 'src/components/lib';

function HomeMenu({ appUser }) {
  const homeItems = [
    {
      Icon: <MdOutlineBusinessCenter size={200} className="text-primary" />,
      label: 'Brands',
    },
    {
      Icon: <VscFileMedia size={200} className="text-primary" />,
      label: 'Contents',
    },
    {
      Icon: <TbScript size={200} className="text-primary" />,
      label: 'Scripts',
    },
    {
      Icon: <MdOutlineDeliveryDining size={200} className="text-primary" />,
      label: 'Carriers',
    },
    {
      Icon: <MdPersonalVideo size={200} className="text-primary" />,
      label: 'Content personalizations',
    },
    {
      Icon: <MdViewStream size={200} className="text-primary" />,
      label: 'Streams',
    },
  ];

  return (
    <div className="mt-2">
      <Row
        xs={{ cols: 1 }}
        md={{ cols: 2 }}
        lg={{ cols: 3 }}
        className="gx-3 gy-3"
      >
        {homeItems.map((item, index) => (
          <Col key={index}>
            <HomeMenuItem
              Icon={item.Icon}
              label={item.label}
              appUser={appUser}
            />
          </Col>
        ))}
      </Row>
    </div>
  );
}

export function HomeMenuItem({ label, Icon }) {
  const {
    useContentsRoute,
    useScriptsRoute,
    useCarriersRoute,
    useContentPersonalizationsRoute,
    useBrandsRoute,
    useStreamsRoute,
  } = useRoutes();
  const { handleContentsRoute } = useContentsRoute();
  const { handleScriptsRoute } = useScriptsRoute();
  const { handleCarriersRoute } = useCarriersRoute();
  const { handleContentPersonalizationsRoute } =
    useContentPersonalizationsRoute();
  const { handleBrandsRoute } = useBrandsRoute();
  const { handleStreamsRoute } = useStreamsRoute();

  const handleApiResult = useMemo(() => {
    let handler;
    switch (label) {
      case 'Contents':
        handler = handleContentsRoute;
        break;
      case 'Scripts':
        handler = handleScriptsRoute;
        break;
      case 'Carriers':
        handler = handleCarriersRoute;
        break;
      case 'Content personalizations':
        handler = handleContentPersonalizationsRoute;
        break;
      case 'Brands':
        handler = handleBrandsRoute;
        break;
      case 'Streams':
        handler = handleStreamsRoute;
        break;
      default:
        handler = undefined;
    }
    return handler;
  }, [
    handleBrandsRoute,
    handleCarriersRoute,
    handleContentPersonalizationsRoute,
    handleContentsRoute,
    handleScriptsRoute,
    handleStreamsRoute,
    label,
  ]);

  return (
    <div style={{ cursor: 'pointer' }}>
      <MenuCard onClick={handleApiResult} label={label} Icon={Icon} />
    </div>
  );
}

HomeMenu.propTypes = {
  appUser: PropTypes.object,
};
HomeMenu.defaultProps = {
  appUser: null,
};
HomeMenuItem.propTypes = {
  appUser: PropTypes.object,
  label: PropTypes.string,
  Icon: PropTypes.node,
};
HomeMenuItem.defaultProps = {
  appUser: null,
  label: 'Exercise',
  Icon: 'running',
};

export default HomeMenu;
