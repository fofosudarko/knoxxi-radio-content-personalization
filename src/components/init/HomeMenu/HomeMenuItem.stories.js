import { HomeMenuItem } from './HomeMenu';

const Story = {
  title: 'Home/MenuItem',
  component: HomeMenuItem,
};

const Template = (args) => {
  return <HomeMenuItem {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
