import HomeMenu from './HomeMenu';

const Story = {
  title: 'Home/Menu',
  component: HomeMenu,
};

const Template = (args) => {
  return <HomeMenu {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
