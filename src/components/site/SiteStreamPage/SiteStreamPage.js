import PropTypes from 'prop-types';

import { MusicStreamListPage } from 'src/components/music';
import { MotivationalMessageStreamListPage } from 'src/components/motivational-messages';

function SiteStreamPage({ contentCategory, streamedById }) {
  return (
    <div>
      {contentCategory === 'Music' ? (
        <MusicStreamListPage streamedById={streamedById} />
      ) : contentCategory === 'Motivational messages' ? (
        <MotivationalMessageStreamListPage streamedById={streamedById} />
      ) : null}
    </div>
  );
}

SiteStreamPage.propTypes = {
  contentCategory: PropTypes.string,
  streamedById: PropTypes.string,
};
SiteStreamPage.defaultProps = {
  contentCategory: undefined,
  streamedById: undefined,
};

export default SiteStreamPage;
