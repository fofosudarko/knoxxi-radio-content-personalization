import SitePage from './SiteStreamPage';

const Story = {
  title: 'Pages/SiteStream',
  component: SitePage,
};

const Template = (args) => {
  return <SitePage {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
