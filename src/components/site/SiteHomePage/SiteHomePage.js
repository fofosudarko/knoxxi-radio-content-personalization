import PropTypes from 'prop-types';

import WelcomeSection from './WelcomeSection';

function SiteHomePage({ carrierId, carrierSecurityCode }) {
  return (
    <WelcomeSection
      carrierId={carrierId}
      carrierSecurityCode={carrierSecurityCode}
    />
  );
}

SiteHomePage.propTypes = {
  carrierId: PropTypes.string,
  carrierSecurityCode: PropTypes.string,
};
SiteHomePage.defaultProps = {
  carrierId: undefined,
  carrierSecurityCode: undefined,
};

export default SiteHomePage;
