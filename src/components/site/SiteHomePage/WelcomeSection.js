import PropTypes from 'prop-types';
import { Stack } from 'react-bootstrap';

import { ContentPersonalizationNewRoute } from 'src/components/content-personalizations';
import { CarrierCampaignsListRoute } from 'src/components/carrier-campaigns';

function WelcomeSection({ carrierId, carrierSecurityCode }) {
  return (
    <div>
      <div className="site-fs-5 text-center fw-bold text-secondary">
        <div>Welcome to Knoxxi Radio!</div>
      </div>
      <Stack direction="vertical" gap={2} className="mt-4">
        <ContentPersonalizationNewRoute
          text="Personalize your radio"
          carrierId={carrierId}
          carrierSecurityCode={carrierSecurityCode}
        />
        <CarrierCampaignsListRoute
          text="Follow a campaign"
          carrierId={carrierId}
        />
      </Stack>
    </div>
  );
}

WelcomeSection.propTypes = {
  carrierId: PropTypes.string,
  carrierSecurityCode: PropTypes.string,
};
WelcomeSection.defaultProps = {
  carrierId: undefined,
  carrierSecurityCode: undefined,
};

export default WelcomeSection;
