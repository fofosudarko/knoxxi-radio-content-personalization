// index.js

import SiteHomePage from './SiteHomePage/SiteHomePage';
import SiteStreamPage from './SiteStreamPage/SiteStreamPage';

export { SiteHomePage, SiteStreamPage };
