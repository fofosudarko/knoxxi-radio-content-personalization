import PropTypes from 'prop-types';
import { Container } from 'react-bootstrap';
import Image from 'next/image';

import { useDeviceDimensions } from 'src/hooks';
import { APP_LOGO } from 'src/config';

function SiteMain({ children }) {
  const { isLargeDevice } = useDeviceDimensions();

  return (
    <main className="main-site">
      <div id="welcome" className="content-center">
        <section className="py-2 py-lg-5 overflow-auto">
          <Container style={{ width: isLargeDevice ? '40%' : '100%' }}>
            <div className={`px-2 px-lg-3 py-4 rounded bg-white shadow-lg`}>
              <div className="mb-4 text-center">
                <Image
                  src={APP_LOGO}
                  alt="Knoxxi Logo"
                  width={isLargeDevice ? 200 : 100}
                  height={isLargeDevice ? 70 : 35}
                />
              </div>
              {children}
            </div>
          </Container>
        </section>
      </div>
    </main>
  );
}

SiteMain.propTypes = {
  children: PropTypes.node,
};
SiteMain.defaultProps = {
  children: null,
};

export default SiteMain;
