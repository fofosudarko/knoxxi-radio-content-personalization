import PropTypes from 'prop-types';

import SiteMain from './SiteMain';

function SiteLayout({ children }) {
  return (
    <>
      <SiteMain>{children}</SiteMain>
    </>
  );
}

export function getSiteLayout(page) {
  return <SiteLayout>{page}</SiteLayout>;
}

SiteLayout.propTypes = {
  children: PropTypes.node,
};
SiteLayout.defaultProps = {
  children: null,
};

export default SiteLayout;
