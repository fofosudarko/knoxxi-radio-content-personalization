// StreamLayout.stories.js

import StreamLayout from './StreamLayout';

const Story = {
  title: 'Layouts/StreamLayout',
  component: StreamLayout,
};

const Template = (args) => {
  return <StreamLayout {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
