// StreamMain.stories.js

import StreamMain from './StreamMain';

const Story = {
  title: 'Layouts/StreamMain',
  component: StreamMain,
};

const Template = (args) => {
  return <StreamMain {...args} />;
};

export const Main = Template.bind({});
Main.args = {};

export default Story;
