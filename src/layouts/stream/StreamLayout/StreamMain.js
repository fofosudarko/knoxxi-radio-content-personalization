import PropTypes from 'prop-types';

function StreamMain({ children }) {
  return <div>{children}</div>;
}

StreamMain.propTypes = {
  children: PropTypes.node,
};
StreamMain.defaultProps = {
  children: null,
};

export default StreamMain;
