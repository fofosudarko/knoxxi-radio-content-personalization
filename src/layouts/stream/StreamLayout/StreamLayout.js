import PropTypes from 'prop-types';

import SiteLayout from 'src/layouts/site/SiteLayout/SiteLayout';
import StreamMain from './StreamMain';

function StreamLayout({ children }) {
  return <StreamMain>{children}</StreamMain>;
}

export function getStreamLayout(page) {
  return (
    <SiteLayout>
      <StreamLayout>{page}</StreamLayout>
    </SiteLayout>
  );
}

StreamLayout.propTypes = {
  children: PropTypes.node,
};
StreamLayout.defaultProps = {
  children: null,
};

export default StreamLayout;
