import React, { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';

import {
  getMediaStreamListItemDetails,
  getMediaStreamAdvertItemDetails,
  getMediaStreamScriptItemDetails,
} from 'src/utils';
import { ADVERT_PLAY_INTERVAL } from 'src/config';
import { useNotificationHandler } from 'src/hooks';
import { useMediaStreamListStore } from 'src/stores/media-stream-list';
import { useContentPersonalizationPublicStore } from 'src/stores/content-personalization-public';

const MediaStreamListContext = React.createContext();

function MediaStreamListProvider({
  children,
  mediaStreamList,
  mediaStreamAdverts,
  mediaStreamScripts,
}) {
  const {
    currentStreamIndex,
    streamUrl,
    mediaStreamList: _mediaStreamList,
    currentMediaStreamListItem,
    currentAdvertIndex,
    advertUrl,
    mediaStreamAdverts: _mediaStreamAdverts,
    currentMediaStreamAdvertItem,
    shouldPlayAdvert,
    currentScriptIndex,
    scriptUrl,
    mediaStreamScripts: _mediaStreamScripts,
    currentMediaStreamScriptItem,
    shouldPlayScript,
    shouldPlayAdvertBeforeSelectingNextMediaStreamListItem,
    shouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem,
    shouldPlayAdvertBeforeSelectingMediaStreamListItem,
    bufferingScript,
    bufferingAdvert,
    bufferingStream,
    showRestart,
    playingStream,
    playingAdvert,
    playingScript,
    loadingStream,
    loadingAdvert,
    loadingScript,
    streamsCount,
    setStreamUrl,
    setMediaStreamList,
    setCurrentMediaStreamListItem,
    setAdvertUrl,
    setMediaStreamAdverts,
    setCurrentMediaStreamAdvertItem,
    setScriptUrl,
    setMediaStreamScripts,
    setCurrentMediaStreamScriptItem,
    setCurrentStreamIndex,
    setCurrentAdvertIndex,
    setCurrentScriptIndex,
    setShouldPlayAdvert,
    setShouldPlayAdvertBeforeSelectingNextMediaStreamListItem,
    setShouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem,
    setShouldPlayAdvertBeforeSelectingMediaStreamListItem,
    setShouldPlayScript,
    setBufferingScript,
    setBufferingAdvert,
    setBufferingStream,
    setShowRestart,
    setPlayingStream,
    setPlayingAdvert,
    setPlayingScript,
    setLoadingStream,
    setLoadingAdvert,
    setLoadingScript,
    setStreamsCount,
    clearMediaStreamListState,
  } = useMediaStreamListStore((state) => state);
  const { contentPersonalization } = useContentPersonalizationPublicStore(
    (state) => state
  );
  const state = {
    currentStreamIndex,
    streamUrl,
    mediaStreamList: _mediaStreamList,
    currentMediaStreamListItem,
    currentAdvertIndex,
    advertUrl,
    mediaStreamAdverts: _mediaStreamAdverts,
    currentMediaStreamAdvertItem,
    shouldPlayAdvert,
    currentScriptIndex,
    scriptUrl,
    mediaStreamScripts: _mediaStreamScripts,
    currentMediaStreamScriptItem,
    shouldPlayScript,
    shouldPlayAdvertBeforeSelectingNextMediaStreamListItem,
    shouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem,
    shouldPlayAdvertBeforeSelectingMediaStreamListItem,
    bufferingScript,
    bufferingAdvert,
    bufferingStream,
    showRestart,
    playingStream,
    playingAdvert,
    playingScript,
    loadingStream,
    loadingAdvert,
    loadingScript,
    streamsCount,
    contentPersonalization,
  };
  const handleNotification = useNotificationHandler();

  let handleProcessMediaStreamList,
    incrementStreamsCount,
    decrementStreamsCount;

  useEffect(() => {
    handleProcessMediaStreamList({
      mediaStreamList,
      mediaStreamScripts,
      mediaStreamAdverts,
      currentScriptIndex: state.currentScriptIndex,
      currentAdvertIndex: state.currentAdvertIndex,
      currentStreamIndex: state.currentStreamIndex,
    });
  }, [
    handleProcessMediaStreamList,
    mediaStreamAdverts,
    mediaStreamList,
    mediaStreamScripts,
    state.currentAdvertIndex,
    state.currentScriptIndex,
    state.currentStreamIndex,
  ]);

  // handle select previous media stream list item
  const handleSelectPreviousMediaStreamListItem = useCallback(
    (previousStreamIndex, skipAdvert = false) => {
      if (
        !skipAdvert &&
        state.streamsCount % ADVERT_PLAY_INTERVAL === 0 &&
        state.currentAdvertIndex < state.mediaStreamAdverts?.length &&
        !state.shouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem
      ) {
        setShouldPlayAdvert(true);
        setShouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem(true);
      } else {
        if (state.shouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem) {
          setShouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem(false);
        }
        const currentStreamIndex = previousStreamIndex - 1;
        const mediaStreamListLength = state.mediaStreamList?.length;
        if (
          currentStreamIndex > -1 &&
          currentStreamIndex < mediaStreamListLength
        ) {
          setPlayingStream(true);
          setCurrentStreamIndex(currentStreamIndex);
          incrementStreamsCount();
        }
      }
    },
    [
      state.streamsCount,
      state.currentAdvertIndex,
      state.mediaStreamAdverts?.length,
      state.shouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem,
      state.mediaStreamList?.length,
      setShouldPlayAdvert,
      setShouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem,
      setPlayingStream,
      setCurrentStreamIndex,
      incrementStreamsCount,
    ]
  );
  // handle select next media stream list item
  const handleSelectNextMediaStreamListItem = useCallback(
    (previousStreamIndex, skipAdvert = false) => {
      if (
        !skipAdvert &&
        state.streamsCount % ADVERT_PLAY_INTERVAL === 0 &&
        state.currentAdvertIndex < state.mediaStreamAdverts?.length &&
        !state.shouldPlayAdvertBeforeSelectingNextMediaStreamListItem
      ) {
        setShouldPlayAdvert(true);
        setShouldPlayAdvertBeforeSelectingNextMediaStreamListItem(true);
      } else {
        if (state.shouldPlayAdvertBeforeSelectingNextMediaStreamListItem) {
          setShouldPlayAdvertBeforeSelectingNextMediaStreamListItem(false);
        }
        const currentStreamIndex =
          previousStreamIndex === undefined ? 0 : previousStreamIndex + 1;
        const mediaStreamListLength = state.mediaStreamList?.length;
        if (
          currentStreamIndex < mediaStreamListLength &&
          currentStreamIndex !== mediaStreamListLength
        ) {
          setPlayingStream(true);
          setCurrentStreamIndex(currentStreamIndex);
          incrementStreamsCount();
        } else {
          setPlayingStream(false);
          setShowRestart(true);
        }
      }
    },
    [
      state.streamsCount,
      state.currentAdvertIndex,
      state.mediaStreamAdverts?.length,
      state.shouldPlayAdvertBeforeSelectingNextMediaStreamListItem,
      state.mediaStreamList?.length,
      setShouldPlayAdvert,
      setShouldPlayAdvertBeforeSelectingNextMediaStreamListItem,
      setPlayingStream,
      setCurrentStreamIndex,
      incrementStreamsCount,
      setShowRestart,
    ]
  );
  // handle select next media stream advert item
  const handleSelectNextMediaStreamAdvertItem = useCallback(
    (previousAdvertIndex) => {
      const currentAdvertIndex = previousAdvertIndex + 1;
      setCurrentAdvertIndex(
        currentAdvertIndex % state.mediaStreamAdverts?.length
      );
    },
    [setCurrentAdvertIndex, state.mediaStreamAdverts?.length]
  );
  // handle select next media stream script item
  const handleSelectNextMediaStreamScriptItem = useCallback(
    (previousScriptIndex) => {
      const currentScriptIndex = previousScriptIndex + 1;
      setCurrentScriptIndex(currentScriptIndex);
    },
    [setCurrentScriptIndex]
  );
  // handle stream ended
  const handleStreamEnded = useCallback(
    (previousStreamIndex) => {
      if (
        state.streamsCount % ADVERT_PLAY_INTERVAL === 0 &&
        state.mediaStreamAdverts?.length &&
        state.currentAdvertIndex < state.mediaStreamAdverts?.length
      ) {
        setShouldPlayAdvert(true);
      } else {
        handleSelectNextMediaStreamListItem(previousStreamIndex);
      }
    },
    [
      handleSelectNextMediaStreamListItem,
      setShouldPlayAdvert,
      state.currentAdvertIndex,
      state.mediaStreamAdverts?.length,
      state.streamsCount,
    ]
  );
  // handle select media stream list item
  const handleSelectMediaStreamListItem = useCallback(
    (mediaStreamListItem, skipAdvert = false) => {
      if (
        !skipAdvert &&
        state.streamsCount % ADVERT_PLAY_INTERVAL === 0 &&
        state.currentAdvertIndex < state.mediaStreamAdverts?.length &&
        !state.shouldPlayAdvertBeforeSelectingMediaStreamListItem
      ) {
        setShouldPlayAdvert(true);
        setShouldPlayAdvertBeforeSelectingMediaStreamListItem(true);
      } else {
        if (state.shouldPlayAdvertBeforeSelectingMediaStreamListItem) {
          setShouldPlayAdvertBeforeSelectingMediaStreamListItem(false);
        }
      }
      setPlayingStream(true);
      setCurrentStreamIndex(
        state.mediaStreamList.findIndex(
          (item) => item.id === mediaStreamListItem.id
        )
      );
      incrementStreamsCount();
    },
    [
      state.streamsCount,
      state.currentAdvertIndex,
      state.mediaStreamAdverts?.length,
      state.shouldPlayAdvertBeforeSelectingMediaStreamListItem,
      state.mediaStreamList,
      setPlayingStream,
      setCurrentStreamIndex,
      incrementStreamsCount,
      setShouldPlayAdvert,
      setShouldPlayAdvertBeforeSelectingMediaStreamListItem,
    ]
  );
  // handle advert ended
  const handleAdvertEnded = useCallback(
    (previousStreamIndex) => {
      setShouldPlayAdvert(false);
      handleSelectNextMediaStreamAdvertItem(state.currentAdvertIndex);
      if (state.shouldPlayAdvertBeforeSelectingNextMediaStreamListItem) {
        handleSelectNextMediaStreamListItem(previousStreamIndex);
      } else if (
        state.shouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem
      ) {
        handleSelectPreviousMediaStreamListItem(previousStreamIndex);
      } else if (state.shouldPlayAdvertBeforeSelectingMediaStreamListItem) {
        handleSelectMediaStreamListItem(
          state.mediaStreamList[previousStreamIndex]
        );
      } else {
        handleSelectNextMediaStreamListItem(previousStreamIndex, true);
      }
    },
    [
      handleSelectMediaStreamListItem,
      handleSelectNextMediaStreamAdvertItem,
      handleSelectNextMediaStreamListItem,
      handleSelectPreviousMediaStreamListItem,
      setShouldPlayAdvert,
      state.currentAdvertIndex,
      state.mediaStreamList,
      state.shouldPlayAdvertBeforeSelectingMediaStreamListItem,
      state.shouldPlayAdvertBeforeSelectingNextMediaStreamListItem,
      state.shouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem,
    ]
  );
  // handle script ended
  const handleScriptEnded = useCallback(() => {
    setShouldPlayScript(false);
    handleSelectNextMediaStreamScriptItem(state.currentScriptIndex);
    handleSelectNextMediaStreamListItem(undefined, true);
  }, [
    handleSelectNextMediaStreamListItem,
    handleSelectNextMediaStreamScriptItem,
    setShouldPlayScript,
    state.currentScriptIndex,
  ]);
  // handle script ready
  const handleScriptReady = useCallback(() => {
    setLoadingScript(false);
    setBufferingScript(false);
    setShowRestart(false);
  }, [setBufferingScript, setLoadingScript, setShowRestart]);
  // handle script play
  const handleScriptPlay = useCallback(() => {
    setLoadingScript(false);
    setBufferingScript(false);
    setShowRestart(false);
  }, [setBufferingScript, setLoadingScript, setShowRestart]);
  // handle script buffer
  const handleScriptBuffer = useCallback(() => {
    setBufferingScript(true);
    setShowRestart(true);
  }, [setBufferingScript, setShowRestart]);
  // handle script error
  const handleScriptError = useCallback(
    (error) => {
      handleNotification(error);
    },
    [handleNotification]
  );
  // handle script player error
  const handleScriptPlayerError = useCallback(
    (error) => {
      handleNotification(error);
    },
    [handleNotification]
  );
  // handle advert ready
  const handleAdvertReady = useCallback(() => {
    setLoadingAdvert(false);
    setBufferingAdvert(false);
    setShowRestart(false);
  }, [setBufferingAdvert, setLoadingAdvert, setShowRestart]);
  // handle advert play
  const handleAdvertPlay = useCallback(() => {
    setLoadingAdvert(false);
    setBufferingAdvert(false);
    setShowRestart(false);
  }, [setBufferingAdvert, setLoadingAdvert, setShowRestart]);
  // handle advert buffer
  const handleAdvertBuffer = useCallback(() => {
    setBufferingAdvert(true);
    setShowRestart(true);
  }, [setBufferingAdvert, setShowRestart]);
  // handle advert error
  const handleAdvertError = useCallback(
    (error) => {
      handleNotification(error);
      handleAdvertEnded(state.currentStreamIndex);
    },
    [handleAdvertEnded, handleNotification, state.currentStreamIndex]
  );
  // handle advert player error
  const handleAdvertPlayerError = useCallback(
    (error) => {
      handleNotification(error);
      handleAdvertEnded(state.currentStreamIndex);
    },
    [handleAdvertEnded, handleNotification, state.currentStreamIndex]
  );
  // handle stream ready
  const handleStreamReady = useCallback(() => {
    setLoadingStream(false);
    setBufferingStream(false);
    setShowRestart(false);
  }, [setBufferingStream, setLoadingStream, setShowRestart]);
  // handle stream play
  const handleStreamPlay = useCallback(() => {
    setLoadingStream(false);
    setBufferingStream(false);
    setShowRestart(false);
  }, [setBufferingStream, setLoadingStream, setShowRestart]);
  // handle stream buffer
  const handleStreamBuffer = useCallback(() => {
    setBufferingStream(true);
    setShowRestart(true);
  }, [setBufferingStream, setShowRestart]);
  // handle stream error
  const handleStreamError = useCallback(
    (error) => {
      decrementStreamsCount();
      handleNotification(error);
      handleStreamEnded(state.currentStreamIndex);
    },
    [
      decrementStreamsCount,
      handleNotification,
      handleStreamEnded,
      state.currentStreamIndex,
    ]
  );
  // handle stream player error
  const handleStreamPlayerError = useCallback(
    (error) => {
      handleNotification(error);
      handleStreamEnded(state.currentStreamIndex);
    },
    [handleNotification, handleStreamEnded, state.currentStreamIndex]
  );
  // handle media stream list restart
  const handleMediaStreamListRestart = useCallback(() => {
    clearMediaStreamListState();
    handleProcessMediaStreamList({
      mediaStreamList,
      mediaStreamScripts,
      mediaStreamAdverts,
    });
  }, [
    clearMediaStreamListState,
    handleProcessMediaStreamList,
    mediaStreamAdverts,
    mediaStreamList,
    mediaStreamScripts,
  ]);
  // handle process media stream list
  handleProcessMediaStreamList = useCallback(
    ({
      mediaStreamList = null,
      mediaStreamAdverts = null,
      mediaStreamScripts = null,
      currentScriptIndex = 0,
      currentAdvertIndex = 0,
      currentStreamIndex = 0,
    }) => {
      if (mediaStreamList?.length) {
        const currentMediaStreamListItem = mediaStreamList[currentStreamIndex];
        const contentUrl = getMediaStreamListItemDetails(
          currentMediaStreamListItem
        ).contentUrl;
        setLoadingStream(true);
        setMediaStreamList(mediaStreamList);
        setStreamUrl(contentUrl);
        setCurrentMediaStreamListItem(currentMediaStreamListItem);
      }
      if (mediaStreamAdverts?.length) {
        const currentMediaStreamAdvertItem =
          mediaStreamAdverts[currentAdvertIndex];
        setLoadingAdvert(true);
        setMediaStreamAdverts(mediaStreamAdverts);
        setAdvertUrl(
          getMediaStreamAdvertItemDetails(currentMediaStreamAdvertItem)
            .contentUrl
        );
        setCurrentMediaStreamAdvertItem(currentMediaStreamAdvertItem);
      }
      if (mediaStreamScripts?.length) {
        const currentMediaStreamScriptItem =
          mediaStreamScripts[currentScriptIndex];
        setLoadingScript(true);
        setMediaStreamScripts(mediaStreamScripts);
        setScriptUrl(
          getMediaStreamScriptItemDetails(currentMediaStreamScriptItem)
            .speechUrl
        );
        setCurrentMediaStreamScriptItem(currentMediaStreamScriptItem);
      } else if (
        mediaStreamScripts?.length === undefined ||
        mediaStreamScripts?.length === 0
      ) {
        setShouldPlayScript(false);
      }
    },
    [
      setAdvertUrl,
      setCurrentMediaStreamAdvertItem,
      setCurrentMediaStreamListItem,
      setCurrentMediaStreamScriptItem,
      setLoadingAdvert,
      setLoadingScript,
      setLoadingStream,
      setMediaStreamAdverts,
      setMediaStreamList,
      setMediaStreamScripts,
      setScriptUrl,
      setShouldPlayScript,
      setStreamUrl,
    ]
  );
  // handle stream replay
  const handleStreamReplay = useCallback(
    (mediaStreamListItem) => {
      setStreamUrl(null);
      setCurrentMediaStreamListItem(mediaStreamListItem);
      setCurrentStreamIndex(
        state.mediaStreamList.findIndex(
          (item) => item.id === mediaStreamListItem.id
        )
      );
      setTimeout(() => {
        setPlayingStream(true);
        setStreamUrl(
          getMediaStreamListItemDetails(mediaStreamListItem).contentUrl
        );
      }, 100);
    },
    [
      setCurrentMediaStreamListItem,
      setCurrentStreamIndex,
      setPlayingStream,
      setStreamUrl,
      state.mediaStreamList,
    ]
  );
  // handle advert replay
  const handleAdvertReplay = useCallback(
    (mediaStreamAdvertItem) => {
      setAdvertUrl(null);
      setCurrentMediaStreamAdvertItem(mediaStreamAdvertItem);
      setCurrentAdvertIndex(
        state.mediaStreamAdverts.findIndex(
          (item) => item.id === mediaStreamAdvertItem.id
        )
      );
      setTimeout(() => {
        setPlayingAdvert(true);
        setAdvertUrl(
          getMediaStreamAdvertItemDetails(mediaStreamAdvertItem).contentUrl
        );
      }, 100);
    },
    [
      setAdvertUrl,
      setCurrentMediaStreamAdvertItem,
      setCurrentAdvertIndex,
      state.mediaStreamAdverts,
      setPlayingAdvert,
    ]
  );
  // handle script replay
  const handleScriptReplay = useCallback(
    (mediaStreamScriptItem) => {
      setScriptUrl(null);
      setCurrentMediaStreamAdvertItem(mediaStreamScriptItem);
      setCurrentScriptIndex(
        state.mediaStreamScripts.findIndex(
          (item) => item.id === mediaStreamScriptItem.id
        )
      );
      setTimeout(() => {
        setPlayingScript(true);
        setScriptUrl(
          getMediaStreamScriptItemDetails(mediaStreamScriptItem).speechUrl
        );
      }, 100);
    },
    [
      setCurrentMediaStreamAdvertItem,
      setCurrentScriptIndex,
      setPlayingScript,
      setScriptUrl,
      state.mediaStreamScripts,
    ]
  );
  // increment streams count
  incrementStreamsCount = useCallback(() => {
    setStreamsCount(state.streamsCount + 1);
  }, [setStreamsCount, state.streamsCount]);
  // decrement streams count
  decrementStreamsCount = useCallback(() => {
    setStreamsCount(
      state.streamsCount > 0 ? state.streamsCount - 1 : state.streamsCount
    );
  }, [setStreamsCount, state.streamsCount]);

  return (
    <MediaStreamListContext.Provider
      value={{
        ...state,
        setStreamUrl,
        setMediaStreamList,
        setCurrentMediaStreamListItem,
        setCurrentStreamIndex,
        clearMediaStreamListState,
        handleSelectPreviousMediaStreamListItem,
        handleSelectNextMediaStreamListItem,
        handleStreamEnded,
        handleSelectMediaStreamListItem,
        setAdvertUrl,
        setMediaStreamAdverts,
        setCurrentMediaStreamAdvertItem,
        setCurrentAdvertIndex,
        setShouldPlayAdvert,
        handleAdvertEnded,
        handleSelectNextMediaStreamAdvertItem,
        setScriptUrl,
        setMediaStreamScripts,
        setCurrentMediaStreamScriptItem,
        setCurrentScriptIndex,
        setShouldPlayScript,
        handleScriptEnded,
        handleScriptReady,
        handleScriptPlay,
        handleScriptBuffer,
        handleScriptError,
        handleScriptPlayerError,
        handleAdvertReady,
        handleAdvertPlay,
        handleAdvertBuffer,
        handleAdvertError,
        handleAdvertPlayerError,
        handleStreamReady,
        handleStreamPlay,
        handleStreamBuffer,
        handleStreamError,
        handleStreamPlayerError,
        handleMediaStreamListRestart,
        handleStreamReplay,
        handleAdvertReplay,
        handleScriptReplay,
      }}
    >
      {children}
    </MediaStreamListContext.Provider>
  );
}

export { MediaStreamListContext, MediaStreamListProvider };

MediaStreamListProvider.propTypes = {
  children: PropTypes.node,
  mediaStreamList: PropTypes.array,
  mediaStreamAdverts: PropTypes.array,
  mediaStreamScripts: PropTypes.array,
};
MediaStreamListProvider.defaultProps = {
  children: null,
  mediaStreamList: null,
  mediaStreamAdverts: null,
  mediaStreamScripts: null,
};
