import { create } from 'zustand';
import { persist } from 'zustand/middleware';

const initialState = {
  webPushUuid: undefined,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  setWebPushUuid(webPushUuid) {
    set({ webPushUuid });
  },
  clearWebPushUuidState() {
    set({ ...initialState });
  },
});
export const useWebPushUuidStore = create(
  persist(storeFn, { name: 'web-uuid-storage' })
);
