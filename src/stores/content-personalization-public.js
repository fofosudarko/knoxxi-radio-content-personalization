import { create } from 'zustand';
import { persist, createJSONStorage } from 'zustand/middleware';

const initialState = {
  contentPersonalization: null,
  contentPersonalizations: null,
  contentPersonalizationPage: null,
  contentPersonalizationEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  setContentPersonalization(contentPersonalization) {
    set({ contentPersonalization });
  },
  clearContentPersonalizationPublicState() {
    set({ ...initialState });
  },
});
export const useContentPersonalizationPublicStore = create(
  persist(storeFn, {
    name: 'content-personalization-public-storage',
    storage: createJSONStorage(() => sessionStorage),
  })
);
