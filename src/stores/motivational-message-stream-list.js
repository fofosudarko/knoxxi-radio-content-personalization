import { create } from 'zustand';
import { persist } from 'zustand/middleware';

import Collection from 'src/utils/collection';

const initialState = {
  motivationalMessageStreamList: null,
  motivationalMessageStreamListPage: null,
  motivationalMessageStreamListEndPaging: null,
  motivationalMessageStreamAdverts: null,
  motivationalMessageStreamScripts: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listMotivationalMessageStreamList(motivationalMessageStreamList) {
    const key = motivationalMessageStreamList.key,
      items = motivationalMessageStreamList.items;
    set((state) => ({
      motivationalMessageStreamList: {
        ...state.motivationalMessageStreamList,
        [key]: Collection.list(
          state.motivationalMessageStreamList
            ? state.motivationalMessageStreamList[key] ?? []
            : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setMotivationalMessageStreamListPage(motivationalMessageStreamListPage) {
    const key = motivationalMessageStreamListPage.key,
      item = motivationalMessageStreamListPage.item;
    set((state) => ({
      motivationalMessageStreamListPage: {
        ...state.motivationalMessageStreamListPage,
        [key]: item,
      },
    }));
  },
  setMotivationalMessageStreamListEndPaging(
    motivationalMessageStreamListEndPaging
  ) {
    const key = motivationalMessageStreamListEndPaging.key,
      item = motivationalMessageStreamListEndPaging.item;
    set((state) => ({
      motivationalMessageStreamListEndPaging: {
        ...state.motivationalMessageStreamListEndPaging,
        [key]: item,
      },
    }));
  },
  resetMotivationalMessageStreamListPage(motivationalMessageStreamListPage) {
    const key = motivationalMessageStreamListPage.key;
    set((state) => ({
      motivationalMessageStreamListPage: {
        ...state.motivationalMessageStreamListPage,
        [key]: null,
      },
      motivationalMessageStreamListEndPaging: {
        ...state.motivationalMessageStreamListEndPaging,
        [key]: null,
      },
    }));
  },
  setMotivationalMessageStreamAdverts(motivationalMessageStreamAdverts) {
    const key = motivationalMessageStreamAdverts.key,
      items = motivationalMessageStreamAdverts.items;
    set((state) => ({
      motivationalMessageStreamAdverts: {
        ...state.motivationalMessageStreamAdverts,
        [key]: items,
      },
    }));
  },
  setMotivationalMessageStreamScripts(motivationalMessageStreamScripts) {
    const key = motivationalMessageStreamScripts.key,
      items = motivationalMessageStreamScripts.items;
    set((state) => ({
      motivationalMessageStreamScripts: {
        ...state.motivationalMessageStreamScripts,
        [key]: items,
      },
    }));
  },
  clearMotivationalMessageStreamList(motivationalMessageStreamList) {
    const key = motivationalMessageStreamList.key;
    set((state) => ({
      motivationalMessageStreamList: {
        ...state.motivationalMessageStreamList,
        [key]: null,
      },
    }));
  },
  clearMotivationalMessageStreamAdverts(motivationalMessageStreamAdverts) {
    const key = motivationalMessageStreamAdverts.key;
    set((state) => ({
      motivationalMessageStreamAdverts: {
        ...state.motivationalMessageStreamAdverts,
        [key]: null,
      },
    }));
  },
  clearMotivationalMessageStreamScripts(motivationalMessageStreamScripts) {
    const key = motivationalMessageStreamScripts.key;
    set((state) => ({
      motivationalMessageStreamScripts: {
        ...state.motivationalMessageStreamScripts,
        [key]: null,
      },
    }));
  },
  clearMotivationalMessageStreamListState() {
    set({ ...initialState });
  },
});
export const useMotivationalMessageStreamListStore = create(
  persist(storeFn, {
    name: 'motivational-message-stream-list-storage',
  })
);
