import { create } from 'zustand';
import { persist, createJSONStorage } from 'zustand/middleware';

const initialState = {
  currentStreamIndex: 0,
  streamUrl: null,
  mediaStreamList: null,
  currentMediaStreamListItem: null,
  currentAdvertIndex: 0,
  advertUrl: null,
  mediaStreamAdverts: null,
  currentMediaStreamAdvertItem: null,
  shouldPlayAdvert: false,
  currentScriptIndex: 0,
  scriptUrl: null,
  mediaStreamScripts: null,
  currentMediaStreamScriptItem: null,
  shouldPlayScript: true,
  shouldPlayAdvertBeforeSelectingNextMediaStreamListItem: false,
  shouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem: false,
  shouldPlayAdvertBeforeSelectingMediaStreamListItem: false,
  bufferingScript: false,
  bufferingAdvert: false,
  bufferingStream: false,
  showRestart: false,
  playingStream: true,
  playingAdvert: true,
  playingScript: false,
  loadingStream: false,
  loadingAdvert: false,
  loadingScript: false,
  streamsCount: 0,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  // set stream url
  setStreamUrl(streamUrl) {
    set({ streamUrl });
  },
  // set media stream list
  setMediaStreamList(mediaStreamList) {
    set({ mediaStreamList });
  },
  // set current media stream list item
  setCurrentMediaStreamListItem(currentMediaStreamListItem) {
    set({ currentMediaStreamListItem });
  },
  // set advert url
  setAdvertUrl(advertUrl) {
    set({ advertUrl });
  },
  // set media stream adverts
  setMediaStreamAdverts(mediaStreamAdverts) {
    set({ mediaStreamAdverts });
  },
  // set current media stream advert item
  setCurrentMediaStreamAdvertItem(currentMediaStreamAdvertItem) {
    set({ currentMediaStreamAdvertItem });
  },
  // set script url
  setScriptUrl(scriptUrl) {
    set({ scriptUrl });
  },
  // set media stream scripts
  setMediaStreamScripts(mediaStreamScripts) {
    set({ mediaStreamScripts });
  },
  // set current media stream script item
  setCurrentMediaStreamScriptItem(currentMediaStreamScriptItem) {
    set({ currentMediaStreamScriptItem });
  },
  // set current stream index
  setCurrentStreamIndex(currentStreamIndex) {
    set({ currentStreamIndex });
  },
  // set current advert index
  setCurrentAdvertIndex(currentAdvertIndex) {
    set({ currentAdvertIndex });
  },
  // set current script index
  setCurrentScriptIndex(currentScriptIndex) {
    set({ currentScriptIndex });
  },
  // set should play advert
  setShouldPlayAdvert(shouldPlayAdvert) {
    set({ shouldPlayAdvert });
  },
  // set should play advert before selecting next media stream list item
  setShouldPlayAdvertBeforeSelectingNextMediaStreamListItem(
    shouldPlayAdvertBeforeSelectingNextMediaStreamListItem
  ) {
    set({ shouldPlayAdvertBeforeSelectingNextMediaStreamListItem });
  },
  // set should play advert before selecting previous media stream list item
  setShouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem(
    shouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem
  ) {
    set({ shouldPlayAdvertBeforeSelectingPreviousMediaStreamListItem });
  },
  // set should play advert before selecting media stream list item
  setShouldPlayAdvertBeforeSelectingMediaStreamListItem(
    shouldPlayAdvertBeforeSelectingMediaStreamListItem
  ) {
    set({ shouldPlayAdvertBeforeSelectingMediaStreamListItem });
  },
  // set should play script
  setShouldPlayScript(shouldPlayScript) {
    set({ shouldPlayScript });
  },
  // set buffering script
  setBufferingScript(bufferingScript) {
    set({ bufferingScript });
  },
  // set buffering advert
  setBufferingAdvert(bufferingAdvert) {
    set({ bufferingAdvert });
  },
  // set buffering stream
  setBufferingStream(bufferingStream) {
    set({ bufferingStream });
  },
  // set show restart
  setShowRestart(showRestart) {
    set({ showRestart });
  },
  // set playing stream
  setPlayingStream(playingStream) {
    set({ playingStream });
  },
  // set playing advert
  setPlayingAdvert(playingAdvert) {
    set({ playingAdvert });
  },
  // set playing script
  setPlayingScript(playingScript) {
    set({ playingScript });
  },
  // set loading stream
  setLoadingStream(loadingStream) {
    set({ loadingStream });
  },
  // set loading advert
  setLoadingAdvert(loadingAdvert) {
    set({ loadingAdvert });
  },
  // set loading script
  setLoadingScript(loadingScript) {
    set({ loadingScript });
  },
  // set streams count
  setStreamsCount(streamsCount) {
    set({ streamsCount });
  },
  clearMediaStreamListState() {
    set({ ...initialState });
  },
});
export const useMediaStreamListStore = create(
  persist(storeFn, {
    name: 'media-stream-list-storage',
    storage: createJSONStorage(() => sessionStorage),
  })
);
