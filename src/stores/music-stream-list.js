import { create } from 'zustand';
import { persist } from 'zustand/middleware';

import Collection from 'src/utils/collection';

const initialState = {
  musicStreamList: null,
  musicStreamListPage: null,
  musicStreamListEndPaging: null,
  musicStreamAdverts: null,
  musicStreamScripts: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listMusicStreamList(musicStreamList) {
    const key = musicStreamList.key,
      items = musicStreamList.items;
    set((state) => ({
      musicStreamList: {
        ...state.musicStreamList,
        [key]: Collection.list(
          state.musicStreamList ? state.musicStreamList[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setMusicStreamListPage(musicStreamListPage) {
    const key = musicStreamListPage.key,
      item = musicStreamListPage.item;
    set((state) => ({
      musicStreamListPage: {
        ...state.musicStreamListPage,
        [key]: item,
      },
    }));
  },
  setMusicStreamListEndPaging(musicStreamListEndPaging) {
    const key = musicStreamListEndPaging.key,
      item = musicStreamListEndPaging.item;
    set((state) => ({
      musicStreamListEndPaging: {
        ...state.musicStreamListEndPaging,
        [key]: item,
      },
    }));
  },
  resetMusicStreamListPage(musicStreamListPage) {
    const key = musicStreamListPage.key;
    set((state) => ({
      musicStreamListPage: {
        ...state.musicStreamListPage,
        [key]: null,
      },
      musicStreamListEndPaging: {
        ...state.musicStreamListEndPaging,
        [key]: null,
      },
    }));
  },
  setMusicStreamAdverts(musicStreamAdverts) {
    const key = musicStreamAdverts.key,
      items = musicStreamAdverts.items;
    set((state) => ({
      musicStreamAdverts: {
        ...state.musicStreamAdverts,
        [key]: items,
      },
    }));
  },
  setMusicStreamScripts(musicStreamScripts) {
    const key = musicStreamScripts.key,
      items = musicStreamScripts.items;
    set((state) => ({
      musicStreamScripts: {
        ...state.musicStreamScripts,
        [key]: items,
      },
    }));
  },
  clearMusicStreamList(musicStreamList) {
    const key = musicStreamList.key;
    set((state) => ({
      musicStreamList: {
        ...state.musicStreamList,
        [key]: null,
      },
    }));
  },
  clearMusicStreamAdverts(musicStreamAdverts) {
    const key = musicStreamAdverts.key;
    set((state) => ({
      musicStreamAdverts: {
        ...state.musicStreamAdverts,
        [key]: null,
      },
    }));
  },
  clearMusicStreamScripts(musicStreamScripts) {
    const key = musicStreamScripts.key;
    set((state) => ({
      musicStreamScripts: {
        ...state.musicStreamScripts,
        [key]: null,
      },
    }));
  },
  clearMusicStreamListState() {
    set({ ...initialState });
  },
});
export const useMusicStreamListStore = create(
  persist(storeFn, {
    name: 'music-stream-list-storage',
  })
);
