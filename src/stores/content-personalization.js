import { create } from 'zustand';
import Collection from 'src/utils/collection';

const initialState = {
  contentPersonalization: null,
  contentPersonalizations: null,
  contentPersonalizationPage: null,
  contentPersonalizationEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listContentPersonalizations(contentPersonalizations) {
    const key = contentPersonalizations.key,
      items = contentPersonalizations.items;
    set((state) => ({
      contentPersonalizations: {
        ...state.contentPersonalizations,
        [key]: Collection.list(
          state.contentPersonalizations
            ? state.contentPersonalizations[key] ?? []
            : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setContentPersonalization(contentPersonalization) {
    set({ contentPersonalization });
  },
  setContentPersonalizationPage(contentPersonalizationPage) {
    const key = contentPersonalizationPage.key,
      item = contentPersonalizationPage.item;
    set((state) => ({
      contentPersonalizationPage: {
        ...state.contentPersonalizationPage,
        [key]: item,
      },
    }));
  },
  setContentPersonalizationEndPaging(contentPersonalizationEndPaging) {
    const key = contentPersonalizationEndPaging.key,
      item = contentPersonalizationEndPaging.item;
    set((state) => ({
      contentPersonalizationEndPaging: {
        ...state.contentPersonalizationEndPaging,
        [key]: item,
      },
    }));
  },
  resetContentPersonalizationPage(contentPersonalizationPage) {
    const key = contentPersonalizationPage.key;
    set((state) => ({
      contentPersonalizationPage: {
        ...state.contentPersonalizationPage,
        [key]: null,
      },
      contentPersonalizationEndPaging: {
        ...state.contentPersonalizationEndPaging,
        [key]: null,
      },
    }));
  },
  clearContentPersonalizations(contentPersonalizations) {
    const key = contentPersonalizations.key;
    set((state) => ({
      contentPersonalizations: {
        ...state.contentPersonalizations,
        [key]: null,
      },
    }));
  },
  clearContentPersonalizationState() {
    set({ ...initialState });
  },
});
export const useContentPersonalizationStore = create(storeFn);
