import { create } from 'zustand';
import { persist } from 'zustand/middleware';
import Collection from 'src/utils/collection';

const initialState = {
  carrierCampaigns: null,
  carrierCampaignPage: null,
  carrierCampaignEndPaging: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  listCarrierCampaigns(carrierCampaigns) {
    const key = carrierCampaigns.key,
      items = carrierCampaigns.items;
    set((state) => ({
      carrierCampaigns: {
        ...state.carrierCampaigns,
        [key]: Collection.list(
          state.carrierCampaigns ? state.carrierCampaigns[key] ?? [] : [],
          items,
          { isUnique: true }
        ),
      },
    }));
  },
  setCarrierCampaignPage(carrierCampaignPage) {
    const key = carrierCampaignPage.key,
      item = carrierCampaignPage.item;
    set((state) => ({
      carrierCampaignPage: {
        ...state.carrierCampaignPage,
        [key]: item,
      },
    }));
  },
  setCarrierCampaignEndPaging(carrierCampaignEndPaging) {
    const key = carrierCampaignEndPaging.key,
      item = carrierCampaignEndPaging.item;
    set((state) => ({
      carrierCampaignEndPaging: {
        ...state.carrierCampaignEndPaging,
        [key]: item,
      },
    }));
  },
  resetCarrierCampaignPage(carrierCampaignPage) {
    const key = carrierCampaignPage.key;
    set((state) => ({
      carrierCampaignPage: {
        ...state.carrierCampaignPage,
        [key]: null,
      },
      carrierCampaignEndPaging: {
        ...state.carrierCampaignEndPaging,
        [key]: null,
      },
    }));
  },
  clearCarrierCampaigns(carrierCampaigns) {
    const key = carrierCampaigns.key;
    set((state) => ({
      carrierCampaigns: {
        ...state.carrierCampaigns,
        [key]: null,
      },
    }));
  },
  clearCarrierCampaignState() {
    set({ ...initialState });
  },
});
export const useCarrierCampaignStore = create(
  persist(storeFn, { name: 'carrier-campaign-storage' })
);
