import { create } from 'zustand';
import { persist } from 'zustand/middleware';

const initialState = {
  webPushNotificationSubscription: null,
  webPushSubscription: null,
};
const storeFn = (set) => ({
  ...initialState,
  // actions
  setWebPushNotificationSubscription(webPushNotificationSubscription) {
    set({ webPushNotificationSubscription });
  },
  setWebPushSubscription(webPushSubscription) {
    set({ webPushSubscription });
  },
  clearWebPushNotificationState() {
    set({ ...initialState });
  },
});
export const useWebPushNotificationStore = create(
  persist(storeFn, { name: 'web-push-notification-storage' })
);
